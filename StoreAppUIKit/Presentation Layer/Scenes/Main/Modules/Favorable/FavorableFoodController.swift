//
//  FavorableViewController.swift
//  Sarawan
//
//  Created by MAC on 05.11.2021.
//

import UIKit
import Combine

protocol FavorableFoodViewProtocol: AnyObject {
    func favorableFoodTappedBuyButton(string: String)
}

final class FavorableViewController: UICollectionViewController {

    // MARK: - Properties
    var products = [ProductModel]()
    weak var delegate: FavorableFoodViewProtocol?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
}

private extension FavorableViewController {

    func configureUI() {
        view.backgroundColor = AppColor.white
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        collectionView.registerClass(FavorableFoodCell.self)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = AppColor.favorableBackground
        collectionView.isScrollEnabled = false
        collectionView.layoutIfNeeded()
    }
}

// MARK: - UICollectionViewDelegate
extension FavorableViewController {

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
}

// MARK: - UICollectionViewDataSource
extension FavorableViewController {

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = products[indexPath.item]
        guard let cell: FavorableFoodCell = collectionView.cell(forRowAt: indexPath) else {
            return UICollectionViewCell()
        }
        cell.configureCell(data: .init(nameFood: product.name,
                                       iconFood: UIImage(named: "icDefaultFood"),
                                       nameMarket: product.storeName,
                                       priceFood: product.priceFood,
                                       weightFood: product.priceType,
                                       discountFood: product.discountFood))
        cell.onTappedBuyButton = { [weak self] text in
            guard let self = self else { return }
            self.delegate?.favorableFoodTappedBuyButton(string: text)
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension FavorableViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (collectionView.frame.width - 40 - 15) / 2,
                     height: 255)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20)
    }
}

// MARK: - FavorableFoodDataSourceProtocol
extension FavorableViewController: FavorableFoodViewProtocol {
    func favorableFoodTappedBuyButton(string: String) {
        print(string)
    }
}
