//
//  FavorableFoodCell.swift
//  Sarawan
//
//  Created by MAC on 05.11.2021.
//

import UIKit

final class FavorableFoodCell: UICollectionViewCell {

    // MARK: - Properties
    struct ViewData {
        let nameFood: String?
        let iconFood: UIImage?
        let nameMarket: String?
        let priceFood: String?
        let weightFood: String?
        let discountFood: String?
    }
    var onTappedBuyButton: ((String) -> Void)?

    // MARK: - Outlet
    private lazy var comonContainerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.layer.borderWidth = 0.5
        view.layer.borderColor = AppColor.black.cgColor
        return view
    }()
    private lazy var discountView: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = AppColor.orange
        view.alpha = 1
        view.layer.cornerRadius = 25
        return view
    }()
    private lazy var discountLabel: UILabel = {
        let label = SarawanLabel(alignment: .center,
                                 fontSize: AppFont.montserratFont(ofSize: 16, weight: .medium),
                                 colorText: AppColor.white,
                                 numberLines: 0)
        return label
    }()
    private lazy var nameLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 14, weight: .medium),
                                 numberLines: 3)
        return label
    }()
    private lazy var minPriceLabel: UILabel = {
        let label = SarawanLabel(title: Texts.FoodCard.minPrice,
                                 alignment: .right,
                                 fontSize: AppFont.montserratFont(ofSize: 14, weight: .regular))
        return label
    }()
    private lazy var minPriceMarketLabel: UILabel = {
        let label = SarawanLabel(alignment: .right,
                                 fontSize: AppFont.montserratFont(ofSize: 14, weight: .regular))
        return label
    }()
    private lazy var priceFoodLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 18, weight: .semiBold),
                                 colorText: AppColor.orange)
        return label
    }()
    private lazy var weightFoodLabel: UILabel = {
        let label = SarawanLabel(alignment: .right,
                                 fontSize: AppFont.montserratFont(ofSize: 14, weight: .semiBold),
                                 colorText: AppColor.grey)
        return label
    }()
    private lazy var foodImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        return imageView
    }()
    private lazy var buyButton: QuantityButton = {
        let button = QuantityButton()
        button.addTarget(self, action: #selector(handleBuyButtonTapped), for: .touchUpInside)
        return button
    }()

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        layer.cornerRadius = 8

        contentView.addSubviews([comonContainerView, discountView, discountLabel ])
        comonContainerView.addSubviews(foodImage,
                                nameLabel, minPriceLabel, minPriceMarketLabel,
                                priceFoodLabel, weightFoodLabel, buyButton)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func prepareForReuse() {
        super.prepareForReuse()
        self.foodImage.image = nil
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Action functions
@objc extension FavorableFoodCell {

    func handleBuyButtonTapped() {
        onTappedBuyButton?("Нажал кнопку купиьт")
    }
}

// MARK: - Configure Cell
extension FavorableFoodCell {

    func configureCell(data: FavorableFoodCell.ViewData) {
        nameLabel.text = data.nameFood
        foodImage.image = data.iconFood
        minPriceMarketLabel.text = data.nameMarket
        priceFoodLabel.text = data.priceFood
        weightFoodLabel.text = data.weightFood
        discountLabel.text = data.discountFood
    }
}

// MARK: - ConfigureUI
private extension FavorableFoodCell {

    func configureUI() {
        comonContainerView.snp.makeConstraints {
            $0.edges.equalTo(contentView).offset(0)
        }

        foodImage.snp.makeConstraints {
            $0.top.equalTo(comonContainerView.snp.top).offset(10)
            $0.leading.equalTo(comonContainerView.snp.leading).offset(16)
            $0.trailing.equalTo(comonContainerView.snp.trailing).offset(-16)
        }

        nameLabel.snp.makeConstraints {
            $0.top.equalTo(foodImage.snp.bottom).offset(10)
            $0.leading.equalTo(comonContainerView).offset(20)
            $0.trailing.equalTo(comonContainerView).offset(-20)
            $0.height.equalTo(60)
        }

        minPriceLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(5)
            $0.leading.equalTo(comonContainerView).offset(16)
        }

        minPriceMarketLabel.snp.makeConstraints {
            $0.bottom.equalTo(minPriceLabel.snp.bottom).offset(0)
            $0.leading.equalTo(minPriceLabel.snp.trailing).offset(4)
            $0.trailing.equalTo(comonContainerView.snp.trailing).offset(-16)
        }

        priceFoodLabel.snp.makeConstraints {
            $0.top.equalTo(minPriceLabel.snp.bottom).offset(2)
            $0.leading.equalTo(comonContainerView.snp.leading).offset(16)
        }

        weightFoodLabel.snp.makeConstraints {
            $0.bottom.equalTo(priceFoodLabel.snp.bottom).offset(0)
            $0.leading.equalTo(priceFoodLabel.snp.trailing).offset(4)
            $0.trailing.equalTo(comonContainerView).offset(-16)
        }

        buyButton.snp.makeConstraints {
            $0.height.equalTo(34)
            $0.top.equalTo(priceFoodLabel.snp.bottom).offset(10)
            $0.leading.equalTo(comonContainerView.snp.leading).offset(23)
            $0.trailing.equalTo(comonContainerView.snp.trailing).offset(-23)
            $0.bottom.equalTo(contentView.snp.bottom).offset(-10)
        }

        discountView.snp.makeConstraints {
            $0.height.width.equalTo(50).priority(1)
            $0.top.equalTo(comonContainerView.snp.top).offset(-10)
            $0.trailing.equalTo(comonContainerView.snp.trailing).offset(10)
        }

        discountLabel.snp.makeConstraints {
            $0.centerY.equalTo(discountView.snp.centerY)
            $0.centerX.equalTo(discountView.snp.centerX)
        }
    }
}
