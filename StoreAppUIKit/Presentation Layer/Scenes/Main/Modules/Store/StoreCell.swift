//
//  StoreCell.swift
//  Sarawan
//
//  Created by MAC on 07.11.2021.
//

import UIKit

final class StoreCell: UICollectionViewCell {

    // MARK: - Outlets
    private lazy var logoStoreImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "testImage2")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: .zero)
        addSubview(logoStoreImageView)
        logoStoreImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
