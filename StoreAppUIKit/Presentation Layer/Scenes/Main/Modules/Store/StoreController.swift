//
//  StoreController.swift
//  Sarawan
//
//  Created by MAC on 07.11.2021.
//

import UIKit

final class StoreController: UIViewController {

    // MARK: - Outlets
    private let viewModel: StoreViewModel

    // MARK: - Outlets
    lazy var storeView = self.view as? StoreView

    // MARK: - Init
    init(viewModel: StoreViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = StoreView()
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppColor.white
    }
}
