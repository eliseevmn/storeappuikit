//
//  StoreView.swift
//  Sarawan
//
//  Created by MAC on 07.11.2021.
//

import UIKit

final class StoreView: UIView {

    // MARK: - Outlets
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.registerClass(StoreCell.self)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()

    // MARK: - Init
    init() {
        super.init(frame: .zero)
        backgroundColor = AppColor.white
        addSubview(collectionView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - UICollectionViewDelegate
extension StoreView: UICollectionViewDelegate {

}

// MARK: - UICollectionViewDataSource
extension StoreView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: StoreCell = collectionView.cell(forRowAt: indexPath) else {
            return UICollectionViewCell()
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension StoreView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (collectionView.frame.width - 10) / 2,
                     height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

// MARK: - Configure UI
private extension StoreView {

    func configureUI() {
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
