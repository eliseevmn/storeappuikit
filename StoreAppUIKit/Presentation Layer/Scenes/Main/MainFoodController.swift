//
//  CatalogFoodController.swift
//  Sarawan
//
//  Created by MAC on 04.11.2021.
//

import UIKit
import Combine

final class MainFoodController: UIViewController {

    // MARK: - Properties
    let viewModel: MainFoodViewModel
    var canceballe = Set<AnyCancellable>()

    // MARK: - Outlets
    private lazy var mainFoodView = self.view as? MainFoodView

    // MARK: - Init
    init(viewModel: MainFoodViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = MainFoodView(viewModel: viewModel)
        view.delegate = self
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppColor.white

        setupGestureRecognizer()
        setupBindings()
        refreshingData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeGesture()
        mainFoodView?.removeTextTextField()
    }
}

// MARK: - Public API
extension MainFoodController {
    func setupBindings() {
        mainFoodView?.searchView.textField
            .textPublisher
            .receive(on: DispatchQueue.main)
            .assign(to: \.nameFood, on: viewModel)
            .store(in: &canceballe)
    }
}

// MARK: - Private API
extension MainFoodController {

    private func setupGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(removeGesture))
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }

    private func refreshingData() {
        mainFoodView?.startingActivityIndicator()
        mainFoodView?.isHiddenCollectionView(isLoadingIndicator: true)

        getModelsFromApi()

        viewModel.isLoadingIndicator.sink(receiveValue: { isLoadingIndicator in
            UIView.animate(withDuration: 0.2) {
                self.mainFoodView?.stopActivityIndicator()
                self.mainFoodView?.isHiddenCollectionView(isLoadingIndicator: isLoadingIndicator)
            }
        }).store(in: &canceballe)
    }

    private func getModelsFromApi() {
        viewModel.getAllPopularProducts()
        viewModel.getAllProductsWithDiscount()
    }
}

// MARK: - Public API
extension MainFoodController {

    @objc func removeGesture() {
        mainFoodView?.removeMainFoodTextFieldFirstResponder()
    }
}

// MARK: - Actions button
extension MainFoodController: MainFoodViewProtocol {

    func tapViewAllButton() {
        UIView.animate(withDuration: 0.2) {
            self.tabBarController?.selectedIndex = 1
        }
    }

    func tapSearchButtonView() {
        if !viewModel.nameFood.isEmpty {
            viewModel.onCategoryScreen?(.productsBySearchName(foodName: viewModel.nameFood))
        }
    }

    func tapNextStoreButton(string: String) {
        mainFoodView?.removeMainFoodTextFieldFirstResponder()
    }

    func tapPreviewStoreButton(string: String) {
        mainFoodView?.removeMainFoodTextFieldFirstResponder()
    }
}
