//
//  CatalogFooViewModel.swift
//  Sarawan
//
//  Created by MAC on 04.11.2021.
//

import Foundation
import Combine

final class MainFoodViewModel: ObservableObject {

    // MARK: - Properties
    private let productService: ProductServiceProtocol?
    private var cancellable = Set<AnyCancellable>()

    @Published var nameFood: String = ""
    let isLoadingIndicator = PassthroughSubject<Bool, Never>()
    var popularProducts = PassthroughSubject<[ProductModel], Never>()
    var discountProducts = PassthroughSubject<[ProductModel], Never>()

    var onCategoryScreen: ((TypeFoodQuery) -> Void)?

    // MARK: - Init
    init(productService: ProductServiceProtocol?) {
        self.productService = productService
        self.isLoadingIndicator.send(true)
    }
}

// MARK: - Public API
extension MainFoodViewModel {

    func getAllPopularProducts() {
        productService?.getProductsByTypeFilters(isPopularProducts: true,
                                                 nameFood: nil,
                                                 categoryId: nil,
                                                 filters: nil,
                                                 isDiscountProducts: nil)
            .receive(on: RunLoop.main)
            .map(\.results)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                case .finished:
                    self.isLoadingIndicator.send(false)
                }
            }, receiveValue: { products in
                self.popularProducts.send(products.prefix(10).map({ ProductModel(productResponse: $0) }))
            }).store(in: &cancellable)
    }

    func getAllProductsWithDiscount() {
        productService?.getProductsByTypeFilters(isPopularProducts: nil,
                                                 nameFood: nil,
                                                 categoryId: nil,
                                                 filters: nil,
                                                 isDiscountProducts: true)
            .receive(on: RunLoop.main)
            .receive(on: RunLoop.main)
            .map(\.results)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                case .finished:
                    self.isLoadingIndicator.send(false)
                }
            }, receiveValue: { products in
                self.discountProducts.send(products.prefix(2).map({ ProductModel(productResponse: $0) }))
            }).store(in: &cancellable)
    }
}
