//
//  CatalogFoodView.swift
//  Sarawan
//
//  Created by MAC on 04.11.2021.
//

import UIKit
import Combine

protocol MainFoodViewProtocol: AnyObject {
    func tapPreviewStoreButton(string: String)
    func tapNextStoreButton(string: String)
    func tapSearchButtonView()
    func tapViewAllButton()
}

final class MainFoodView: UIView {

    // MARK: - Outlets
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let width = (UIScreen.main.bounds.size.width - 40 - 15)/2
        layout.estimatedItemSize = CGSize(width: width, height: 10)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        collectionView.showsVerticalScrollIndicator = false
        collectionView.registerClass(MainFoodViewCell.self)
        collectionView.registerHeader(MainHeaderCollectionReusableView.self)
        collectionView.registerFooter(MainFooterCollectionReusableView.self)
        return collectionView
    }()
    private let logoView = LogoView()
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    let searchView = SearchView()

    // MARK: - Properties
    weak var delegate: MainFoodViewProtocol?
    private var cancellable = Set<AnyCancellable>()
    private var viewModel: MainFoodViewModel
    private var productsWithDiscount: [ProductModel] = []
    private var popularProducts: [ProductModel] = []

    // MARK: - Init
    init(viewModel: MainFoodViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        backgroundColor = AppColor.white
        addSubviews(collectionView, searchView, logoView, activityIndicator)
        searchView.delegate = self

        getProductsFromApi()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - SearchViewProtocol
extension MainFoodView: SearchViewProtocol {
    func didTapSearchButtonView() {
        delegate?.tapSearchButtonView()
    }
}

// MARK: - Public API
extension MainFoodView {

    func removeTextTextField() {
        searchView.removeTextTextField()
    }

    func removeMainFoodTextFieldFirstResponder() {
        searchView.removeTextFieldFirstResponder()
    }

    func startingActivityIndicator() {
        activityIndicator.startAnimating()
    }

    func stopActivityIndicator() {
        activityIndicator.stopAnimating()
    }

    func isHiddenCollectionView(isLoadingIndicator: Bool) {
        if isLoadingIndicator {
            collectionView.alpha = 0
        } else {
            collectionView.alpha = 1
        }
    }
}

// MARK: - Private functions
private extension MainFoodView {

    func getProductsFromApi() {
        viewModel.popularProducts.sink(receiveValue: { popularProducts in
            self.popularProducts = popularProducts
            self.collectionView.reloadData()
        }).store(in: &cancellable)

        viewModel.discountProducts.sink(receiveValue: { discountProducts in
            self.productsWithDiscount = discountProducts
            self.collectionView.reloadData()
        }).store(in: &cancellable)
    }
}

// MARK: - UICollectionViewDelegate
extension MainFoodView: UICollectionViewDelegate {

}

// MARK: - UICollectionViewDataSource
extension MainFoodView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return popularProducts.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = popularProducts[indexPath.item]
        guard let cell: MainFoodViewCell = collectionView.cell(forRowAt: indexPath) else {
            return UICollectionViewCell()
        }
        cell.backgroundColor = AppColor.white
        cell.configureCell(data: .init(nameFood: product.name,
                                       imageFood: UIImage(named: product.name),
                                       nameMarket: product.storeName,
                                       priceFood: product.priceFood + " \u{20BD}",
                                       weightFood: product.priceType))
        cell.layoutIfNeeded()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            guard let header: MainHeaderCollectionReusableView = collectionView.cellFooterHeader(ofKind: .header, forIndexPath: indexPath) else {
                return UICollectionReusableView()
            }
            header.controller.products = productsWithDiscount
            header.controller.collectionView.reloadData()
            header.onInviteButtonClicked = {
                self.delegate?.tapViewAllButton()
            }
            return header
        }

        if kind == UICollectionView.elementKindSectionFooter {
            guard let footer: MainFooterCollectionReusableView = collectionView.cellFooterHeader(ofKind: .footer, forIndexPath: indexPath) else {
                return UICollectionReusableView()
            }
            footer.onTappedNextStoreButton = { [weak self] string in
                guard let self = self else { return }
                self.delegate?.tapNextStoreButton(string: string)
            }
            footer.onTappedPreviewStoreButton = { [weak self] string in
                guard let self = self else { return }
                self.delegate?.tapPreviewStoreButton(string: string)
            }
            footer.backgroundColor = .white
            return footer
        }

        return UICollectionReusableView()
    }
}

// MARK: - UICollectionViewDataSource
extension MainFoodView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.frame.size.width, height: 440)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: self.frame.size.width, height: 150)
    }
}

// MARK: - ConfigureUI
private extension MainFoodView {

    func configureUI() {
        logoView.snp.makeConstraints {
            $0.top.equalTo(safeAreaLayoutGuide).offset(20)
            $0.leading.trailing.equalTo(self)
        }

        searchView.snp.makeConstraints {
            $0.top.equalTo(logoView.snp.bottom).offset(10)
            $0.leading.trailing.equalTo(self)
            $0.height.equalTo(36)
        }

        collectionView.snp.makeConstraints {
            $0.top.equalTo(searchView.snp.bottom).offset(24)
            $0.leading.trailing.equalTo(self).offset(0)
            $0.bottom.equalTo(self).offset(0)
        }

        activityIndicator.snp.makeConstraints {
            $0.centerX.equalTo(self).offset(0)
            $0.centerY.equalTo(self).offset(0)
        }
    }
}
