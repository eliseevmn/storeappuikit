//
//  HeaderCollectionView.swift
//  AutoResizingCollectionView
//
//  Created by MAC on 23.11.2021.
//  Copyright © 2021 Andrea Toso. All rights reserved.
//

import UIKit

class MainHeaderCollectionReusableView: UICollectionReusableView {

    // MARK: - Outlets
    var controller = FavorableViewController(collectionViewLayout: UICollectionViewFlowLayout())
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = AppColor.favorableBackground
        return view
    }()

    private lazy var titleLabel = SarawanLabel(title: Texts.Main.favorable,
                                               alignment: .left,
                                               fontSize: AppFont.montserratFont(ofSize: 28, weight: .medium))
    private lazy var recommendTiteLabel = SarawanLabel(title: Texts.Main.reccomend,
                                               alignment: .left,
                                               fontSize: AppFont.montserratFont(ofSize: 18, weight: .semiBold))
    private lazy var viewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Посмотреть всё", for: .normal)
        button.setTitleColor(AppColor.green, for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = AppColor.green.cgColor
        button.layer.cornerRadius = 10
        button.backgroundColor = AppColor.white
        button.addTarget(self, action: #selector(handleViewAll), for: .touchUpInside)
        return button
    }()

    // MARK: - Properties
    var onInviteButtonClicked: (() -> Void)?

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        addSubview(containerView)
        containerView.addSubviews([titleLabel, controller.view, viewButton, recommendTiteLabel])
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func handleViewAll() {
        onInviteButtonClicked?()
    }
}

// MARK: - Private API
private extension MainHeaderCollectionReusableView {
    func configureUI() {

        containerView.snp.makeConstraints {
            $0.top.equalTo(self).offset(0)
            $0.leading.trailing.equalTo(self).offset(0)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(containerView).offset(15)
            $0.leading.equalTo(containerView).offset(20)
        }

        controller.view.snp.makeConstraints {
            $0.leading.trailing.equalTo(containerView)
            $0.top.equalTo(titleLabel.snp.bottom).offset(0)
            $0.height.equalTo(265)
        }

        viewButton.snp.makeConstraints {
            $0.top.equalTo(controller.view.snp.bottom).offset(15)
            $0.centerX.equalTo(containerView).offset(0)
            $0.width.equalTo(177)
            $0.height.equalTo(42)
            $0.bottom.equalTo(containerView).offset(-10)
        }

        recommendTiteLabel.snp.makeConstraints {
            $0.top.equalTo(containerView.snp.bottom).offset(10)
            $0.leading.equalTo(self).offset(20)
            $0.bottom.equalTo(self).offset(-10)
        }
    }
}
