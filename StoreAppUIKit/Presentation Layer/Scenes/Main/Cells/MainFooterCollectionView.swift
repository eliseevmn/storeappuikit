//
//  FooterCollectionView.swift
//  AutoResizingCollectionView
//
//  Created by MAC on 23.11.2021.
//  Copyright © 2021 Andrea Toso. All rights reserved.
//

import UIKit

class MainFooterCollectionReusableView: UICollectionReusableView {

    // MARK: - Outlets
    private let controller = StoreController(viewModel: StoreViewModel())

    private lazy var titleLabel = SarawanLabel(title: Texts.Main.ourStores,
                                               alignment: .left,
                                               fontSize: AppFont.montserratFont(ofSize: 28, weight: .medium))

    private lazy var previewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        button.tintColor = #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)
        button.addTarget(self, action: #selector(handlePrviewButtonTapped), for: .touchUpInside)
        return button
    }()
    private lazy var nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(#colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1), for: .normal)
        button.setImage(UIImage(systemName: "chevron.right"), for: .normal)
        button.tintColor = #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)
        button.addTarget(self, action: #selector(handleNextButtonTapped), for: .touchUpInside)
        return button
    }()

    // MARK: - Properties
    var onTappedPreviewStoreButton: ((String) -> Void)?
    var onTappedNextStoreButton: ((String) -> Void)?
    var contentOffset: CGFloat = 0

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        self.addSubviews(titleLabel, controller.view, previewButton, nextButton)
        configureUI()

        if contentOffset == 0 {
            previewButton.isHidden = true
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Objc functions
@objc extension MainFooterCollectionReusableView {

    func handlePrviewButtonTapped(collectionView: UICollectionView) {
        let collectionBounds = controller.storeView?.collectionView.bounds
        let xPoint = controller.storeView?.collectionView.contentOffset.x ?? 0
        let width = collectionBounds?.size.width ?? 0
        contentOffset = CGFloat(floor(xPoint - (width + 10)/2))
        self.moveCollectionToFrame(contentOffset: contentOffset)
        if contentOffset == 0 {
            previewButton.isHidden = true
        }

        onTappedPreviewStoreButton?("Кнопка назад нажата")
    }

    func handleNextButtonTapped() {
        let collectionBounds = controller.storeView?.collectionView.bounds
        let xPoint = controller.storeView?.collectionView.contentOffset.x ?? 0
        let width = collectionBounds?.size.width ?? 0
        contentOffset = CGFloat(floor(xPoint + (width + 10)/2))
        self.moveCollectionToFrame(contentOffset: contentOffset)
        previewButton.isHidden = false

        onTappedPreviewStoreButton?("Кнопка вперед нажата")
    }

    func moveCollectionToFrame(contentOffset: CGFloat) {
        let xPoint = contentOffset
        let yPoint = controller.storeView?.collectionView.contentOffset.y ?? 0
        let width = controller.storeView?.collectionView.frame.width ?? 0
        let height = controller.storeView?.collectionView.frame.height ?? 0

        let frame: CGRect = CGRect(x: xPoint,
                                   y: yPoint,
                                   width: width,
                                   height: height)
        controller.storeView?.collectionView.scrollRectToVisible(frame, animated: true)
    }
}

// MARK: - Configure UI
private extension MainFooterCollectionReusableView {

    func configureUI() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(self).offset(15)
            $0.leading.equalTo(self).offset(20)
        }

        nextButton.snp.makeConstraints {
            $0.height.equalTo(44)
            $0.width.equalTo(20)
            $0.trailing.equalTo(self).offset(-10)
            $0.top.equalTo(titleLabel.snp.bottom).offset(30)
            $0.bottom.equalTo(self).offset(-20)
        }

        previewButton.snp.makeConstraints {
            $0.height.equalTo(44)
            $0.width.equalTo(20)
            $0.leading.equalTo(self).offset(10)
            $0.top.equalTo(titleLabel.snp.bottom).offset(30)
            $0.bottom.equalTo(self).offset(-20)
        }

        controller.view.snp.makeConstraints {
            $0.leading.equalTo(previewButton.snp.trailing).offset(5)
            $0.trailing.equalTo(nextButton.snp.leading).offset(-5)
            $0.top.equalTo(nextButton.snp.top).offset(-10)
            $0.bottom.equalTo(nextButton.snp.bottom).offset(10)
        }
    }
}
