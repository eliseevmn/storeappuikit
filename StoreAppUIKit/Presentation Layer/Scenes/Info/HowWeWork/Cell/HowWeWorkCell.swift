//
//  HowWeWorkCell.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

final class HowWeWorkCell: UITableViewCell {

    struct ViewData {
        let icon: String?
        let idNumber: String?
        let description: String?
    }

    // MARK: - Outlets
    private lazy var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 1
        view.layer.borderColor = AppColor.lightGray.cgColor
        return view
    }()
    private lazy var numberView: UIView = {
        let view = UIView()
        view.backgroundColor = AppColor.green
        view.layer.cornerRadius = 30
        return view
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = AppColor.black
        return imageView
    }()
    private lazy var numberLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 18, weight: .semiBold),
                                 colorText: AppColor.white)
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 16, weight: .regular),
                                 colorText: AppColor.black,
                                 numberLines: 0)
        return label
    }()

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubviews([containerView, numberView])
        containerView.addSubviews([iconImageView, descriptionLabel])
        numberView.addSubview(numberLabel)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configure()
    }
}

// MARK: - Configure Cell
extension HowWeWorkCell {

    func configureCell(with data: ViewData) {
        numberLabel.text = data.idNumber
        iconImageView.image = UIImage(named: data.icon ?? "")
        descriptionLabel.text = data.description
    }
}

// MARK: - ConfigureUI
private extension HowWeWorkCell {

    func configure() {
        containerView.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(30)
            $0.leading.equalTo(contentView).offset(80)
            $0.trailing.equalTo(contentView).offset(-80)
            $0.bottom.equalTo(contentView).offset(-10)
        }

        numberView.snp.makeConstraints {
            $0.width.height.equalTo(60)
            $0.centerX.equalTo(containerView.snp.centerX)
            $0.top.equalTo(containerView.snp.top).offset(-30)
        }

        iconImageView.snp.makeConstraints {
            $0.centerX.equalTo(containerView.snp.centerX).offset(0)
            $0.top.equalTo(containerView).offset(50)
            $0.width.height.equalTo(60)
        }

        numberLabel.snp.makeConstraints {
            $0.centerX.equalTo(numberView.snp.centerX)
            $0.centerY.equalTo(numberView.snp.centerY)
        }

        descriptionLabel.snp.makeConstraints {
            $0.leading.equalTo(containerView).offset(10)
            $0.trailing.equalTo(containerView).offset(-10)
            $0.top.equalTo(iconImageView.snp.bottom).offset(20)
            $0.bottom.equalTo(containerView).offset(-20)
        }
    }
}
