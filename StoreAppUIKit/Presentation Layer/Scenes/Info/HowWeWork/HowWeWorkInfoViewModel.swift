//
//  HowWeWorkInfoViewModel.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

final class HowWeWorkInfoViewModel {

    // MARK: - Properties
    var isFinishScreen: (() -> Void)?
    var howWeWorkModels: [HowWeWorkModel] = [
        HowWeWorkModel(idNumber: 1,
                       icon: "icList",
                       description: Texts.HowWeWorkInfo.chooseProduct),
        HowWeWorkModel(idNumber: 2,
                       icon: "icCar",
                       description: Texts.HowWeWorkInfo.delivery),
        HowWeWorkModel(idNumber: 3,
                       icon: "icCart",
                       description: Texts.HowWeWorkInfo.subscription)
    ]
}
