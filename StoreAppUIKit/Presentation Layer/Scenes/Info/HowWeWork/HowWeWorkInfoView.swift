//
//  HowWeWorkInfoView.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

protocol HowWeWorkInfoViewProtocol: AnyObject {
    func didTapBackButton()
}

final class HowWeWorkInfoView: UIView {

    // MARK: - Outlets
    private lazy var navigationView = NavigationView(typeFoodQyery: .none, titleName: Texts.CommonInfo.howWeWork)
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.showsVerticalScrollIndicator = false
        tableView.registerClass(HowWeWorkCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        return tableView
    }()

    // MARK: - Properties
    weak var delegate: HowWeWorkInfoViewProtocol?
    private var models: [HowWeWorkModel]

    // MARK: - Init
    init(models: [HowWeWorkModel] = []) {
        self.models = models
        super.init(frame: .zero)
        addSubviews([navigationView, tableView])
        navigationView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - NavigationViewProtocol
extension HowWeWorkInfoView: NavigationViewProtocol {
    func didTapSelectFiltersButtton() {

    }
    
    func didTapBackButton() {
        delegate?.didTapBackButton()
    }
}

// MARK: - UITableViewDelegate
extension HowWeWorkInfoView: UITableViewDelegate {

}

// MARK: - UITableViewDataSource
extension HowWeWorkInfoView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = models[indexPath.row]
        guard let cell: HowWeWorkCell = tableView.cell(forRowAt: indexPath) else {
            return UITableViewCell()
        }
        cell.configureCell(with: .init(icon: model.icon,
                                       idNumber: String(model.idNumber),
                                       description: model.description))
        cell.layoutIfNeeded()
        return cell
    }
}

// MARK: - ConfigureUI
private extension HowWeWorkInfoView {
    func configureUI() {
        navigationView.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(0)
            $0.leading.trailing.equalTo(self).offset(20)
            $0.height.equalTo(34)
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(navigationView.snp.bottom).offset(10)
            $0.leading.equalTo(self).offset(0)
            $0.trailing.equalTo(self).offset(0)
            $0.bottom.equalTo(self).offset(-20)
        }
    }
}
