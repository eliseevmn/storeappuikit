//
//  HowWeWorkController.swift
//  Sarawan
//
//  Created by MAC on 10.11.2021.
//

import UIKit

final class HowWeWorkInfoController: UIViewController {

    // MARK: - Properties
    private lazy var howWeWorkInfoView = self.view as? HowWeWorkInfoView
    let viewModel: HowWeWorkInfoViewModel

    // MARK: - Init
    init(viewModel: HowWeWorkInfoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = HowWeWorkInfoView(models: viewModel.howWeWorkModels)
        view.delegate = self
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppColor.white
    }
}

// MARK: - HowWeWorkInfoViewProtocol
extension HowWeWorkInfoController: HowWeWorkInfoViewProtocol {
    func didTapBackButton() {
        viewModel.isFinishScreen?()
    }
}
