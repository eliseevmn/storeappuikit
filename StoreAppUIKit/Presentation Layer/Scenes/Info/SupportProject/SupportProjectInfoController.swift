//
//  SupportProjectController.swift
//  Sarawan
//
//  Created by MAC on 10.11.2021.
//

import UIKit

final class SupportProjectInfoController: UIViewController {

    // MARK: - Properties
    private lazy var supportProjectInfoView = self.view as? SupportProjectInfoView
    let viewModel: SupportProjectInfoViewModel

    // MARK: - Init
    init(viewModel: SupportProjectInfoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = SupportProjectInfoView(telephoneNumber: viewModel.supportTelephone)
        view.delegate = self
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppColor.white
    }
}

// MARK: - SupportProjectInfoViewProtocol
extension SupportProjectInfoController: SupportProjectInfoViewProtocol {
    func didTapBackButton() {
        viewModel.isFinishScreen?()
    }
}
