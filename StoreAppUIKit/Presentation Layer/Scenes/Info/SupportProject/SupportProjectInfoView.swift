//
//  SupportProjectInfoView.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

protocol SupportProjectInfoViewProtocol: AnyObject {
    func didTapBackButton()
}

final class SupportProjectInfoView: UIView {

    // MARK: - Outlets
    private lazy var telephoneLabel: UILabel = {
        let label = SarawanLabel(title: telephoneNumber,
                                 alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 18, weight: .semiBold))
        return label
    }()
    private lazy var navigationView = NavigationView(typeFoodQyery: .none,
                                                     titleName: Texts.CommonInfo.supportProject)

    // MARK: - Properties
    private let telephoneNumber: String
    weak var delegate: SupportProjectInfoViewProtocol?

    // MARK: - Init
    init(telephoneNumber: String) {
        self.telephoneNumber = telephoneNumber
        super.init(frame: .zero)
        addSubviews([navigationView, telephoneLabel])
        navigationView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - NavigationViewProtocol
extension SupportProjectInfoView: NavigationViewProtocol {
    func didTapSelectFiltersButtton() {
        delegate?.didTapBackButton()
    }

    func didTapBackButton() {
        delegate?.didTapBackButton()
    }
}

// MARK: - ConfigureUI
private extension SupportProjectInfoView {
    func configureUI() {
        navigationView.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(0)
            $0.leading.trailing.equalTo(self).offset(20)
            $0.height.equalTo(34)
        }

        telephoneLabel.snp.makeConstraints {
            $0.top.equalTo(navigationView.snp.bottom).offset(20)
            $0.leading.equalTo(self).offset(20)
            $0.trailing.equalTo(self).offset(-20)
        }
    }
}
