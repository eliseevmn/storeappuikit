//
//  SupportProjectInfoViewModel.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

final class SupportProjectInfoViewModel {

    // MARK: - Properties
    let supportTelephone = Texts.SupportProject.telephoneNumber
    var isFinishScreen: (() -> Void)?
}
