//
//  BenefitsCell.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

final class BenefitsCell: UITableViewCell {

    struct ViewData {
        let icon: String?
        let title: String?
        let description: String?
    }

    // MARK: - Outlets
    private lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = AppColor.green
        return imageView
    }()
    private lazy var titleLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 18, weight: .semiBold))
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 16, weight: .regular),
                                numberLines: 0)
        return label
    }()

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(containerView)
        containerView.addSubviews([iconImageView, titleLabel, descriptionLabel])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configure()
    }
}

// MARK: - Configure Cell
extension BenefitsCell {

    func configureCell(with data: ViewData) {
        iconImageView.image = UIImage(named: data.icon ?? "")
        titleLabel.text = data.title
        descriptionLabel.text = data.description
    }
}

// MARK: - ConfigureUI
private extension BenefitsCell {

    func configure() {
        containerView.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(10)
            $0.leading.equalTo(contentView).offset(20)
            $0.trailing.equalTo(contentView).offset(-20)
            $0.bottom.equalTo(contentView).offset(-10)
        }

        iconImageView.snp.makeConstraints {
            $0.leading.equalTo(containerView).offset(0)
            $0.width.height.equalTo(60)
            $0.centerY.equalTo(contentView).offset(0)
        }

        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(iconImageView.snp.trailing).offset(25)
            $0.trailing.equalTo(containerView).offset(0)
            $0.top.equalTo(containerView).offset(10)
        }

        descriptionLabel.snp.makeConstraints {
            $0.leading.equalTo(titleLabel.snp.leading).offset(0)
            $0.trailing.equalTo(containerView).offset(0)
            $0.top.equalTo(titleLabel.snp.bottom).offset(20)
            $0.bottom.equalTo(containerView).offset(-20)
        }
    }
}
