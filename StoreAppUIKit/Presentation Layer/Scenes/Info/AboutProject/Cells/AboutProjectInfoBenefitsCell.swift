//
//  AboutProjectInfoBenefitsCell.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit
import SnapKit

class AboutProjectInfoBenefitsCell: UITableViewCell {

    // MARK: - Outlets
    let controller = BenefitsController(viewModel: BenefitsViewModel())

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = AppColor.white
        contentView.addSubview(controller.view)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        controller.view.snp.makeConstraints {
            $0.top.leading.equalTo(contentView).offset(10)
            $0.trailing.equalTo(contentView).offset(-10)
            $0.bottom.equalTo(contentView.snp.bottom).offset(0)
        }
    }
}
