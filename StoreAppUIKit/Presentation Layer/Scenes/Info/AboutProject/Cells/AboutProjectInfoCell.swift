//
//  AboutProjectInfoCell.swift
//  Sarawan
//
//  Created by MAC on 10.11.2021.
//

import UIKit

final class AboutProjectInfoCell: UITableViewCell {

    struct ViewData {
        let title: String?
        let info: String?
    }

    // MARK: - Outlet
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = AppColor.greenBackground
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = SarawanLabel(title: Texts.AboutProject.about,
                                 alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 28, weight: .medium),
                                 colorText: AppColor.black,
                                 numberLines: 0)
        return label
    }()

    private lazy var infoLabel: UILabel = {
        let label = SarawanLabel(title: Texts.AboutProject.info,
                                 alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 16, weight: .regular),
                                 colorText: AppColor.black,
                                 numberLines: 0)
        return label
    }()

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none
        contentView.addSubviews(containerView)
        containerView.addSubviews([titleLabel, infoLabel])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Configure UI
private extension AboutProjectInfoCell {

    func configureUI() {
        containerView.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(20)
            $0.leading.equalTo(contentView).offset(20)
            $0.trailing.equalTo(contentView).offset(-20)
            $0.bottom.equalTo(contentView).offset(-10)
        }

        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(containerView).offset(16)
            $0.trailing.equalTo(containerView).offset(-16)
            $0.top.equalTo(containerView).offset(16)
        }

        infoLabel.snp.makeConstraints {
            $0.leading.equalTo(containerView).offset(16)
            $0.trailing.equalTo(containerView).offset(-16)
            $0.top.equalTo(titleLabel.snp.bottom).offset(24)
            $0.bottom.equalTo(containerView).offset(-16)
        }
    }
}
