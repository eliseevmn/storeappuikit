//
//  BenefitsViewModel.swift
//  Sarawan
//
//  Created by MAC on 10.11.2021.
//

import Foundation

final class BenefitsViewModel {

    var benefits: [BenefitModel] = [
        BenefitModel(icon: "icDairy",
                     title: Texts.AboutProject.wideAssortment,
                     description: Texts.AboutProject.wideAssortmentDescription),
        BenefitModel(icon: "icVegetable",
                     title: Texts.AboutProject.freshFood,
                     description: Texts.AboutProject.freshFoodDescription),
        BenefitModel(icon: "icMoneyBox",
                     title: Texts.AboutProject.costSaving,
                     description: Texts.AboutProject.costSavingDescription),
        BenefitModel(icon: "icCar",
                     title: Texts.AboutProject.fastShipping,
                     description: Texts.AboutProject.fastShippingDescription)
    ]
}
