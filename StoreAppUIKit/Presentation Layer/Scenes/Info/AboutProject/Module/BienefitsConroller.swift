//
//  BenefitsController.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

final class BenefitsController: UIViewController {

    // MARK: - Properties
    lazy var benefitsView = self.view as? BenefitsView
    private let viewModel: BenefitsViewModel

    // MARK: - Init
    init(viewModel: BenefitsViewModel = BenefitsViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = BenefitsView(benefits: viewModel.benefits)
        self.view = view
    }

    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppColor.white
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
    }
}
