//
//  BenefitsView.swift
//  Sarawan
//
//  Created by MAC on 10.11.2021.
//

import UIKit

final class BenefitsView: UIView {

    // MARK: - Outlets
    private lazy var titleLabel: UILabel = {
        let label = SarawanLabel(title: Texts.AboutProject.benefits,
                                 alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 28, weight: .medium))
        return label
    }()
    lazy var tableView: DynamicHeightTableView = {
        let tableView = DynamicHeightTableView()
        tableView.showsVerticalScrollIndicator = false
        tableView.registerClass(BenefitsCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        return tableView
    }()

    // MARK: - Properties
    private var benefits: [BenefitModel]

    // MARK: - Init
    init(benefits: [BenefitModel] = []) {
        self.benefits = benefits
        super.init(frame: .zero)
        addSubviews([titleLabel, tableView])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - UITableViewDelegate
extension BenefitsView: UITableViewDelegate {

}

// MARK: - UITableViewDataSource
extension BenefitsView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return benefits.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let benefit = benefits[indexPath.row]
        guard let cell: BenefitsCell = tableView.cell(forRowAt: indexPath) else { return UITableViewCell() }
        cell.configureCell(with: .init(icon: benefit.icon,
                                       title: benefit.title,
                                       description: benefit.description))
        cell.layoutIfNeeded()
        return cell
    }
}

// MARK: - ConfigureUI
private extension BenefitsView {

    func configureUI() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalTo(self).offset(10)
            $0.trailing.equalTo(self).offset(-20)
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(0)
            $0.leading.equalTo(self).offset(0)
            $0.trailing.equalTo(self).offset(0)
            $0.bottom.equalTo(self).offset(-10)
        }
    }
}
