//
//  CodeBaseView.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

protocol AboutProjectInfoViewProtocol: AnyObject {
    func didTapBackButton()
}

final class AboutProjectInfoView: UIView {

    // MARK: - Properties
    weak var delegate: AboutProjectInfoViewProtocol?
    private enum CellType: String, CaseIterable {
        case aboutInfo, benefits
    }
    private var cellsType: [CellType] = CellType.allCases

    // MARK: - Outlets
    private lazy var tableView: DynamicHeightTableView = {
        let tableView = DynamicHeightTableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.registerClass(AboutProjectInfoBenefitsCell.self)
        tableView.registerClass(AboutProjectInfoCell.self)
        tableView.allowsSelection = false
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    private lazy var navigationView = NavigationView(typeFoodQyery: .none, titleName: Texts.CommonInfo.aboutProject)

    // MARK: - Init
    init() {
        super.init(frame: .zero)
        addSubviews([navigationView, tableView])
        navigationView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Public functions
extension AboutProjectInfoView {

    func refreshAboutProjectInfoView() {
        layoutIfNeeded()
        tableView.reloadData()
    }
}

extension AboutProjectInfoView: NavigationViewProtocol {
    func didTapSelectFiltersButtton() {

    }

    func didTapBackButton() {
        delegate?.didTapBackButton()
    }
}

// MARK: - UITableViewDelegate
extension AboutProjectInfoView: UITableViewDelegate {
}

// MARK: - UITableViewDataSource
extension AboutProjectInfoView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsType.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType: CellType = cellsType[indexPath.row]
        let cell: UITableViewCell

        switch cellType {
        case .aboutInfo:
            guard let aboutInfoCell: AboutProjectInfoCell = tableView.cell(forRowAt: indexPath) else { return UITableViewCell() }
            aboutInfoCell.layoutIfNeeded()
            cell = aboutInfoCell
        case .benefits:
            guard let benefitsCell: AboutProjectInfoBenefitsCell = tableView.cell(forRowAt: indexPath) else { return UITableViewCell() }
            benefitsCell.layoutIfNeeded()
            cell = benefitsCell
        }
        return cell
    }
}

// MARK: - ConfigureUI
private extension AboutProjectInfoView {
    func configureUI() {
        navigationView.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(0)
            $0.leading.trailing.equalTo(self).offset(20)
            $0.height.equalTo(34)
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(navigationView.snp.bottom).offset(0)
            $0.leading.trailing.equalTo(self).offset(0)
            $0.bottom.equalTo(self.safeAreaLayoutGuide).offset(0)
        }
    }
}
