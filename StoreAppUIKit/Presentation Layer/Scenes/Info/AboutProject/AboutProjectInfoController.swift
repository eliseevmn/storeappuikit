//
//  AboutProjectInfoController.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

class AboutProjectInfoController: UIViewController {

    // MARK: - Properties
    private lazy var aboutProjectInfoView = self.view as? AboutProjectInfoView
    let viewModel: AboutProjectInfoViewModel

    // MARK: - Init
    init(viewModel: AboutProjectInfoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View liecycle
    override func loadView() {
        super.loadView()
        let view = AboutProjectInfoView()
        view.delegate = self
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppColor.white
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        aboutProjectInfoView?.refreshAboutProjectInfoView()
        navigationController?.navigationBar.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        aboutProjectInfoView?.refreshAboutProjectInfoView()
    }
}

extension AboutProjectInfoController: AboutProjectInfoViewProtocol {
    func didTapBackButton() {
        viewModel.isFinishScreen?()
    }
}
