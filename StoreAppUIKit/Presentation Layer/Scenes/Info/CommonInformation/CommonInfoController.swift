//
//  CommonInformationController.swift
//  Sarawan
//
//  Created by MAC on 10.11.2021.
//

import UIKit

final class CommonInfoController: UIViewController {

    // MARK: - Outlets
    private lazy var commonInfoView = self.view as? CommonInfoView

    // MARK: - Properties
    let viewModel: CommonInfoViewModel
    var itemSelect: ((InfoSettingCellType) -> Void)?

    // MARK: - Init
    init(viewModel: CommonInfoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = CommonInfoView()
        view.delegate = self
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = AppColor.white
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
}

extension CommonInfoController: CommonInfoViewProtocol {
    func tableView(didSelect cellType: InfoSettingCellType) {
        viewModel.onSettingTypeSreens?(cellType)
    }
}
