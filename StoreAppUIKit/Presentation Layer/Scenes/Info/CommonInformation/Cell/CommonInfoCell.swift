//
//  CommonInfoCell.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

final class CommonInfoCell: UITableViewCell {

    struct ViewData {
        let icon: String?
        let title: String?
    }

    // MARK: - Outlets
    private lazy var titleLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 18, weight: .regular))
        return label
    }()
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = AppColor.black
        return imageView
    }()

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubviews([titleLabel, iconImageView])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - ConfigureCell
extension CommonInfoCell {
    func configureCell(with data: ViewData) {
        titleLabel.text = data.title
        iconImageView.image = UIImage(named: data.icon ?? "")
    }
}

// MARK: - Configure UI
private extension CommonInfoCell {

    func configureUI() {

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(15)
            $0.bottom.equalTo(contentView).offset(-15)
            $0.leading.equalTo(contentView).offset(20)
        }
        iconImageView.snp.makeConstraints {
            $0.centerY.equalTo(contentView).offset(0)
            $0.trailing.equalTo(contentView).offset(-20)
        }
    }
}
