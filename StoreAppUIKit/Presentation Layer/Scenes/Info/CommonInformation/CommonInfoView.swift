//
//  CommonInfoView.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import UIKit

protocol CommonInfoViewProtocol: AnyObject {
    func tableView(didSelect cellType: InfoSettingCellType)
}

enum InfoSettingCellType {
    case howWeWorkInfo(icon: String?, title: String?)
    case supportProjectInfo(icon: String?, title: String?)
    case aboutProjectInfo(icon: String?, title: String?)
}

final class CommonInfoView: UIView {

    // MARK: - Outlets
    private lazy var titleLabel: UILabel = {
        let label = SarawanLabel(title: Texts.CommonInfo.info,
                                 alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 28, weight: .medium))
        return label
    }()
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.showsVerticalScrollIndicator = false
        tableView.registerClass(CommonInfoCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        return tableView
    }()

    // MARK: - Properties
    weak var delegate: CommonInfoViewProtocol?
    private let infoSettingTypeCells: [InfoSettingCellType] = [
        .howWeWorkInfo(icon: "icArrowRight",
                       title: Texts.CommonInfo.howWeWork),
        .supportProjectInfo(icon: "icArrowRight",
                            title: Texts.CommonInfo.supportProject),
        .aboutProjectInfo(icon: "icArrowRight",
                          title: Texts.CommonInfo.aboutProject)
    ]

    // MARK: - Init
    init() {
        super.init(frame: .zero)
        addSubviews([tableView, titleLabel])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - UITableViewDelegate
extension CommonInfoView: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let infoSettingTypeCell: InfoSettingCellType  = infoSettingTypeCells[indexPath.row]
        switch infoSettingTypeCell {
        case .howWeWorkInfo(icon: _, title: let title):
            delegate?.tableView(didSelect: .howWeWorkInfo(icon: nil, title: title))
        case .supportProjectInfo(icon: _, title: let title):
            delegate?.tableView(didSelect: .supportProjectInfo(icon: nil, title: title))
        case .aboutProjectInfo(icon: _, title: let title):
            delegate?.tableView(didSelect: .aboutProjectInfo(icon: nil, title: title))
        }
    }
}

// MARK: - UITableViewDataSource
extension CommonInfoView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoSettingTypeCells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let infoSettingTypeCell: InfoSettingCellType  = infoSettingTypeCells[indexPath.row]
        let cell: CommonInfoCell

        switch infoSettingTypeCell {
        case .howWeWorkInfo(icon: let icon, title: let title):
            guard let howWeWorkInfoCell: CommonInfoCell = tableView.cell(forRowAt: indexPath) else {
                return UITableViewCell()
            }
            howWeWorkInfoCell.configureCell(with: .init(icon: icon, title: title))
            cell = howWeWorkInfoCell
        case .supportProjectInfo(icon: let icon, title: let title):
            guard let supportProjectInfoCell: CommonInfoCell = tableView.cell(forRowAt: indexPath) else {
                return UITableViewCell()
            }
            supportProjectInfoCell.configureCell(with: .init(icon: icon, title: title))
            cell = supportProjectInfoCell
        case .aboutProjectInfo(icon: let icon, title: let title):
            guard let aboutProjectInfoCell: CommonInfoCell = tableView.cell(forRowAt: indexPath) else {
                return UITableViewCell()
            }
            aboutProjectInfoCell.configureCell(with: .init(icon: icon, title: title))
            cell = aboutProjectInfoCell
        }

        cell.layoutIfNeeded()
        return cell
    }
}

// MARK: - ConfigureUI
private extension CommonInfoView {
    func configureUI() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(40)
            $0.leading.trailing.equalTo(self).offset(20)
        }
        tableView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(30)
            $0.leading.equalTo(self).offset(0)
            $0.trailing.equalTo(self).offset(0)
            $0.bottom.equalTo(self).offset(-20)
        }
    }
}
