//
//  CommonInfoCoordinator.swift
//  Sarawan
//
//  Created by MAC on 14.11.2021.
//

import UIKit

final class CommonInfoCoordinator: BaseCoordinator {

    // MARK: - Properies
    private let moduleFactory: ModuleFactoryProtocol
    private let router: Router

    // MARK: - Init
    init(router: Router, moduleFactory: ModuleFactoryProtocol) {
        self.moduleFactory = moduleFactory
        self.router = router
    }

    // MARK: - Navigation functions
    override func start() {
        showCommonInfoScreen()
    }

    // MARK: - Private Api
    private func showCommonInfoScreen() {
        let commonInfoScreen = moduleFactory.makeCommonInfoModule()
        commonInfoScreen.viewModel.onSettingTypeSreens = { [weak self] cellType in
            switch cellType {
            case .howWeWorkInfo(icon: _, title: _):
                self?.showHowWeWorkController()
            case .supportProjectInfo(icon: _, title: _):
                self?.showProjectSupportController()
            case .aboutProjectInfo(icon: _, title: _):
                self?.showAboutProjectInfoController()
            }
        }
        router.push(commonInfoScreen)
    }

    // MARK: - Navigations functions
    func showHowWeWorkController() {
        let howWeWorkScreen = moduleFactory.makeHowWeWorkInfoModule()
        router.push(howWeWorkScreen)
        howWeWorkScreen.viewModel.isFinishScreen = {
            self.router.pop(animated: true)
        }
    }

    func showProjectSupportController() {
        let projectSupportScreen = moduleFactory.makeSupportProjectInfoModule()
        router.push(projectSupportScreen)
        projectSupportScreen.viewModel.isFinishScreen = {
            self.router.pop(animated: true)
        }
    }

    func showAboutProjectInfoController() {
        let aboutProjectInfoScreen = moduleFactory.makeAboutProjectInfoModule()
        router.push(aboutProjectInfoScreen)
        aboutProjectInfoScreen.viewModel.isFinishScreen = {
            self.router.pop(animated: true)
        }
    }
}
