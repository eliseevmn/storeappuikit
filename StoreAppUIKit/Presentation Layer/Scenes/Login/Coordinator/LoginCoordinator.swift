//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class LoginCoordinator: BaseCoordinator {

	// MARK: - Private Properties

	private let moduleFactory: ModuleFactoryProtocol
	private let router: Router

	// MARK: - Public Properties

	var finishFlow: ((Bool) -> Void)?

	var isLogin: Bool?

	// MARK: - Initialisers

	init(router: Router, moduleFactory: ModuleFactoryProtocol) {
		self.moduleFactory = moduleFactory
		self.router = router
	}

	// MARK: - Public Methods

	override func start() {
		print(String(describing: Self.self), "started")
		showLoginScreen()
	}

	// MARK: - Private Methods

	private func showLoginScreen() {
		let loginScreen = moduleFactory.makeLoginModule()
		router.setRoot(loginScreen, animated: false, hideBar: true)

		loginScreen.loginViewModel.onLoginCallBack = { [weak self] _, phone in
			self?.showAuthScreen(with: phone)
		}
		loginScreen.loginViewModel.isLogin = { [weak self] isLogin in
			self?.finishFlow?(isLogin)
		}

		isLogin = loginScreen.loginViewModel.checkLoginStatus()
	}

	private func showAuthScreen(with phoneNumber: String) {
		let authScreen = moduleFactory.makeAuthModule(with: phoneNumber)
		router.push(authScreen)

		authScreen.authenticationViewModel.onRegistrationComplete = { [weak self] isComplete in
			print("❗️это из логин координатора \(isComplete)")
			self?.finishFlow?(isComplete)
		}
	}

	deinit { print(Self.self, "deinited") }

}
