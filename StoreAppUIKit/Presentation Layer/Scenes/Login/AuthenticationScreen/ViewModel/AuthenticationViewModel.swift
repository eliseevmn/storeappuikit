//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

final class AuthenticationViewModel {

	// MARK: - Public Properties

	@Published var isCodeCorrect = true
	@Published var phoneNumber: String = ""

	var oneTimeCode: String = ""
	var onRegistrationComplete: ((Bool) -> Void)?

	// MARK: - Private Properties

	private let authService: AuthServiceProtocol?
	private var subscriptions: AnyCancellable?

	// MARK: - Initializers

	init(service: AuthServiceProtocol?, phoneNumber: String) {
		self.authService = service
		self.phoneNumber = phoneNumber
	}

	// MARK: - Public Methods

	func codeEntered() {
		print("phone number in AuthViewModel \(phoneNumber)")
		getToken()
	}

	func sendCodeButtonTapped() {
		print("send button tapped")
	}

	func requestCodeButtonTapped() {
		requestAnotherCode()
	}

	// MARK: - Private Methods

	private func getToken() {
//		onRegistrationComplete?(true)
		subscriptions = authService?.getToken(for: phoneNumber, code: oneTimeCode)
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { [weak self] completion in
				switch completion {
				case .finished:
					self?.onRegistrationComplete?(true)
					print("✅Yuhooo getToken")
				case let .failure(error):
					self?.isCodeCorrect = false
					print("❌ошибка тут \(error)")
				}
			}, receiveValue: { some in
				print(some)
			})
	}

	private func requestAnotherCode() {
		subscriptions = authService?.sendSms(to: phoneNumber)
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { completion in
				switch completion {
				case .finished:
					print("✅Yuhooo sendSms")
				case let .failure(error):
					print(error)
				}
			}, receiveValue: { some in
				print(some)
			})
	}

}
