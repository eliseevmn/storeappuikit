//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class AuthenticationView: UIView {

	// MARK: - Private Properties

	private(set) lazy var codeTextField = VerificationCodeTextField(with: 6)

	private(set) lazy var infoLabel: UILabel = {
		let label = UILabel()
		label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
		label.textAlignment = .center
		label.numberOfLines = 2
		label.textColor = .label
		label.text = "Введите код из СМС"

		return label
	}()

	private(set) lazy var sendCodeButton: BaseButton = {
		let button = BaseButton()
		button.backgroundColor = AppColor.green
		button.setTitle("Отправить", for: .normal)
		button.setTitleColor(UIColor.white, for: .normal)
		button.titleLabel?.font = AppFont.montserratFont(ofSize: 20, weight: .regular)
		button.alpha = 1
		button.isValid = false

		return button
	}()

	private(set) lazy var sendingCodeLabel: UILabel = {
		let label = UILabel()
		label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
		label.textAlignment = .center
		label.numberOfLines = 2
		label.textColor = .label
		label.text = "Код отправлен на телефон \n+7 000 00 00 00"

		return label
	}()

	private(set) lazy var requestCodeLabel: UILabel = {
		let label = UILabel()
		label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
		label.textAlignment = .center
		label.numberOfLines = 1
		label.textColor = .label
		label.text = "Запросить код повторно: через"

		return label
	}()

	private(set) lazy var remainingTimeLabel: UILabel = {
		let label = UILabel()
		label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
		label.textAlignment = .left
		label.numberOfLines = 1
		label.textColor = .label
		label.text = "01:00"

		return label
	}()

	private(set) lazy var requestCodeButton: BaseButton = {
		let button = BaseButton()
		button.backgroundColor = AppColor.green
		button.setTitle("Запросить код повторно", for: .normal)
		button.setTitleColor(UIColor.white, for: .normal)
		button.titleLabel?.font = AppFont.montserratFont(ofSize: 20, weight: .regular)
		button.alpha = 1
		button.isValid = false

		return button
	}()

	// MARK: - Initialisers

	override init(frame: CGRect) {
		super.init(frame: frame)
		backgroundColor = .systemBackground
		setupLayout()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private Methods

	private func setupLayout() {
		addSubview(infoLabel)
		addSubview(sendCodeButton)
		addSubview(sendingCodeLabel)
		addSubview(requestCodeLabel)
		addSubview(remainingTimeLabel)
		addSubview(requestCodeButton)
		addSubview(codeTextField)

		infoLabel.snp.makeConstraints {
			$0.top.equalTo(safeAreaLayoutGuide).offset(10)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(200)
			$0.height.equalTo(24)
		}
		codeTextField.snp.makeConstraints {
			$0.top.equalTo(infoLabel.snp.bottom).offset(32)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(272)
			$0.height.equalTo(48)
		}
		sendCodeButton.snp.makeConstraints {
			$0.top.equalTo(codeTextField.snp.bottom).offset(16)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(155)
			$0.height.equalTo(48)
		}
		sendingCodeLabel.snp.makeConstraints {
			$0.top.equalTo(sendCodeButton.snp.bottom).offset(32)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(261)
			$0.height.equalTo(48)
		}
		requestCodeLabel.snp.makeConstraints {
			$0.top.equalTo(sendingCodeLabel.snp.bottom).offset(27)
			$0.centerX.equalTo(self.snp.centerX)
//			$0.width.equalTo(320)
			$0.height.equalTo(24)
		}
		remainingTimeLabel.snp.makeConstraints {
			$0.top.equalTo(sendingCodeLabel.snp.bottom).offset(27)
			$0.leading.equalTo(requestCodeLabel.snp.trailing).offset(3)
			$0.height.equalTo(24)
		}
		requestCodeButton.snp.makeConstraints {
			$0.top.equalTo(requestCodeLabel.snp.bottom).offset(16)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(305)
			$0.height.equalTo(48)
		}
	}

}
