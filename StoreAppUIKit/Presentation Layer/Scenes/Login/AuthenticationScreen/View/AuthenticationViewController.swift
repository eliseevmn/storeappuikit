//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

class AuthenticationViewController: UIViewController {

	// MARK: - Public Properties

	let authenticationViewModel: AuthenticationViewModel
	var subscriptions = Set<AnyCancellable>()

	// MARK: - Private Properties

//	private let provider: LoginProvider
	private lazy var codeVerView = AuthenticationView()
	private var remaingTime: Int = 10
	private var codeTimer: Timer!

	// MARK: - Initialisers

	init(viewModel: AuthenticationViewModel /*provider: LoginProvider*/) {
		self.authenticationViewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Lifecycle

	override func loadView() {
		view = codeVerView
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.setNavigationBarHidden(false, animated: true)
		navigationController?.navigationBar.prefersLargeTitles = false
		setupViewModelBindings()
		setupTimer()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		codeVerView.codeTextField.becomeFirstResponder()
	}

	// MARK: - Private Methods

	private func setupViewModelBindings() {
		codeVerView.codeTextField.textPublisher
			.sink(receiveValue: { [unowned self] code in
				if code.count == 6 {
					self.authenticationViewModel.oneTimeCode = code
					self.codeVerView.sendCodeButton.isValid = true
					self.authenticationViewModel.codeEntered()
				} else {
					self.codeVerView.infoLabel.text = "Введите код из СМС"
					self.codeVerView.codeTextField.isCorrect = true
					self.codeVerView.sendCodeButton.isValid = false
				}
			})
			.store(in: &subscriptions)

		codeVerView.sendCodeButton.tapPublisher
			.sink(receiveValue: { [unowned self] _ in
				self.authenticationViewModel.sendCodeButtonTapped()
			})
			.store(in: &subscriptions)

		codeVerView.requestCodeButton.tapPublisher
			.sink { [unowned self] in
				self.remaingTime = 10
				self.codeVerView.remainingTimeLabel.text = "\(remaingTime)"
				self.authenticationViewModel.requestCodeButtonTapped()
				self.codeVerView.requestCodeButton.isValid = false
				self.setupTimer()
			}
			.store(in: &subscriptions)

		authenticationViewModel.$phoneNumber
			.sink { [weak self] phone in
				self?.codeVerView.sendingCodeLabel.text = "Код отправлен на телефон\n\(phone)"
			}
			.store(in: &subscriptions)

		authenticationViewModel.$isCodeCorrect
			.sink(receiveValue: { [weak self] codeCorrect in
				self?.codeVerView.codeTextField.isCorrect = codeCorrect
				self?.codeVerView.infoLabel.text = codeCorrect ? "Введите код из СМС" : "Неверный код"
			})
			.store(in: &subscriptions)
	}

	private func setupTimer() {
		codeTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(stepTimer), userInfo: nil, repeats: true)
	}

	@objc private func stepTimer() {
		if remaingTime > 0 {
			codeVerView.requestCodeButton.isValid = false
			remaingTime -= 1
		} else {
			codeTimer.invalidate()
			codeVerView.requestCodeButton.isValid = true
		}
		codeVerView.remainingTimeLabel.text = "\(remaingTime)"
	}

}
