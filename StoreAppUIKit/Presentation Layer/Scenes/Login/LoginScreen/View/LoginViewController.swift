//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

final class LoginViewController: UIViewController {

	// MARK: - Private Properties

//	private let provider: LoginProvider
	private lazy var loginView = LoginView()

	// MARK: - Public Properties

	let loginViewModel: LoginViewModel
	var subscriptions = Set<AnyCancellable>()

	// MARK: - Initialisers

	init(viewModel: LoginViewModel /*provider: LoginProvider*/) {
		self.loginViewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Lifecycle

	override func loadView() {
		view = loginView
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		setupViewModelBindings()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(true, animated: true)
		navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
	}

	// MARK: - Public Methods

	// MARK: - Private Methods

	func setupViewModelBindings() {

		loginView.phoneTextField.textPublisher
			.assign(to: \.phoneNumber, on: loginViewModel)
			.store(in: &subscriptions)

		loginView.acceptanceButton.tapPublisher
			.sink(receiveValue: { [weak self] in
				self?.loginViewModel.acceptancePolicy.toggle()
			})
			.store(in: &subscriptions)

		loginView.getCodeButton.tapPublisher
			.sink(receiveValue: { [weak self] _ in
				self?.loginViewModel.codeButtonDidTapped()
			})
			.store(in: &subscriptions)

		loginViewModel.$acceptancePolicy
			.assign(to: \.isSelected, on: loginView.acceptanceButton)
			.store(in: &subscriptions)

		loginViewModel.isInputValid
			.assign(to: \.isValid, on: loginView.getCodeButton)
			.store(in: &subscriptions)

		loginViewModel.onPhoneReqCallback = { [weak self] isError in
			if isError {
				#warning("Убрать алерт либо в кастомный, либо куда-то в общие")
				let alert = UIAlertController(title: "Sheet happens",
											  message: "Не хочет твой код отправляться, наверно инета нету, тыкай пока не появится",
											  preferredStyle: .alert)
				alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
				NSLog("The \"OK\" alert occured.")
				}))
				self?.present(alert, animated: true, completion: nil)
			}
		}
	}

}
