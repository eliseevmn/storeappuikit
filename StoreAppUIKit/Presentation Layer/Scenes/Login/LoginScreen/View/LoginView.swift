//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import SnapKit

class LoginView: UIView {

	// MARK: - Private Properties

	private lazy var infoLabel: UILabel = {
		let label = UILabel()
		label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
		label.textAlignment = .left
		label.numberOfLines = 2
		label.textColor = .label
		label.text = "Введите номер телефона, чтобы войти или зарегистрироваться"

		return label
	}()

	private(set) var phoneTextField: STextField = {
		let textField = STextField()
		textField.placeholder = "+7(___) ___ __ __"
		textField.text = "+7"
		textField.keyboardType = .phonePad
//		textField.defaultTextAttributes.updateValue(50.0, forKey: .kern)

		return textField
	}()

	private(set) lazy var getCodeButton: BaseButton = {
		let button = BaseButton()
		button.backgroundColor = AppColor.green.withAlphaComponent(0.3)
		button.setTitle("Получить смс с кодом", for: .normal)
		button.setTitleColor(UIColor.white, for: .normal)
		button.titleLabel?.font = AppFont.montserratFont(ofSize: 20, weight: .regular)
		button.isEnabled = false
		button.layer.cornerRadius = 15

		return button
	}()

	private lazy var acceptanceLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 14, weight: .regular)
		label.textAlignment = .left
		label.numberOfLines = 3
		label.lineBreakMode = .byWordWrapping
		label.textColor = .label
		label.text = "Нажимая на кнопку, я принимаю условия пользовательского соглашения"
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private(set) lazy var acceptanceButton: UIButton = {
		let button = UIButton()
		button.setImage(UIImage(named: "icNoCheck"), for: .normal)
		button.setImage(UIImage(named: "icCheck"), for: .selected)
		button.translatesAutoresizingMaskIntoConstraints = false

		return button
	}()

	// MARK: - Initialisers

	override init(frame: CGRect) {
		super.init(frame: frame)

		backgroundColor = .systemBackground
		setupLayout()
		phoneTextField.delegate = self
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private Methods

	private func setupLayout() {
		addSubview(infoLabel)
		addSubview(phoneTextField)
		addSubview(getCodeButton)
		addSubview(acceptanceButton)
		addSubview(acceptanceLabel)

		infoLabel.snp.makeConstraints {
			$0.top.equalTo(safeAreaLayoutGuide).offset(32)
			$0.centerX.equalTo(self.snp.centerX)
			$0.leading.equalTo(self).offset(46)
			$0.trailing.equalTo(self).offset(-46)
		}
		phoneTextField.snp.makeConstraints {
			$0.top.equalTo(infoLabel.snp.bottom).offset(16)
			$0.centerX.equalTo(self.snp.centerX)
			$0.height.equalTo(44)
			$0.leading.equalTo(self).offset(54)
			$0.trailing.equalTo(self).offset(-54)

		}
		getCodeButton.snp.makeConstraints {
			$0.top.equalTo(phoneTextField.snp.bottom).offset(32)
			$0.centerX.equalTo(self.snp.centerX)
			$0.height.equalTo(48)
			$0.leading.equalTo(self).offset(65)
			$0.trailing.equalTo(self).offset(-65)
		}
		acceptanceButton.snp.makeConstraints {
			$0.top.equalTo(getCodeButton.snp.bottom).offset(22)
			$0.height.equalTo(22)
			$0.width.equalTo(22)
			$0.leading.equalTo(self).offset(30)
		}
		acceptanceLabel.snp.makeConstraints {
			$0.centerY.equalTo(acceptanceButton.snp.centerY)
			$0.leading.equalTo(acceptanceButton.snp.trailing).offset(8)

			$0.trailing.lessThanOrEqualTo(self.snp.trailing).offset(-20)
			$0.trailing.equalTo(self).offset(-20)
			$0.height.equalTo(70)
		}
	}

}

extension LoginView: UITextFieldDelegate {

	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let text = textField.text else { return false }
		if text == "+7" && string == "" {
			return false
		}
		return string.isEmpty || Int(string) != nil && text.count <= 11
	}

}
