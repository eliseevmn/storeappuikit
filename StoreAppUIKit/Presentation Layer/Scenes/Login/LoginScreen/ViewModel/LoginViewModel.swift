//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

final class LoginViewModel {

	// MARK: - Public Properties

	@Published var phoneNumber: String = ""
	@Published var acceptancePolicy: Bool = false

	var onLoginCallBack: ((Bool, String) -> Void)?
	var onPhoneReqCallback: ((Bool) -> Void)?
	var isLogin: ((Bool) -> Void)?

	// MARK: - Private Properties

	private let loginService: AuthServiceProtocol?
	private var subscriptions: AnyCancellable?
	private(set) lazy var isInputValid = Publishers.CombineLatest($phoneNumber, $acceptancePolicy)
		.map { $0.count == 12 && $1 }
		.eraseToAnyPublisher()

	// MARK: - Initializers

	init(service: AuthServiceProtocol?) {
		self.loginService = service
		guard let loginService = loginService else { return }
		isLogin?(loginService.isLogin)
	}

	// MARK: - Public Methods

	 func codeButtonDidTapped() {
		subscriptions = loginService?.sendSms(to: phoneNumber)
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { [weak self] completion in
				switch completion {
				case .finished:
					print("✅Yuhooo sendSms")
					self?.onLoginCallBack?(true, self?.phoneNumber ?? "none")
				case let .failure(error):
					print(error)
					self?.onPhoneReqCallback!(true)
				}
			}, receiveValue: { some in
				print(some)
			})
//		onLoginCallBack?(true, phoneNumber)
	}

	func checkLoginStatus() -> Bool {
		guard let loginService = loginService else { return false }
		return loginService.isLogin
	}

}
