//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

final class AddressViewModel {

	// MARK: - Private Properties

	private let userService: UserServiceProtocol?
	private var address: Address? {
		didSet {
			updateAddressData?(address!)
		}
	}
	private let userID: Int
	private var subscriptions: AnyCancellable?

	// MARK: - Public Properties

	var isFinishScreen: ((Bool) -> Void)?
	var updateAddressData: ((Address) -> Void)?

	var city: String = ""
	var street: String = ""
	var house: String = ""
	var building: String = ""
	var housing: String = ""
	var roomNumber: String = ""

	// MARK: - Initializers

	init(service: UserServiceProtocol) {
		self.userService = service
		self.userID = service.userId
	}

	// MARK: - Public Methods

	func requestData() {
		getAddresses()
	}

	func saveButtonDidTap() {
		updateAddressToPost()
		guard let address = address else { return }
		updateAddress(with: address)
	}

	// MARK: - Private Methods

	private func updateAddressToPost() {
		address?.city = city
		address?.street = street
		address?.house = house
		address?.building = building
		address?.housing = housing
		address?.roomNumber = roomNumber
		address?.primary = true
		address?.user = userID
	}

	private func getAddresses() {
		subscriptions = userService?.addresses()
			.sink(receiveCompletion: { completion in
				switch completion {
				case .finished:
					print("⬇️🏛")
				case let .failure(error):
					print(error)
				}
			}, receiveValue: { [weak self] addresses in
				self?.address = addresses.first
			})
	}

	private func updateAddress(with data: Address) {
		subscriptions = userService?.newAddress(with: data)
			.sink(receiveCompletion: { [weak self] completion in
				switch completion {
				case .finished:
					self?.isFinishScreen?(true)
				case let .failure(error):
					print(error)
				}
			}, receiveValue: { _ in })
	}

}
