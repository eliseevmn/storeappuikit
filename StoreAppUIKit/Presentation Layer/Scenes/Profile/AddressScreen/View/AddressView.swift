//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class AddressView: UIView {

	// MARK: - Private Properties

	private(set) var cityTextField: STextField = {
		let textField = STextField()
		textField.placeholder = "Город"

		return textField
	}()

	private(set) var streetTextField: STextField = {
		let textField = STextField()
		textField.placeholder = "Улица"

		return textField
	}()

	private(set) var houseTextField: STextField = {
		let textField = STextField()
		textField.placeholder = "Дом"
		textField.keyboardType = .numberPad

		return textField
	}()

	private(set) var entranceTextField: STextField = {
		let textField = STextField()
		textField.placeholder = "Подъезд"
		textField.keyboardType = .numberPad

		return textField
	}()

	private(set) var apartTextField: STextField = {
		let textField = STextField()
		textField.placeholder = "Квартира"
		textField.keyboardType = .numberPad

		return textField
	}()

	private(set) lazy var saveButton = SaveButton()

	private var textFields: [STextField] = []

	// MARK: - Initializers

	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private Methods

	private func configure() {
		self.backgroundColor = .systemBackground

		textFields = [cityTextField, streetTextField, houseTextField, entranceTextField, apartTextField]
		textFields.forEach { addSubview($0) }

		addSubview(saveButton)

		cityTextField.snp.makeConstraints {
			$0.top.equalTo(self.safeAreaLayoutGuide).offset(24)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(350)
			$0.height.equalTo(44)
		}
		streetTextField.snp.makeConstraints {
			$0.top.equalTo(cityTextField.snp.bottom).offset(24)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(350)
			$0.height.equalTo(44)
		}
		entranceTextField.snp.makeConstraints {
			$0.top.equalTo(streetTextField.snp.bottom).offset(24)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(100)
			$0.height.equalTo(44)
		}
		houseTextField.snp.makeConstraints {
			$0.top.equalTo(streetTextField.snp.bottom).offset(24)
			$0.trailing.equalTo(entranceTextField.snp.leading).offset(-25)
			$0.width.equalTo(100)
			$0.height.equalTo(44)
		}

		apartTextField.snp.makeConstraints {
			$0.top.equalTo(streetTextField.snp.bottom).offset(24)
			$0.leading.equalTo(entranceTextField.snp.trailing).offset(25)
			$0.width.equalTo(100)
			$0.height.equalTo(44)
		}
		saveButton.snp.makeConstraints {
			$0.top.equalTo(entranceTextField.snp.bottom).offset(48)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(130)
			$0.height.equalTo(44)
		}
	}

}
