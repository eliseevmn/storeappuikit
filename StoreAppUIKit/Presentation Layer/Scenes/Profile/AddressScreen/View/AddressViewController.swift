//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

class AddressViewController: UIViewController {

	private lazy var addressView = view as? AddressView

	// MARK: - Public Properties

	let addressViewModel: AddressViewModel
	var subscriptions = Set<AnyCancellable>()

	// MARK: - Initializers

	init(viewModel: AddressViewModel /*provider: AddressProvider*/) {
		self.addressViewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Lifecycle

	override func loadView() {
		view = AddressView()
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Добавить адрес"
		setupViewModelBindings()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationItem.largeTitleDisplayMode = .never
		addressViewModel.requestData()
	}

	// MARK: - Private Methods

	private func setupViewModelBindings() {

		// view to viewModel

		addressView?.saveButton.tapPublisher
			.sink(receiveValue: { _ in
				self.addressViewModel.city = self.addressView?.cityTextField.text ?? ""
				self.addressViewModel.street = self.addressView?.streetTextField.text ?? ""
				self.addressViewModel.house = self.addressView?.houseTextField.text ?? ""
				self.addressViewModel.building = self.addressView?.entranceTextField.text ?? ""
				self.addressViewModel.housing = self.addressView?.entranceTextField.text ?? ""
				self.addressViewModel.roomNumber = self.addressView?.apartTextField.text ?? ""
				self.addressViewModel.saveButtonDidTap()
			})
			.store(in: &subscriptions)

		addressView?.cityTextField.textPublisher.combineLatest(
			(addressView?.streetTextField.textPublisher)!,
			(addressView?.houseTextField.textPublisher)!,
			(addressView?.entranceTextField.textPublisher)!)
			.sink(receiveValue: { [weak self] (city, street, house, entrance) in
				self?.addressViewModel.city = city
				self?.addressViewModel.street = street
				self?.addressViewModel.house = house
				self?.addressViewModel.housing = entrance
				self?.addressViewModel.building = entrance
			})
			.store(in: &subscriptions)

		addressView?.apartTextField.textPublisher
			.sink(receiveValue: { [weak self]  apart in
				self?.addressViewModel.roomNumber = apart
			})
			.store(in: &subscriptions)

		// viewModel to view

		addressViewModel.updateAddressData = { [weak self] address in
			DispatchQueue.main.async {
				self?.addressView?.cityTextField.text = address.city
				self?.addressView?.streetTextField.text = address.street
				self?.addressView?.houseTextField.text = address.house
				self?.addressView?.entranceTextField.text = address.housing
				self?.addressView?.apartTextField.text = address.roomNumber
			}
		}
	}

	@objc private func saveButtonDidTap() {
		addressViewModel.saveButtonDidTap()
	}

}
