//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class ProfileTableCell: BaseTableCell {

	// MARK: - Public Properties

	var userInfoLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .left
		label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
		label.textColor = .label
		label.numberOfLines = 2
		label.lineBreakMode = .byWordWrapping
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	struct Model {
		var userInfo: String
	}

	// MARK: - Public Methods

	override func setup(with data: Any?) {
		configureUI()
		guard let model = data as? Model else { return }
		if model.userInfo.isEmpty {
			userInfoLabel.text = "пусто"
		}
		userInfoLabel.text = model.userInfo
	}

	func configureUI() {
		contentView.addSubview(userInfoLabel)
		accessoryType = .disclosureIndicator
		userInfoLabel.snp.makeConstraints { make in
			make.edges.equalTo(contentView).inset(UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20))
		}
	}

}
