//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

class ProfileViewController: UIViewController {

    // MARK: - Private Properties

//    private let provider: ProfileProvider
	private lazy var profileView = view as? ProfileView

    // MARK: - Public Properties

    let profileViewModel: ProfileViewModel
	var subscriptions = Set<AnyCancellable>()

    // MARK: - Initializers

    init(viewModel: ProfileViewModel /*provider: ProfileProvider*/) {
        self.profileViewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func loadView() {
        view = ProfileView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
		title = "Профиль"
		setupViewModelBindings()
		setupNavigationController()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		profileViewModel.requestData()
	}

	// MARK: - Private Methods

	private func setupViewModelBindings() {
		profileView?.profileTable.cellDidTap = { [weak self] indexPath in
			self?.profileViewModel.screenToShow(by: indexPath)
		}

		profileView?.exitButton.tapPublisher
			.sink(receiveValue: { [weak self] _ in
				self?.profileViewModel.exitButtonDidTapped()
			})
			.store(in: &subscriptions)

		profileViewModel.profileUserData = { userInfo in
			DispatchQueue.main.async {
				self.profileView?.profileTable.content(with: userInfo)
			}
		}
	}

	private func setupNavigationController() {
		navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
		navigationController?.navigationBar.prefersLargeTitles = true
		navigationItem.largeTitleDisplayMode = .always
	}

}
