//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class ProfileTableView: BaseTable {

	// MARK: - Initializers

	override init(frame: CGRect, style: UITableView.Style) {
		super.init(frame: frame, style: style)
		register(ProfileTableCell.self, forCellReuseIdentifier: Cell.reuseID)
		isScrollEnabled = false
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Public Methods

	func content(with userInfo: ProfileUserInfo) {
		data = [
			Section(name: "Профиль", cells: [
				Cell(data: ProfileTableCell.Model(userInfo: !userInfo.name.isEmpty ? userInfo.name : "Добавить имя")),
				Cell(data: ProfileTableCell.Model(userInfo: userInfo.phone)),
				Cell(data: ProfileTableCell.Model(userInfo: userInfo.address))
			])
		]
		reloadData()
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = super.tableView(tableView, cellForRowAt: indexPath)
		if indexPath.row == 1 {
			cell.accessoryType = .none
		}

		return cell
	}
}
