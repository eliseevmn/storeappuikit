//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class ProfileView: UIView {

	// MARK: - Private Properties

	private(set) lazy var exitButton: UIButton = {
		let button = UIButton(type: .system)
		button.backgroundColor = .systemBackground
		button.setTitle("Выйти", for: .normal)
		button.setTitleColor(AppColor.grey, for: .normal)
		button.titleLabel?.font = AppFont.montserratFont(ofSize: 12, weight: .regular)
		button.addTarget(self, action: #selector(exitButtonTapped), for: .touchUpInside)
		return button
	}()

	// MARK: - Public Properties

	let profileTable = ProfileTableView()

	// MARK: - Initialisers

	override init(frame: CGRect) {
		super.init(frame: frame)

		backgroundColor = .systemBackground
		setupLayout()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private Methods

	private func setupLayout() {
		addSubview(profileTable)
		addSubview(exitButton)

		profileTable.snp.makeConstraints {
			$0.top.equalTo(safeAreaLayoutGuide)
			$0.leading.equalTo(self)
			$0.trailing.equalTo(self)
			$0.height.equalTo(132)
		}

		exitButton.snp.makeConstraints {
			$0.top.equalTo(profileTable.snp.bottom).offset(24)
			$0.leading.equalTo(self).offset(20)
		}
	}

	@objc private func exitButtonTapped() {
		print("saveButton tapped")
	}

}
