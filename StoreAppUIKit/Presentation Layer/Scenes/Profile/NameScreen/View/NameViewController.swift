//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

class NameViewController: UIViewController {

	private lazy var nameView = view as? NameView

	// MARK: - Public Properties

	let nameViewModel: NameViewModel
	var subscriptions = Set<AnyCancellable>()

	// MARK: - Initializers

	init(viewModel: NameViewModel /*provider: NameProvider*/) {
		self.nameViewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Lifecycle

	override func loadView() {
		view = NameView()
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Добавить имя"
//		navigationItem.largeTitleDisplayMode = .automatic
		setupViewModelBindings()

		nameViewModel.requestData()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationItem.largeTitleDisplayMode = .never
	}

	// MARK: - Private Methods

	private func setupViewModelBindings() {

		// view to viewModel

		nameView?.saveButton.tapPublisher
			.sink(receiveValue: { [weak self] _ in
				self?.nameViewModel.firstname = self?.nameView?.nameTextField.text ?? ""
				self?.nameViewModel.lastname = self?.nameView?.surnameTextField.text ?? ""
				self?.nameViewModel.saveButtonDidTap()
			})
			.store(in: &subscriptions)

		nameView?.nameTextField.textPublisher
			.assign(to: \.firstname, on: nameViewModel)
			.store(in: &subscriptions)
		nameView?.surnameTextField.textPublisher
			.assign(to: \.lastname, on: nameViewModel)
			.store(in: &subscriptions)

		// viewModel to view

		nameViewModel.updateProfileData = { [weak self] user in
			DispatchQueue.main.async {
				self?.nameView?.nameTextField.text = user.firstName
				self?.nameView?.surnameTextField.text = user.lastName
			}
		}
	}

	@objc private func saveButtonDidTap() {
		nameViewModel.saveButtonDidTap()
	}

}
