//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class NameView: UIView {

	// MARK: - Public Properties

	private(set) lazy var nameTextField: STextField = {
		let textField = STextField()
		textField.placeholder = "Имя"
		textField.addTarget(self, action: #selector(nameTextDidChange), for: .editingDidEnd)

		return textField
	}()

	private(set) lazy var surnameTextField: STextField = {
		let textField = STextField()
		textField.placeholder = "Фамилия"
		textField.addTarget(self, action: #selector(surnameTextDidChange), for: .editingDidEnd)

		return textField
	}()

	private(set) lazy var saveButton = SaveButton()

	var nameTextChange: ((String) -> Void)?
	var surnameTextChange: ((String) -> Void)?

	// MARK: - Initializers

	override init(frame: CGRect) {
		super.init(frame: frame)
		backgroundColor = .systemBackground

        setupLayout()

		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UIView.endEditing(_:)))
		self.addGestureRecognizer(tapGesture)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private Methods

	@objc private func nameTextDidChange(_ textField: UITextField) {
		if let text = textField.text {
			nameTextChange?(text)
		}
	}
	@objc private func surnameTextDidChange(_ textField: UITextField) {
		if let text = textField.text {
			surnameTextChange?(text)
		}
	}

	private func setupLayout() {
		addSubview(nameTextField)
		addSubview(surnameTextField)
		addSubview(saveButton)

		nameTextField.snp.makeConstraints {
			$0.top.equalTo(self.safeAreaLayoutGuide).offset(24)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(350)
			$0.height.equalTo(44)
		}

		surnameTextField.snp.makeConstraints {
			$0.top.equalTo(nameTextField.snp.bottom).offset(24)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(350)
			$0.height.equalTo(44)
		}
		saveButton.snp.makeConstraints {
			$0.top.equalTo(surnameTextField.snp.bottom).offset(48)
			$0.centerX.equalTo(self.snp.centerX)
			$0.width.equalTo(130)
			$0.height.equalTo(44)
		}
	}

}
