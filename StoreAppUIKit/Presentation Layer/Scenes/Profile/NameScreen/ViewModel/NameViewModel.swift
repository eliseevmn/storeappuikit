//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

final class NameViewModel {

	// MARK: - Private Properties

	private let userService: UserServiceProtocol?
	private var user: User? {
		didSet {
			updateProfileData?(user!)
		}
	}
	private var nameData = ["first_name": "",
							"last_name": ""]
	private let userID: Int
	private var subscriptions: AnyCancellable?

	// MARK: - Public Properties

	var isFinishScreen: ((Bool) -> Void)?
	var updateProfileData: ((User) -> Void)?

	@Published var firstname: String = ""
	@Published var lastname: String = ""

	// MARK: - Initializers

	init(service: UserServiceProtocol) {
		self.userService = service
		self.userID = service.userId
	}

	// MARK: - Public Methods

	func saveButtonDidTap() {
		updateUserToPost()
		updateUser(with: nameData)
	}

	func requestData() {
		getUser(for: userID)
	}

	// MARK: - Private Methods

	private func updateUserToPost() {
		nameData["first_name"] = firstname
		nameData["last_name"] = lastname
	}

	private func getUser(for id: Int) {
		subscriptions = userService?.getUser()
			.sink(receiveCompletion: { completion in
				switch completion {
				case .finished:
					print("")
				case let .failure(error):
					print(error)
				}
			}, receiveValue: { [weak self] user in
				self?.user = user
				self?.nameData["first_name"] = user.firstName
				self?.nameData["last_name"] = user.lastName
			})
	}

	private func updateUser(with data: [String: String]) {
		subscriptions = userService?.updateUser(with: data)
			.sink(receiveCompletion: { [weak self] completion in
				switch completion {
				case .finished:
					self?.isFinishScreen?(true)
				case let .failure(error):
					print("❌\(error)")
				}
			}, receiveValue: { _ in })
	}

}
