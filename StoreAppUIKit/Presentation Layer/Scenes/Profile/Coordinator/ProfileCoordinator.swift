//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class ProfileCoordinator: BaseCoordinator {

    // MARK: - Private Properties

    private let moduleFactory: ModuleFactoryProtocol
    private let router: Router

    // MARK: - Public Properties

    var finishFlow: ((Bool) -> Void)?

    // MARK: - Initialisers

    init(router: Router, moduleFactory: ModuleFactoryProtocol) {
        self.moduleFactory = moduleFactory
        self.router = router
    }

    // MARK: - Public Methods

    override func start() {
        print(String(describing: Self.self), "started")
        showProfileScreen()
    }

    // MARK: - Private Methods

    private func showProfileScreen() {
        let profileScreen = moduleFactory.makeProfileModule()
        router.push(profileScreen)

		profileScreen.profileViewModel.onNameScreen = { [weak self] _ in
			self?.showNameScreen()
		}
		profileScreen.profileViewModel.onAddressScreen = { [weak self] _ in
			self?.showAddressScreen()
		}
		profileScreen.profileViewModel.isLogin = { [weak self] isLogin in
			self?.finishFlow?(isLogin)
		}
    }

	private func showNameScreen() {
		let nameScreen = moduleFactory.makeNameModule()
		nameScreen.navigationItem.largeTitleDisplayMode = .never
		router.push(nameScreen)
		nameScreen.nameViewModel.isFinishScreen = { [weak self] _ in
			DispatchQueue.main.async {
				self?.router.pop(animated: true)
			}
		}
	}

	private func showAddressScreen() {
		let addressScreen = moduleFactory.makeAddressModule()
		router.push(addressScreen)
		addressScreen.addressViewModel.isFinishScreen = { [weak self] _ in
			DispatchQueue.main.async {
				self?.router.pop(animated: true)
			}
		}
	}

}
