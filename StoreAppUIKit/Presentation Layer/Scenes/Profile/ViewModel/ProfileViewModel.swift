//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

final class ProfileViewModel {

	enum TypeOfScreen: Int, CaseIterable {
		case nameInput = 0
		case addressInput = 2
	}

	// MARK: - Public Properties

	var onNameScreen: ((Bool) -> Void)?
	var onAddressScreen: ((Bool) -> Void)?
	var profileUserData: ((ProfileUserInfo) -> Void)?

	var isLogin: ((Bool) -> Void)?

	// MARK: - Private Properties

	private let profileService: ProfileServiceProtocol?
	private var profileUser: ProfileUserInfo? {
		didSet {
			guard let profileUser = profileUser else { return }
			profileUserData?(profileUser)
		}
	}
	private let userID: Int
	private var subscriptions: AnyCancellable?

	// MARK: - Initializers

	init(service: ProfileServiceProtocol) {
		self.profileService = service
		self.profileUser = ProfileUserInfo(name: "нонейм", phone: "ноуфон", address: "ноуадрес")
		self.userID = service.userId
	}

	// MARK: - Public Methods

	func requestData() {
		doUserNetwork(for: 9)
	}

	func screenToShow(by indexPath: Int) {
		let typeOfScreen = TypeOfScreen(rawValue: indexPath)
		switch typeOfScreen {
		case .nameInput:
			onNameScreen?(true)
		case .addressInput:
			onAddressScreen?(true)
		default:
			print("default")
		}
	}

	func exitButtonDidTapped() {
		profileService?.removeToken()
		isLogin?(false)
	}

	// MARK: - Private Methods

	private func doUserNetwork(for id: Int) {
		subscriptions = profileService?.getUser(by: id)
			.zip((profileService?.getAddresses())!)
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { completion in
				switch completion {
				case .finished:
					print("")
				case let .failure(error):
					print(error)
				}
			}, receiveValue: { [weak self] user in
				self?.profileUser?.name = user.0.fullName
				self?.profileUser?.phone = user.0.phone
				self?.profileUser?.address = user.1.first?.fullAddress ?? "Добавить адрес"
			})
	}

}
