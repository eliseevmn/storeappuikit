//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

class BasketViewController: UIViewController {

	// MARK: - Public Properties

	let basketViewModel: BasketViewModel

	// MARK: - Private Properties

	private var basketView: BasketView!
	private var subscriptions = Set<AnyCancellable>()

	// MARK: - Initialisers

	init(viewModel: BasketViewModel) {
		self.basketViewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Lifecycle

	override func loadView() {
		view = BasketView()
		if let basketView = view as? BasketView {
			self.basketView = basketView
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		basketViewModel.viewDidLoad.send()
		view.backgroundColor = AppColor.white
		configureNavigationBar()
		setupBindings()
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()

		basketView.basketTableView.updateHeaderViewHeigh()
		basketView.basketTableView.updateFooterViewHeigh()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		basketViewModel.viewWillAppear.send()
	}

	// MARK: - Private Methods

	private func setupBindings() {

		basketView.basketTableView.basketTableFooterView?.clearButton.addTarget(self, action: #selector(clearButtonDidTap), for: .touchUpInside)

		basketView.basketTableView.basketTableFooterView?.placeOrderButton.addTarget(self, action: #selector(placeOrderButtonDidTap), for: .touchUpInside)

		basketViewModel.$basket.sink(receiveValue: { [unowned self] basket in
			self.basketView.basketTableView.setContent(with: basket)
		})
		.store(in: &subscriptions)

		basketViewModel.$totalBasketInfo.sink(receiveValue: { [unowned self] basket in
			self.basketView.basketTableView.updateTotalBasketInfo(with: basket)
		})
		.store(in: &subscriptions)

		basketViewModel.$deliveryCost.sink(receiveValue: { [unowned self] delivery in
			self.basketView.basketTableView.updateTotalOrderInfo(with: delivery)
		})
		.store(in: &subscriptions)

		basketViewModel.$deliverySettings.sink(receiveValue: { [unowned self] settings in
			self.basketView.basketTableView.basketTableFooterView?.deliveryTableView.setContent(with: settings)
		})
		.store(in: &subscriptions)

		basketView.basketTableView.didSelectRowAt = { [unowned self] cellRow in
			self.basketViewModel.basketTableViewDidSelectRowAt.send(cellRow)
		}

		basketView.basketTableView.onDeleteButtonDidTap = { [unowned self] cellRow in
			self.basketViewModel.basketTableViewDeleteButtonDidTap.send(cellRow)
		}

		basketView.basketTableView.basketTableFooterView?.deliveryTableView.onDidSelectScreen = { [unowned self] selectedScreen in
			self.basketViewModel.deliveryTableViewDidSelectScreen.send(selectedScreen)
		}

		basketView.basketTableView.onStepperButtonDidTap = { [unowned self] (row, count) in
			self.basketViewModel.basketTableViewStepperButtonDidTap.send((row, count))
		}

		basketViewModel.$isLoadingIndicator.sink(receiveValue: { [unowned self] state in
			if state {
				self.basketView.activityIndicator.startAnimating()
			} else {
				self.basketView.activityIndicator.stopAnimating()
			}
		})
		.store(in: &subscriptions)

		basketViewModel.$isPlaceOrderButtonEnabled.sink(receiveValue: { [unowned self] state in
			self.basketView.basketTableView.basketTableFooterView?.placeOrderButton.isEnabled = state
			self.basketView.basketTableView.basketTableFooterView?.placeOrderButton.alpha = !state ? 0.6 : 1
		})
		.store(in: &subscriptions)
	}

	@objc func clearButtonDidTap() {
		basketViewModel.clearBasket()
	}

	@objc func placeOrderButtonDidTap() {
		basketViewModel.placeOrder()
	}

	private func configureNavigationBar() {
		navigationController?.navigationBar.prefersLargeTitles = true
		navigationItem.title = "Корзина"
	}
}
