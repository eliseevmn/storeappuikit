//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class BasketView: UIView {

	// MARK: - Outlets

	private(set) lazy var activityIndicator: UIActivityIndicatorView = {
		let indicator = UIActivityIndicatorView(style: .large)

		return indicator
	}()

	private(set) lazy var basketTableView: BasketTableView = {
		let tableView = BasketTableView()
		tableView.contentInsetAdjustmentBehavior = .never
		tableView.separatorStyle = .none
		tableView.estimatedRowHeight = 100
		tableView.rowHeight = UITableView.automaticDimension
		tableView.registerClass(BasketTableViewCell.self)

		let headerView = BasketTableHeaderView()
		headerView.translatesAutoresizingMaskIntoConstraints = false
		tableView.tableHeaderView = headerView

		let footerView = BasketTableFooterView()
		footerView.translatesAutoresizingMaskIntoConstraints = false
		tableView.tableFooterView = footerView

		tableView.translatesAutoresizingMaskIntoConstraints = false

		return tableView
	}()

	// MARK: - Initializers

	override init(frame: CGRect) {
		super.init(frame: frame)
		backgroundColor = UIColor.systemPink.withAlphaComponent(0.1)
		setupLayout()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private Methods

	private func setupLayout() {

		addSubviews(basketTableView, activityIndicator)

		basketTableView.snp.makeConstraints { (make) in
			make.edges.equalTo(safeAreaLayoutGuide).inset(UIEdgeInsets(top: 0.0,
																	   left: 0.0,
																	   bottom: 0.0,
																	   right: 0.0))
		}

		activityIndicator.snp.makeConstraints {
			$0.center.equalToSuperview()
		}
	}

}
