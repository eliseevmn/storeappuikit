//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class BasketTableHeaderView: UIView {

	// MARK: - Outlets

	private lazy var productQuantityLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 14, weight: .medium)
		label.textColor = AppColor.grey
		label.text = "В корзине \(productQuantity) товар(а)"
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	// MARK: - Public Properties

	var productQuantity: Int = 0 {
		didSet {
			productQuantityLabel.text = "В корзине \(productQuantity) товар(а)"
		}
	}

	// MARK: - Initializers

	override init(frame: CGRect) {
		super.init(frame: frame)

		setupLayout()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private Methods

	private func setupLayout() {

		addSubview(productQuantityLabel)

		productQuantityLabel.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(15)
			make.bottom.equalToSuperview().offset(-15)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().offset(-20)
		}
	}
}
