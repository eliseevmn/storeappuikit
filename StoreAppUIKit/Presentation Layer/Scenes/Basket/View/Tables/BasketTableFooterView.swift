//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class BasketTableFooterView: UIView {

	// MARK: - Outlets

	private(set) lazy var clearButton: UIButton = {
		let button = UIButton(type: .system)
		button.setTitle("Очистить", for: .normal)
		button.setTitleColor(AppColor.grey, for: .normal)
		button.titleLabel?.font = AppFont.montserratFont(ofSize: 14, weight: .medium)
		button.translatesAutoresizingMaskIntoConstraints = false

		return button
	}()

	private(set) lazy var basketWeightLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 16, weight: .medium)
		label.textColor = AppColor.black
		label.text = "Вес заказа: "
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private(set) lazy var basketCostLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 20, weight: .medium)
		label.textColor = AppColor.black
		label.text = "Стоимость заказа: "
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private lazy var descriptionLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 14, weight: .regular)
		label.textColor = AppColor.grey
		label.text = "Стоимость весового товара может быть пересчитана"
		label.numberOfLines = 0
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private lazy var basketInfoStackView: UIStackView = {
		let stack = UIStackView(arrangedSubviews: [basketWeightLabel, basketCostLabel, descriptionLabel])
		stack.axis = .vertical
		stack.alignment = .leading
		stack.distribution = .equalSpacing
		stack.spacing = 5.0
		stack.translatesAutoresizingMaskIntoConstraints = false

		return stack
	}()

	// Delivery part
	private(set) lazy var deliveryTableView: DeliveryTableView = {
		let tableView = DeliveryTableView()
		tableView.separatorStyle = .none
		tableView.isScrollEnabled = false
		tableView.estimatedRowHeight = 44
		tableView.rowHeight = UITableView.automaticDimension
		tableView.registerClass(DeliveryTableViewCell.self)
		tableView.estimatedSectionHeaderHeight = 100
		tableView.sectionHeaderHeight = UITableView.automaticDimension
		tableView.translatesAutoresizingMaskIntoConstraints = false

		return tableView
	}()

	private(set) lazy var deliveryCostLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 20, weight: .medium)
		label.textColor = AppColor.grey
		label.text = "Стоимость доставки: "
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	// Ordering
	private lazy var totalCostLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 24, weight: .medium)
		label.textColor = AppColor.orange
		label.text = "Итого к оплате: "
		label.numberOfLines = 0
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private lazy var orderCostInfoStackView: UIStackView = {
		let stack = UIStackView(arrangedSubviews: [deliveryCostLabel, totalCostLabel])
		stack.axis = .vertical
		stack.alignment = .leading
		stack.distribution = .fill
		stack.spacing = 30.0
		stack.translatesAutoresizingMaskIntoConstraints = false

		return stack
	}()

	private(set) lazy var placeOrderButton: BaseButton = {
		let button = BaseButton(type: .system)
		button.setTitle("Оформить заказ", for: .normal)
		button.setTitleColor(AppColor.white, for: .normal)
		button.titleLabel?.font = AppFont.montserratFont(ofSize: 16, weight: .semiBold)

		return button
	}()

	// MARK: - Public Properties

	private var basketCost: Double = 0.0

	// MARK: - Initializers

	override init(frame: CGRect) {
		super.init(frame: frame)

		setupLayout()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Public Methods

	func setTotalBasketInfo(with data: Basket) {
		basketWeightLabel.text = "Вес заказа: \(String(format: "%.1f", data.totalWeight)) кг"
		basketCostLabel.text = "Стоимость заказа: \(String(format: "%.2f", data.totalCost)) \u{20bd}"
	}

	func setTotalOrderInfo(with data: OrderCalculateResponse) {
		let totalCost = data.paymentAmount + data.basketSumm
		deliveryCostLabel.text = "Стоимость доставки: \(String(format: "%.2f", data.paymentAmount)) \u{20bd}"
		totalCostLabel.text = "Итого к оплате: \(String(format: "%.2f", totalCost)) \u{20bd}"
	}

	// MARK: - Private Methods

	private func setupLayout() {

		addSubviews(clearButton,
					basketInfoStackView,
					deliveryTableView,
					orderCostInfoStackView,
					placeOrderButton)

		clearButton.snp.makeConstraints { make in
			make.top.equalToSuperview().offset(2)
			make.leading.equalToSuperview().offset(20)
			make.size.equalTo(CGSize(width: 70, height: 25))
		}

		basketInfoStackView.snp.makeConstraints { make in
			make.top.equalTo(clearButton.snp.bottom).offset(25)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().offset(-20)
			make.height.equalTo(95)
		}

		deliveryTableView.snp.makeConstraints { make in
			make.top.equalTo(basketInfoStackView.snp.bottom).offset(65)
			make.leading.equalToSuperview()
			make.trailing.equalToSuperview()
			make.height.equalTo(75)
		}

		orderCostInfoStackView.snp.makeConstraints { make in
			make.top.equalTo(deliveryTableView.snp.bottom).offset(5)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().offset(-20)
		}

		placeOrderButton.snp.makeConstraints { make in
			make.top.equalTo(orderCostInfoStackView.snp.bottom).offset(20)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().offset(-20)
			make.bottom.equalToSuperview().offset(-30)
			make.height.equalTo(43)
		}
	}

}
