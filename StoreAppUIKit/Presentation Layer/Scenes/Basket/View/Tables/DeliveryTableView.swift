//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

extension DeliveryTableView {
    enum DeliveryScreen: CaseIterable {
        case address
        case time
        case payment
    }
}

final class DeliveryTableView: UITableView {

    // MARK: - Public Properties

    var data: [String] = []
    var onDidSelectScreen: ((DeliveryScreen) -> Void)?

    // MARK: - Initializers

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)

        dataSource = self
        delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setContent(with data: Delivery) {
		self.data = [data.address.fullAddress, data.deliveryTime, data.paymentOption.rawValue]
        reloadData()
    }
}

// MARK: - UITableViewDataSource

extension DeliveryTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: DeliveryTableViewCell = tableView.cell(forRowAt: indexPath) else { return UITableViewCell() }
        let item = data[indexPath.row]
        cell.setup(with: item)
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Доставка"
    }
}

// MARK: - UITableViewDelegate

extension DeliveryTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let headerView = view as? UITableViewHeaderFooterView else { return }
        headerView.contentView.backgroundColor = AppColor.white
        headerView.textLabel?.textColor = AppColor.black
        headerView.textLabel?.font = AppFont.montserratFont(ofSize: 28, weight: .medium)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onDidSelectScreen?(DeliveryScreen.allCases[indexPath.row])
    }
}

// MARK: - DeliveryTableViewCell

final class DeliveryTableViewCell: UITableViewCell {

	// MARK: - Outlets

    private(set) lazy var deliveryTextLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.montserratFont(ofSize: 16, weight: .regular)
        label.textColor = AppColor.black
        label.text = "Place holder"
        label.translatesAutoresizingMaskIntoConstraints = false

        return label
    }()

    // MARK: - Initialisers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none
        accessoryType = .disclosureIndicator
        // setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setup(with data: String) {
        textLabel?.text = data
        deliveryTextLabel.text = data
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(deliveryTextLabel)

        deliveryTextLabel.snp.makeConstraints { make in
            make.leading.equalTo(contentView).offset(20)
            make.trailing.equalTo(contentView).offset(-20)
            make.centerY.equalTo(contentView)
        }
    }

}
