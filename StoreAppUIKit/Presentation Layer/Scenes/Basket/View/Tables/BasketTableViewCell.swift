//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import SwiftUI
import SDWebImage

final class BasketTableViewCell: UITableViewCell {

	// MARK: - Outlets

	// Header part
	private(set) lazy var productNameLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 18, weight: .regular)
		label.textColor = AppColor.black
		label.text = "ProductName placeholder"
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private(set) lazy var priceTypeLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 14, weight: .regular)
		label.textColor = AppColor.black
		label.text = "ProductWeight placeholder г"
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private lazy var productTitleStackView: UIStackView = {
		let stack = UIStackView(arrangedSubviews: [productNameLabel, priceTypeLabel])
		stack.axis = .vertical
		stack.alignment = .leading
		stack.distribution = .equalSpacing
		stack.spacing = 1.0
		stack.translatesAutoresizingMaskIntoConstraints = false

		return stack
	}()

	private(set) lazy var deleteButton: UIButton = {
		let button = UIButton(type: .system)
		button.setImage(UIImage(named: "icTrash"), for: .normal)
		button.tintColor = AppColor.black
		button.translatesAutoresizingMaskIntoConstraints = false

		return button
	}()

	// Middle part
	private(set) lazy var productImageView: UIImageView = {
		let imageView = UIImageView()
		imageView.image = UIImage(named: "mushroom")
		imageView.contentMode = .scaleAspectFit
		imageView.translatesAutoresizingMaskIntoConstraints = false

		return imageView
	}()

	private(set) lazy var brandNameLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 16, weight: .regular)
		label.textColor = AppColor.black
		label.text = "BrandName placeholder /n Country"
		label.numberOfLines = 0
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private(set) lazy var shopNameLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 16, weight: .regular)
		label.textColor = AppColor.black
		label.text = "ShopName placeholder"
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	private lazy var productInfoStackView: UIStackView = {
		let stack = UIStackView(arrangedSubviews: [brandNameLabel, shopNameLabel])
		stack.axis = .vertical
		stack.alignment = .leading
		stack.distribution = .equalSpacing
		stack.spacing = 30.0
		stack.translatesAutoresizingMaskIntoConstraints = false

		return stack
	}()

	// Footer part
	private(set) lazy var stepperButton: StepperField = {
		let stepper = StepperField()
		stepper.translatesAutoresizingMaskIntoConstraints = false

		return stepper
	}()

	private(set) lazy var priceLabel: UILabel = {
		let label = UILabel()
		label.font = AppFont.montserratFont(ofSize: 18, weight: .semiBold)
		label.textColor = AppColor.black
		label.text = "ShopName placeholder"
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	// MARK: - Initialisers

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)

		selectionStyle = .none
		setupLayout()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Public Methods

	override func prepareForReuse() {
		super.prepareForReuse()
//		stepperButton.
	}

	func setup(with data: Basket.Product) {
		productNameLabel.text = data.name
		priceTypeLabel.text = data.priceType
		productImageView.sd_setImage(with: URL(string: data.images[0]), placeholderImage: UIImage(named: "icDefaultFood"))
		brandNameLabel.text = "\(data.productInformation.brand)\n\(data.productInformation.manufacturingCountry)"
		shopNameLabel.text = data.store
		stepperButton.quantity = data.quantity
		priceLabel.text = "\(data.price) \u{20bd}"
	}

	// MARK: - Private Methods

	private func setupLayout() {
		contentView.addSubviews(productTitleStackView,
								deleteButton,
								productImageView,
								productInfoStackView,
								stepperButton,
								priceLabel)

		productTitleStackView.snp.makeConstraints { make in
			make.top.equalTo(contentView).offset(5)
			make.leading.equalTo(contentView).offset(20)
		}

		deleteButton.snp.makeConstraints { make in
			make.top.equalTo(productTitleStackView.snp.top).offset(0)
			make.leading.equalTo(productTitleStackView.snp.trailing).offset(15)
			make.trailing.equalTo(contentView).offset(-20)
			make.size.equalTo(CGSize(width: 22, height: 29))
		}

		productImageView.snp.makeConstraints { make in
			make.top.equalTo(productTitleStackView.snp.bottom).offset(15)
			make.leading.equalTo(productTitleStackView.snp.leading).offset(0)
			make.size.equalTo(CGSize(width: 175, height: 105))
		}

		productInfoStackView.snp.makeConstraints { make in
			make.top.equalTo(productImageView.snp.top).offset(0)
			make.leading.equalTo(productImageView.snp.trailing).offset(20)
			make.trailing.equalTo(deleteButton.snp.trailing).offset(-0)
		}

		stepperButton.snp.makeConstraints { make in
			make.top.equalTo(productImageView.snp.bottom).offset(25)
			make.leading.equalTo(productTitleStackView.snp.leading).offset(0)
			make.size.equalTo(CGSize(width: 88, height: 32))
		}

		priceLabel.snp.makeConstraints { make in
			make.leading.equalTo(productInfoStackView.snp.leading).offset(0)
			make.trailing.equalTo(deleteButton).offset(-0)
			make.centerY.equalTo(stepperButton.snp.centerY)
			make.bottom.equalToSuperview().offset(-30)
		}
	}

}

#if DEBUG

struct BasketCellPreview: PreviewProvider {
	static var previews: some View {
		return BasketTableViewCell().preview
	}
}
#endif
