//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class BasketTableView: UITableView {

	// MARK: - Public Properties

	var data: [Basket.Product] = []
	var didSelectRowAt: ((Int) -> Void)?
	var onDeleteButtonDidTap: ((Int) -> Void)?
	var onStepperButtonDidTap: ((Int, Int) -> Void)?

	lazy var basketTableHeaderView: BasketTableHeaderView? = {
		return self.tableHeaderView as? BasketTableHeaderView
	}()

	lazy var basketTableFooterView: BasketTableFooterView? = {
		return self.tableFooterView as? BasketTableFooterView
	}()

	// MARK: - Initialisers

	override init(frame: CGRect, style: UITableView.Style) {
		super.init(frame: frame, style: style)

		dataSource = self
		delegate = self
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Public Methods

	func setContent(with data: Basket) {
		self.data = data.products
		reloadData()
	}

	func updateTotalBasketInfo(with data: Basket) {
		basketTableHeaderView?.productQuantity = data.totalQuantity
		basketTableFooterView?.setTotalBasketInfo(with: data)
	}

	func updateTotalOrderInfo(with data: OrderCalculateResponse) {
		basketTableFooterView?.setTotalOrderInfo(with: data)
	}

	// MARK: - Private Methods

	@objc private func deleteButtonDidTap(_ sender: UIButton) {
		let buttonPosition = sender.convert(sender.bounds.origin, to: self)
		guard let indexPath = indexPathForRow(at: buttonPosition) else { return }
		onDeleteButtonDidTap?(indexPath.row)
	}
}

	// MARK: - UITableViewDataSource

extension BasketTableView: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		data.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell: BasketTableViewCell = tableView.cell(forRowAt: indexPath) else { return UITableViewCell() }
		let product = data[indexPath.row]
		cell.setup(with: product)
		cell.stepperButton.onChange = {[weak self] count in
			self?.onStepperButtonDidTap?(indexPath.row, count)
		}
		cell.deleteButton.addTarget(self, action: #selector(deleteButtonDidTap(_:)), for: .touchUpInside)
		return cell
	}
}

	// MARK: - UITableViewDelegate

extension BasketTableView: UITableViewDelegate {

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		didSelectRowAt?(indexPath.row)
	}
}

extension BasketTableView {
	func updateHeaderViewHeigh() {
		guard let headerView = self.tableHeaderView else { return }
		let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
		if headerView.frame.size.height != height {
			headerView.frame.size.height = height
			self.tableHeaderView = headerView
		}
		headerView.translatesAutoresizingMaskIntoConstraints = true
	}

	func updateFooterViewHeigh() {
		guard let footerView = self.tableFooterView as? BasketTableFooterView else { return }
		let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
		if footerView.frame.size.height != height {
			footerView.frame.size.height = height
			self.tableFooterView = footerView
		}
		footerView.translatesAutoresizingMaskIntoConstraints = true
	}

}
