//
//  BasketViewModel.swift
//  Sarawan
//
//  Created by MacBook Pro on 26.10.2021.
//

import Foundation
import Combine
import UIKit

protocol BasketViewModelProtocol {

	// Input
	var basketTableViewStepperButtonDidTap: PassthroughSubject<(Int, Int), Never> { get }
	var basketTableViewDidSelectRowAt: PassthroughSubject<Int, Never> { get }
	var basketTableViewDeleteButtonDidTap: PassthroughSubject<Int, Never> { get }
	var deliveryTableViewDidSelectScreen: PassthroughSubject<DeliveryTableView.DeliveryScreen, Never> { get }
	var viewDidLoad: PassthroughSubject<Void, Never> { get }
	var viewWillAppear: PassthroughSubject<Void, Never> { get }

	// Output
	var deliveryCost: OrderCalculateResponse { get }
	var totalBasketInfo: Basket { get }
	var basket: Basket { get }
	var deliverySettings: Delivery { get }
	var isLoadingIndicator: Bool { get }
	var isPlaceOrderButtonEnabled: Bool { get }
}

final class BasketViewModel: BasketViewModelProtocol {

	// MARK: - Public Properties

	// Input
	let basketTableViewStepperButtonDidTap = PassthroughSubject<(Int, Int), Never>()
	let basketTableViewDidSelectRowAt = PassthroughSubject<Int, Never>()
	let basketTableViewDeleteButtonDidTap = PassthroughSubject<Int, Never>()
	let deliveryTableViewDidSelectScreen = PassthroughSubject<DeliveryTableView.DeliveryScreen, Never>()
	let viewDidLoad = PassthroughSubject<Void, Never>()
	let viewWillAppear = PassthroughSubject<Void, Never>()

	// Output
	@Published private(set) var deliveryCost = OrderCalculateResponse()
	@Published private(set) var totalBasketInfo = Basket()
	@Published private(set) var basket = Basket()
	@Published private(set) var deliverySettings = Delivery()
	@Published private(set) var isLoadingIndicator = true
	@Published private(set) var isPlaceOrderButtonEnabled = false

	// Navigation
	var onSelectDeliveryScreen: ((DeliveryTableView.DeliveryScreen) -> Void)?
	var onSelectProductCardScreen: ((Int, Int) -> Void)?

	// MARK: - Private Properties

	private var subscriptions = Set<AnyCancellable>()
	private let basketService: BasketServiceProtocol
	private var updateQuantityCancellable: AnyCancellable?
	private var addressId: Int { deliverySettings.address.addressId }

	// MARK: - Initialisers

	init(service: BasketServiceProtocol) {
		basketService = service

		bindOnViewWillAppear()
		bindOnBasketTableViewDeleteButtonDidTap()
		bindOnDeliveryTableViewDidSelectScreen()
		bindOnBasketTableViewDidSelectRow()
		bindOnBasketTableViewStepperButtonDidTap()
	}

	// MARK: - Public Methods

	func placeOrder() {
		isLoadingIndicator = true
		isPlaceOrderButtonEnabled = false
		basketService.makeOrder(forAddress: addressId)
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: {[unowned self] completion in
				switch completion {
				case .finished:
					self.basket = Basket()
					self.totalBasketInfo = Basket()
					self.deliveryCost = OrderCalculateResponse()
					self.isLoadingIndicator = false
				case .failure(let error):
					debugPrint("place order get error:", error)
					self.isLoadingIndicator = false
					self.isPlaceOrderButtonEnabled = true
				}
			}, receiveValue: {_ in	})
			.store(in: &subscriptions)
	}

	func clearBasket() {
		isLoadingIndicator = true
		isPlaceOrderButtonEnabled = false
		basketService.clear()
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { [unowned self] completion in
				switch completion {
				case .finished:
					self.basket = Basket()
					self.totalBasketInfo = Basket()
					self.deliveryCost = OrderCalculateResponse()
					self.isLoadingIndicator = false
				case .failure(let error):
					self.isLoadingIndicator = false
					self.isPlaceOrderButtonEnabled = true
					debugPrint("basket clear error:", error)
				}
			}, receiveValue: {_ in })
			.store(in: &subscriptions)
	}

	// MARK: - Private Methods

	private func bindOnBasketTableViewDeleteButtonDidTap() {
		basketTableViewDeleteButtonDidTap.sink { [unowned self] cellRow in
			self.deleteProductFromBasket(cellRow: cellRow)
		}
		.store(in: &subscriptions)
	}

	private func deleteProductFromBasket(cellRow: Int) {
		isLoadingIndicator = true
		isPlaceOrderButtonEnabled = false
		let basketProductId = basket.products[cellRow].basketProductId
		basketService.remove(productBy: basketProductId)
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { [unowned self] completion in
				switch completion {
				case .finished:
					self.basket.products.remove(at: cellRow)
					self.refreshBasket()
				case .failure(let error):
					debugPrint("basket delete error:", error)
					self.isLoadingIndicator = false
				}
			}, receiveValue: {_ in })
			.store(in: &subscriptions)
	}

	private func bindOnBasketTableViewStepperButtonDidTap() {
		basketTableViewStepperButtonDidTap
			.debounce(for: .seconds(0.5), scheduler: RunLoop.main)
			.sink { [unowned self] (row, count) in
				print("send request with count:", count)
				updateProductQuantity(for: row, count: count)
			}
			.store(in: &subscriptions)
	}

	private func updateProductQuantity(for row: Int, count: Int) {
		self.isPlaceOrderButtonEnabled = false
		updateQuantityCancellable?.cancel()
		let productStoreId = basket.products[row].productStoreId
		updateQuantityCancellable = basketService.update(basketWith: (productId: productStoreId, quantity: count))
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { [unowned self] completion in
				switch completion {
				case .finished:
					self.basket.products[row].quantity = count
					self.refreshBasket()
				case .failure(let error):
					isLoadingIndicator = false
					debugPrint("basket update error:", error)
				}
			}, receiveValue: {_ in	})
	}

	private func bindOnBasketTableViewDidSelectRow() {
		basketTableViewDidSelectRowAt.sink { [unowned self] (cellRow) in
			let basketProductId = basket.products[cellRow].basketProductId
			let quantity = basket.products[cellRow].quantity
			onSelectProductCardScreen?(basketProductId, quantity)
		}
		.store(in: &subscriptions)
	}

	private func bindOnDeliveryTableViewDidSelectScreen() {
		deliveryTableViewDidSelectScreen.sink { [unowned self] selectedScreen in
			onSelectDeliveryScreen?(selectedScreen)
		}
		.store(in: &subscriptions)
	}

	private func getUserAddress() {
		basketService.getUserData()
			.receive(on: RunLoop.main)
			.sink(receiveCompletion: { completion in
				switch completion {
				case .finished:
					break
				case .failure(let error):
					debugPrint("get user data error:", error)
				}
			}, receiveValue: { [unowned self] userData in
				self.deliverySettings.address = userData[0].address[0]
			})
			.store(in: &subscriptions)
	}

	private func bindOnViewWillAppear() {
		viewWillAppear.sink { [unowned self] _ in
			if addressId == 0 { self.getUserAddress() }
			self.refreshBasket()
		}
		.store(in: &subscriptions)
	}

	private func refreshBasket() {
		var basketResponse = Basket()
		self.isLoadingIndicator = true
		self.isPlaceOrderButtonEnabled = false
		self.basketService.getBasket()
			.flatMap { [unowned self] (basket) -> AnyPublisher<OrderCalculateResponse, Error> in
				if basket.totalQuantity > 0 {
					basketResponse = basket
					return self.basketService.getOrderDeliveryCost(forAddress: addressId)
				} else {
					return Just(OrderCalculateResponse())
						.setFailureType(to: Error.self)
						.eraseToAnyPublisher()
				}
			}
			.receive(on: DispatchQueue.main)
			.sink(receiveCompletion: { [unowned self] (completion) in
				switch completion {
				case .finished:
					self.isLoadingIndicator = false
					if basketResponse.products.count > 0, !self.deliverySettings.address.isEmptyAddress {
						self.isPlaceOrderButtonEnabled = true
					} else {
						self.isPlaceOrderButtonEnabled = false
					}
				case .failure(let error):
					self.isLoadingIndicator = false
					self.isPlaceOrderButtonEnabled = true
					debugPrint("viewWillAppear getBasket(), getOrderDeliveryCost() error:", error)
				}
			}, receiveValue: { [unowned self] (deliveryResponse) in
				// Eсли число самих продуктов(ячеек) в корзине не поменялось, то не перезагружать таблицу.
				// А то порядок ячеек каждый раз меняется. Используется для метода updateProductQuantity
				if basket.products.count != basketResponse.products.count {
					self.basket = basketResponse
					self.totalBasketInfo = basketResponse
				} else {
					self.totalBasketInfo = basketResponse
				}
				self.deliveryCost = deliveryResponse
			})
			.store(in: &subscriptions)
	}

}
