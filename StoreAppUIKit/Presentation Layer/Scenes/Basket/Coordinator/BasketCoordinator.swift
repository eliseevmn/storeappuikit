//
//  BasketCoordinator.swift
//  Sarawan
//
//  Created by MacBook Pro on 26.10.2021.
//

import UIKit

final class BasketCoordinator: BaseCoordinator {

	// MARK: - Private Properties

	private let moduleFactory: ModuleFactoryProtocol
	private let router: Router
	private var basketViewModel: BasketViewModel!

	// MARK: - Public Properties

	var finishFlow: (() -> Void)?

	// MARK: - Initialisers

	init(router: Router, moduleFactory: ModuleFactoryProtocol) {
		self.moduleFactory = moduleFactory
		self.router = router
	}

	// MARK: - Public Methods

	override func start() {
		showBasketScreen()
	}

	// MARK: - Private Methods

	private func showBasketScreen() {
		let basketScreen = moduleFactory.makeBasketModule()
		basketViewModel = basketScreen.basketViewModel
		router.setRoot(basketScreen, animated: false)

		basketViewModel.onSelectDeliveryScreen = { [weak self] selectedScreen in
			self?.runDeliveryFlow(selectedScreen: selectedScreen)
		}
		basketViewModel.onSelectProductCardScreen = { [weak self] productId, quantity in
			self?.showProductCardScreen(with: productId, quantity: quantity)
		}
	}

	private func showProductCardScreen(with productId: Int, quantity: Int) {
//		let productCardScreen = moduleFactory.makeProductCardModule()
//		productCardScreen.productCardViewModel.productId = productId
//		router.present(productCardScreen)
//		productCardScreen.onClose = { [weak self] _ in
//			router.dismiss(animated: true)
//		}
	}

	private func runDeliveryFlow(selectedScreen: DeliveryTableView.DeliveryScreen) {
		switch selectedScreen {
		case .address:
			showAddressScreen()
		case .time:
			showTimeScreen()
		case .payment:
			showPaymentOptionsScreen()
		}
	}

	private func showAddressScreen() {
		debugPrint("in coordinator choose address")
		//			let addressScreen = moduleFactory.AddressModule()
		//			router.push(addressScreen)
		//			addressScreen.onFinish = { [weak self] address in
		//				basketViewModel.deliverySettings.address = address
		//			}
	}

	private func showTimeScreen() {
	}

	private func showPaymentOptionsScreen() {
	}

}
