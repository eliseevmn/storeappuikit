//
//  CategoryViewController.swift
//  Sarawan
//
//  Created by MAC on 21.11.2021.
//

import UIKit
import SnapKit

class CategoryFoodViewCell: UICollectionViewCell {

    struct ViewData {
        let nameFood: String?
        let imageFood: UIImage?
        let nameMarket: String?
        let priceFood: String?
        let weightFood: String?
    }

    // MARK: - Outlets
    private lazy var nameLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 14, weight: .medium),
                                 numberLines: 3)
        return label
    }()
    private lazy var minPriceLabel: UILabel = {
        let label = SarawanLabel(title: Texts.FoodCard.minPrice,
                                 alignment: .right,
                                 fontSize: AppFont.montserratFont(ofSize: 14, weight: .regular))
        return label
    }()
    private lazy var minPriceMarketLabel: UILabel = {
        let label = SarawanLabel(alignment: .right,
                                 fontSize: AppFont.montserratFont(ofSize: 14, weight: .regular))
        return label
    }()
    private lazy var priceFoodLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 18, weight: .semiBold),
                                 colorText: AppColor.orange)
        return label
    }()
    private lazy var weightFoodLabel: UILabel = {
        let label = SarawanLabel(alignment: .right,
                                 fontSize: AppFont.montserratFont(ofSize: 14, weight: .semiBold),
                                 colorText: AppColor.grey)
        return label
    }()
    private lazy var foodImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        return imageView
    }()

    private lazy var buyButton: QuantityButton = {
        let button = QuantityButton()
        return button
    }()

    private let customView: UIView = {
        let customView = UIView()
        customView.layer.cornerRadius = 8
        customView.layer.borderWidth = 0.5
        customView.layer.borderColor = #colorLiteral(red: 0.05098039216, green: 0.05098039216, blue: 0.05098039216, alpha: 1).cgColor
        customView.translatesAutoresizingMaskIntoConstraints = false
        return customView
    }()

    // MARK: - Properties
    lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()
    var onTappedBuyButton: ((String) -> Void)?

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviewsContent()
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }

    override func systemLayoutSizeFitting(_ targetSize: CGSize,
                                          withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority,
                                          verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
}

// MARK: - Configure Cell
extension CategoryFoodViewCell {
    func configureCell(data: CategoryFoodViewCell.ViewData) {
        nameLabel.text = data.nameFood
        minPriceMarketLabel.text = data.nameMarket
        priceFoodLabel.text = data.priceFood
        weightFoodLabel.text = data.weightFood
        foodImage.image = data.imageFood
    }
}

private extension CategoryFoodViewCell {

    func addSubviewsContent() {
        contentView.addSubview(customView)
        customView.addSubview(nameLabel)
        customView.addSubview(foodImage)
        customView.addSubview(nameLabel)
        customView.addSubview(minPriceLabel)
        customView.addSubview(minPriceMarketLabel)
        customView.addSubview(priceFoodLabel)
        customView.addSubview(weightFoodLabel)
        customView.addSubview(buyButton)
    }

    func configureUI() {
        customView.backgroundColor = .white
        customView.layer.cornerRadius = 8

        foodImage.snp.makeConstraints {
            $0.top.equalTo(customView).offset(10)
            $0.leading.equalTo(customView).offset(16)
            $0.trailing.equalTo(customView).offset(-16)
        }

        nameLabel.snp.makeConstraints {
            $0.top.equalTo(foodImage.snp.bottom).offset(10)
            $0.leading.equalTo(customView).offset(20)
            $0.trailing.equalTo(customView).offset(-20)
            $0.height.equalTo(60)
        }

        minPriceLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(18)
            $0.leading.equalTo(customView).offset(16)
        }

        minPriceMarketLabel.snp.makeConstraints {
            $0.bottom.equalTo(minPriceLabel.snp.bottom).offset(0)
            $0.leading.equalTo(minPriceLabel.snp.trailing).offset(4)
            $0.trailing.equalTo(customView).offset(-16)
        }

        priceFoodLabel.snp.makeConstraints {
            $0.top.equalTo(minPriceLabel.snp.bottom).offset(2)
            $0.leading.equalTo(customView).offset(16)
        }

        weightFoodLabel.snp.makeConstraints {
            $0.bottom.equalTo(priceFoodLabel.snp.bottom).offset(0)
            $0.leading.equalTo(priceFoodLabel.snp.trailing).offset(4)
            $0.trailing.equalTo(customView).offset(-16)
        }

        buyButton.snp.makeConstraints {
            $0.height.equalTo(34)
            $0.top.equalTo(priceFoodLabel.snp.bottom).offset(10)
            $0.leading.equalTo(customView).offset(23)
            $0.trailing.equalTo(customView).offset(-23)
            $0.bottom.equalTo(customView).offset(-10)
        }

        customView.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(10)
            $0.leading.trailing.equalTo(contentView).offset(0)
            $0.bottom.trailing.equalTo(contentView).offset(0)
        }
    }
}
