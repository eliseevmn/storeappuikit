//
//  CategoryViewModel.swift
//  Sarawan
//
//  Created by MAC on 21.11.2021.
//

import Foundation
import Combine

final class CategoryFoodViewModel {

    // MARK: - Properties
    private let productService: ProductServiceProtocol
    private var cancellable = Set<AnyCancellable>()
    var isLoadingIndicator = PassthroughSubject<Bool, Never>()
    let typeFoodQuery: TypeFoodQuery?
    var products = PassthroughSubject<[ProductModel], Never>()
    var isFinishScreen: (() -> Void)?

    // MARK: - Init
    init(productService: ProductServiceProtocol,
         typeFoodQuery: TypeFoodQuery?) {
        self.typeFoodQuery = typeFoodQuery
        self.productService = productService
        isLoadingIndicator.send(true)
    }
}

// MARK: - Public Api
extension CategoryFoodViewModel {

    func getAllProductsBySeachNameFoodWithPopularFilter(nameFood: String) {
        productService.getProductsByTypeFilters(isPopularProducts: true,
                                                nameFood: nameFood,
                                                categoryId: nil,
                                                filters: nil,
                                                isDiscountProducts: nil)
            .receive(on: RunLoop.main)
            .map(\.results)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                case .finished:
                    self.isLoadingIndicator.send(false)
                }
            }, receiveValue: { products in
                self.products.send(products.map({ ProductModel(productResponse: $0) }))
            }).store(in: &cancellable)
    }

    func getAllProductsByCatalogIdWithPopularFilter(categoryId: Int) {
        productService.getProductsByTypeFilters(isPopularProducts: true,
                                                nameFood: nil,
                                                categoryId: categoryId,
                                                filters: nil,
                                                isDiscountProducts: nil)
            .receive(on: RunLoop.main)
            .map(\.results)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                case .finished:
                    self.isLoadingIndicator.send(false)
                }
            }, receiveValue: { products in
                self.products.send(products.map({ ProductModel(productResponse: $0) }))
            }).store(in: &cancellable)
    }

    func getProductsTypeFiltersByNameFood(nameFood: String, typeFilter: FiltersParameters) {
        productService.getProductsByTypeFilters(isPopularProducts: nil,
                                                nameFood: nameFood,
                                                categoryId: nil,
                                                filters: typeFilter,
                                                isDiscountProducts: nil)
            .receive(on: RunLoop.main)
            .map(\.results)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                case .finished:
                    self.isLoadingIndicator.send(false)
                }
            }, receiveValue: { products in
                self.products.send(products.map({ ProductModel(productResponse: $0) }))
            }).store(in: &cancellable)
    }

    func getProductsTypeFiltersByCatalogId(categoryId: Int, typeFilter: FiltersParameters) {
        productService.getProductsByTypeFilters(isPopularProducts: nil,
                                                nameFood: nil,
                                                categoryId: categoryId,
                                                filters: typeFilter,
                                                isDiscountProducts: nil)
            .receive(on: RunLoop.main)
            .map(\.results)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                case .finished:
                    self.isLoadingIndicator.send(false)
                }
            }, receiveValue: { products in
                self.products.send(products.map({ ProductModel(productResponse: $0) }))
            }).store(in: &cancellable)
    }
}
