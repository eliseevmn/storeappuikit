//
//  CategoryViewController.swift
//  Sarawan
//
//  Created by MAC on 21.11.2021.
//

import UIKit
import Combine

protocol CategoryFoodViewProtocol: AnyObject {
    func didTapBackButton()
    func didTapFilterButton(filterType: FilterType)
}

final class CategoryFoodView: UIView {

    // MARK: - Outlets
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    lazy var navigationView = NavigationView(typeFoodQyery: viewModel.typeFoodQuery)
    let filterButton = DropDownButton()
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let width = (UIScreen.main.bounds.size.width - 40 - 15)/2
        layout.estimatedItemSize = CGSize(width: width, height: 10)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 20, right: 20)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.registerClass(CategoryFoodViewCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        return collectionView
    }()
    private var nameFood: String = ""

    // MARK: - Properties
    weak var delegate: CategoryFoodViewProtocol?
    private let viewModel: CategoryFoodViewModel
    private var products: [ProductModel] = []
    var cancelablle = Set<AnyCancellable>()
    var typeFoodQuery: TypeFoodQuery?

    // MARK: - Init
    init(typeFoodQuery: TypeFoodQuery?,
         viewModel: CategoryFoodViewModel) {
        self.typeFoodQuery = typeFoodQuery
        self.viewModel = viewModel
        super.init(frame: .zero)
        addSubviews([collectionView, activityIndicator, navigationView, filterButton])
        navigationView.delegate = self
        filterButton.dropView.delegate = self

        viewModel.products.sink(receiveValue: { products in
            self.products = products
            self.collectionView.reloadData()
        }).store(in: &cancelablle)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Public API
extension CategoryFoodView {

    func startingActivityIndicator() {
        activityIndicator.startAnimating()
    }

    func stopActivityIndicator() {
        activityIndicator.stopAnimating()
    }

    func isHiddenCollectionView(isLoadingIndicator: Bool) {
        if isLoadingIndicator {
            collectionView.alpha = 0
        } else {
            collectionView.alpha = 1
        }
    }
}

// MARK: - UICollectionViewDelegate
extension CategoryFoodView: UICollectionViewDelegate {

}

// MARK: - UICollectionViewDataSource
extension CategoryFoodView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = products[indexPath.item]
        guard let cell: CategoryFoodViewCell = collectionView.cell(forRowAt: indexPath) else {
            return UICollectionViewCell()
        }
        cell.configureCell(data: .init(nameFood: product.name,
                                       imageFood: UIImage(named: "icDefaultFood"),
                                       nameMarket: product.storeName,
                                       priceFood: product.priceFood + " \u{20BD}",
                                       weightFood: product.priceType))
        return cell
    }
}

// MARK: - NavigationViewProtocol
extension CategoryFoodView: NavigationViewProtocol {
    func didTapBackButton() {
        delegate?.didTapBackButton()
    }
}

// MARK: - DropDownProtocol
extension CategoryFoodView: DropDownProtocol {
    func dropDownPressed(filterType: FilterType) {
        filterButton.setTitle(filterType.description, for: .normal)
        if filterType.image == UIImage() {
            filterButton.setImage(UIImage(named: "icArrowDownFilter"), for: .normal)
        } else {
            filterButton.setImage(filterType.image, for: .normal)
        }
        filterButton.dismissDropDown()
        delegate?.didTapFilterButton(filterType: filterType)
    }
}

// MARK: - ConfigureUI
extension CategoryFoodView {
    func configureUI() {
        navigationView.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(0)
            $0.leading.equalTo(self).offset(20)
            $0.width.equalTo(self.frame.width/3 * 2)
            $0.height.equalTo(44)
        }

        filterButton.snp.makeConstraints {
            $0.top.equalTo(navigationView.snp.top).offset(5)
            $0.bottom.equalTo(navigationView.snp.bottom).offset(-5)
            $0.width.equalTo(self.frame.width/3 * 1)
            $0.leading.equalTo(navigationView.snp.trailing).offset(0)
            $0.trailing.equalTo(self).offset(-20)
        }

        collectionView.snp.makeConstraints {
            $0.top.equalTo(navigationView.snp.bottom).offset(0)
            $0.leading.trailing.bottom.equalTo(self).offset(0)
        }

        activityIndicator.snp.makeConstraints {
            $0.centerX.equalTo(self).offset(0)
            $0.centerY.equalTo(self).offset(0)
        }
    }
}
