//
//  CategoryViewController.swift
//  Sarawan
//
//  Created by MAC on 21.11.2021.
//

import UIKit
import Combine

enum TypeFoodQuery {
    case productsPopular(catalogFoodModel: MainCategoryModel)
    case productsBySearchName(foodName: String)
}

final class CategoryFoodViewController: UIViewController, NavigationViewProtocol {

    // MARK: - Outlets
    private lazy var customView = self.view as? CategoryFoodView

    // MARK: - Properties
    let viewModel: CategoryFoodViewModel
    var canceballe = Set<AnyCancellable>()
    var typeFoodQuery: TypeFoodQuery?

    // MARK: - Init
    init(viewModel: CategoryFoodViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = CategoryFoodView(typeFoodQuery: typeFoodQuery,
                                    viewModel: viewModel)
        view.delegate = self
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        refreshingData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
}

// MARK: - Private API
private extension CategoryFoodViewController {

    func refreshingData() {
        startActivityIndicator()

        switch viewModel.typeFoodQuery {
        case .productsPopular(catalogFoodModel: let catalogFoodModel):
            typeFoodQuery = .productsPopular(catalogFoodModel: catalogFoodModel)
            viewModel.getAllProductsByCatalogIdWithPopularFilter(categoryId: catalogFoodModel.idMainCategory ?? 0)
        case .productsBySearchName(foodName: let foodName):
            typeFoodQuery = .productsBySearchName(foodName: foodName)
            viewModel.getAllProductsBySeachNameFoodWithPopularFilter(nameFood: foodName)
        case .none:
            print("Ничего не пришло")
        }

        endActivityIndicator()
    }

    func startActivityIndicator() {
        customView?.startingActivityIndicator()
        customView?.isHiddenCollectionView(isLoadingIndicator: true)
    }

    func endActivityIndicator() {
        viewModel.isLoadingIndicator.sink(receiveValue: { isLoadingIndicator in
            UIView.animate(withDuration: 0.2) {
                self.customView?.stopActivityIndicator()
                self.customView?.isHiddenCollectionView(isLoadingIndicator: isLoadingIndicator)
            }
        }).store(in: &canceballe)
    }

    func changeTypeFoodQuery(filterType: FilterType) {
        switch viewModel.typeFoodQuery {
        case .productsPopular(catalogFoodModel: let catalogFoodModel):
            changeTypeFilterByCategoryId(filterType: filterType, categoryId: catalogFoodModel.idMainCategory ?? 0)
        case .productsBySearchName(foodName: let foodName):
            changeTypeFilterByNameFood(filterType: filterType, nameFood: foodName)
        case .none:
            print("Ничего не пришло")
        }
    }

    func changeTypeFilterByNameFood(filterType: FilterType, nameFood: String) {
        switch filterType {
        case .upPriceFilter:
            viewModel.getProductsTypeFiltersByNameFood(nameFood: nameFood, typeFilter: .upPrice)
        case .downPriceFilter:
            viewModel.getProductsTypeFiltersByNameFood(nameFood: nameFood, typeFilter: .downPrice)
        case .alphabetFilter:
            viewModel.getProductsTypeFiltersByNameFood(nameFood: nameFood, typeFilter: .name)
        case .upDiscountFilter:
            viewModel.getProductsTypeFiltersByNameFood(nameFood: nameFood, typeFilter: .upDiscount)
        case .downDiscountFilter:
            viewModel.getProductsTypeFiltersByNameFood(nameFood: nameFood, typeFilter: .downDiscount)
        case .popularFilter:
            viewModel.getAllProductsBySeachNameFoodWithPopularFilter(nameFood: nameFood)
        }
    }

    private func changeTypeFilterByCategoryId(filterType: FilterType, categoryId: Int) {
        switch filterType {
        case .upPriceFilter:
            viewModel.getProductsTypeFiltersByCatalogId(categoryId: categoryId, typeFilter: .upPrice)
        case .downPriceFilter:
            viewModel.getProductsTypeFiltersByCatalogId(categoryId: categoryId, typeFilter: .downPrice)
        case .alphabetFilter:
            viewModel.getProductsTypeFiltersByCatalogId(categoryId: categoryId, typeFilter: .name)
        case .upDiscountFilter:
            viewModel.getProductsTypeFiltersByCatalogId(categoryId: categoryId, typeFilter: .upDiscount)
        case .downDiscountFilter:
            viewModel.getProductsTypeFiltersByCatalogId(categoryId: categoryId, typeFilter: .downDiscount)
        case .popularFilter:
            viewModel.getAllProductsByCatalogIdWithPopularFilter(categoryId: categoryId)
        }
    }
}

// MARK: - CategoryFoodViewProtocol
extension CategoryFoodViewController: CategoryFoodViewProtocol {

    func didTapFilterButton(filterType: FilterType) {
        startActivityIndicator()
        changeTypeFoodQuery(filterType: filterType)
        endActivityIndicator()
    }

    func didTapBackButton() {
        viewModel.isFinishScreen?()
    }
}
