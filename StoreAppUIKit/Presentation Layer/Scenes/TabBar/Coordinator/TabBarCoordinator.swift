//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class TabBarCoordinator: BaseCoordinator {

	// MARK: - Private Properties

	private let coordinatorFactory: CoordinatorFactory
	private let router: Router
	private var isLogin = false
	private var tabBarController = TabBarController()

	// MARK: - Initialisers

	init(router: Router, coordinatorFactory: CoordinatorFactory) {
		self.coordinatorFactory = coordinatorFactory
		self.router = router
	}

	// MARK: - Public Methods

	override func start() {
		initializeTabBar()
	}

	// MARK: - Private Methods

	private func initializeTabBar() {
		// стандартная реализация таба

		let mainNavigationController = MainNavigationController()
		mainNavigationController.tabBarItem = UITabBarItem(title: "Главная", image: UIImage(named: "icTabMain"), tag: 0)
		let mainCoordinator = coordinatorFactory.makeMainFoodCoordinator(with: Router(rootController: mainNavigationController))

		let catalogNavigationController = MainNavigationController()
		catalogNavigationController.tabBarItem = UITabBarItem(title: "Каталог", image: UIImage(named: "icTabCatalog"), tag: 1)
		let catalogCoordinator = coordinatorFactory.makeCatalogFoodCoordinator(with: Router(rootController: catalogNavigationController))

		let basketNavigationController = MainNavigationController()
		basketNavigationController.tabBarItem = UITabBarItem(title: "Корзина", image: UIImage(named: "icTabBasket"), tag: 2)
		let basketCoordinator = coordinatorFactory.makeBasketCoordinator(with: Router(rootController: basketNavigationController))

        let infoNavigationController = MainNavigationController()
        infoNavigationController.tabBarItem = UITabBarItem(title: "Инфо", image: UIImage(named: "icTabInfo"), tag: 3)
		let infoCoordinator = coordinatorFactory.makeCommonInfoCoordinator(with: Router(rootController: infoNavigationController))

		tabBarController.viewControllers = [mainNavigationController,
											catalogNavigationController,
											basketNavigationController,
											infoNavigationController]

		tabbarWithLoginFlow()

		router.setRoot(tabBarController, animated: false, hideBar: true)

		retain(mainCoordinator)
        retain(catalogCoordinator)
		retain(basketCoordinator)
        retain(infoCoordinator)

        mainCoordinator.start()
        catalogCoordinator.start()
		basketCoordinator.start()
        infoCoordinator.start()

		if isLogin {
			tabbarWithProfileFlow()
		}
	}

	private func tabbarWithLoginFlow() {
		let loginNavigationController = MainNavigationController()
		loginNavigationController.tabBarItem = UITabBarItem(title: "Профиль", image: UIImage(named: "icTabProfile"), tag: 4)
		let loginCoordinator = coordinatorFactory.makeLoginCoordinator(with: Router(rootController: loginNavigationController))

		tabBarController.viewControllers?.append(loginNavigationController)

		retain(loginCoordinator)
		loginCoordinator.start()

		isLogin = loginCoordinator.isLogin ?? false

		loginCoordinator.finishFlow = { [weak self, weak loginCoordinator] isLogin in
			self?.isLogin = isLogin
			self?.release(loginCoordinator)
			self?.tabBarController.viewControllers?.removeLast()
			self?.start()
		}

		if isLogin {
			release(loginCoordinator)
			tabBarController.viewControllers?.removeLast()
			return
		}
	}

	private func tabbarWithProfileFlow() {
		let profileNavigationController = MainNavigationController()
		profileNavigationController.tabBarItem = UITabBarItem(title: "Профиль", image: UIImage(named: "icTabProfile"), tag: 4)
		let profileCoordinator = coordinatorFactory.makeProfileCoordinator(with: Router(rootController: profileNavigationController))
		tabBarController.viewControllers?.append(profileNavigationController)

		profileCoordinator.finishFlow = { [weak self, weak profileCoordinator] isLogin in
			self?.isLogin = isLogin
			self?.release(profileCoordinator)
			self?.tabBarController.viewControllers?.removeLast()
			self?.start()
		}

		retain(profileCoordinator)
		profileCoordinator.start()
	}

}
#warning("#3 расскомментировать для авторизации поверх приложения")
/* Для логина на все приложение удалить ВЕСЬ класс сверху, и раскомментировать ниже

final class TabBarCoordinator: BaseCoordinator {

	// MARK: - Private Properties

	private let coordinatorFactory: CoordinatorFactory
	private let router: Router
	private var tabBarController = TabBarController()

	// MARK: - Public Properties

	var onLogout: ((Bool) -> Void)?

	// MARK: - Initialisers

	init(router: Router, coordinatorFactory: CoordinatorFactory) {
		self.coordinatorFactory = coordinatorFactory
		self.router = router
	}

	// MARK: - Public Methods

	override func start() {
		initializeTabBar()
	}

	// MARK: - Private Methods

	private func initializeTabBar() {
		// стандартная реализация таба

		let mainNavigationController = MainNavigationController()
		mainNavigationController.tabBarItem = UITabBarItem(title: "Главная", image: UIImage(named: "icTabMain"), tag: 0)
		let mainCoordinator = coordinatorFactory.makeMainFoodCoordinator(with: Router(rootController: mainNavigationController))

		let catalogNavigationController = MainNavigationController()
		catalogNavigationController.tabBarItem = UITabBarItem(title: "Каталог", image: UIImage(named: "icTabCatalog"), tag: 1)
		let catalogCoordinator = coordinatorFactory.makeCatalogFoodCoordinator(with: Router(rootController: catalogNavigationController))

		let basketNavigationController = MainNavigationController()
		basketNavigationController.tabBarItem = UITabBarItem(title: "Корзина", image: UIImage(named: "icTabBasket"), tag: 2)
		let basketCoordinator = coordinatorFactory.makeBasketCoordinator(with: Router(rootController: basketNavigationController))

		let infoNavigationController = MainNavigationController()
		infoNavigationController.tabBarItem = UITabBarItem(title: "Инфо", image: UIImage(named: "icTabInfo"), tag: 3)
		let infoCoordinator = coordinatorFactory.makeCommonInfoCoordinator(with: Router(rootController: infoNavigationController))

		let profileNavigationController = MainNavigationController()
		profileNavigationController.tabBarItem = UITabBarItem(title: "Профиль", image: UIImage(named: "icTabProfile"), tag: 4)
		let profileCoordinator = coordinatorFactory.makeProfileCoordinator(with: Router(rootController: profileNavigationController))

		profileCoordinator.finishFlow = { [weak self, weak profileCoordinator] isLogin in
			self?.release(profileCoordinator)
			self?.onLogout?(isLogin)
		}

		tabBarController.viewControllers = [mainNavigationController,
											catalogNavigationController,
											basketNavigationController,
											infoNavigationController,
											profileNavigationController]

		router.setRoot(tabBarController, animated: false, hideBar: true)

		retain(mainCoordinator)
		retain(catalogCoordinator)
		retain(basketCoordinator)
		retain(infoCoordinator)
		retain(profileCoordinator)

		mainCoordinator.start()
		catalogCoordinator.start()
		basketCoordinator.start()
		infoCoordinator.start()
		profileCoordinator.start()
	}

}
*/
