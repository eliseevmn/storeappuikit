//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

		UITabBar.appearance().barTintColor = AppColor.greenBackground
		UITabBar.appearance().unselectedItemTintColor = AppColor.black
		UITabBar.appearance().tintColor = AppColor.orange
        UITabBar.appearance().backgroundColor = AppColor.greenBackground
    }

}
