//
//  CatalogFoodHeaderSection.swift
//  Sarawan
//
//  Created by MAC on 05.12.2021.
//

import UIKit

final class CatalogFoodHeaderSection: UITableViewHeaderFooterView {

    // MARK: - Outlets
    private let titleLabel = SarawanLabel(title: "Каталог", alignment: .left,
                               fontSize: AppFont.montserratFont(ofSize: 24, weight: .semiBold),
                               colorText: AppColor.black)

    // MARK: - Init
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.addSubviews(titleLabel)
        backgroundColor = AppColor.white
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - ConfigureUI
private extension CatalogFoodHeaderSection {

    func configureUI() {
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(20)
            $0.trailing.equalTo(contentView).offset(-20)
            $0.top.equalTo(contentView).offset(5)
            $0.bottom.equalTo(contentView).offset(-5)
        }
    }
}
