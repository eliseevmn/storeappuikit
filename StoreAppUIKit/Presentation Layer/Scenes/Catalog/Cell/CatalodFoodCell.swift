//
//  CategoryFoodCell.swift
//  Sarawan
//
//  Created by MAC on 04.11.2021.
//

import UIKit

final class CatalodFoodCell: UITableViewCell {

    // MARK: - Properties
    struct ViewData {
        let nameFood: String?
    }

    // MARK: - Outlets
    private lazy var nameFoodLabel: UILabel = {
        let label = SarawanLabel(alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 16, weight: .regular))
        return label
    }()
    private lazy var separatorView: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = AppColor.lightGray
        return view
    }()

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubviews([nameFoodLabel, separatorView])
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public Api
extension CatalodFoodCell {
    func configureCell(categoryFoodModel: CatalodFoodCell.ViewData) {
        self.nameFoodLabel.text = categoryFoodModel.nameFood
    }
}

// MARK: - Private API
private extension CatalodFoodCell {
    func configureUI() {
        nameFoodLabel.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(20)
            $0.centerY.equalTo(contentView)
        }

        separatorView.snp.makeConstraints {
            $0.leading.equalTo(contentView).offset(20)
            $0.trailing.equalTo(contentView).offset(-20)
            $0.bottom.equalTo(contentView).offset(-1)
            $0.height.equalTo(1)
        }
    }
}
