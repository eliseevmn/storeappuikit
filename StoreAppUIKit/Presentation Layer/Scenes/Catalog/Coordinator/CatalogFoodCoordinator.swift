//
//  CategoryFoodCoordinator.swift
//  Sarawan
//
//  Created by MAC on 04.11.2021.
//

import UIKit

final class CatalogFoodCoordinator: BaseCoordinator {

    // MARK: - Properies
    private let moduleFactory: ModuleFactoryProtocol
    private let router: Router

    // MARK: - Init
    init(router: Router, moduleFactory: ModuleFactoryProtocol) {
        self.moduleFactory = moduleFactory
        self.router = router
    }

    // MARK: - Navigation functions
    override func start() {
        showCategoryFoodScreen()
    }

    // MARK: - Private Api
    private func showCategoryFoodScreen() {
        let catalogFoodScreen = moduleFactory.makeCatalogFoodModule()
        catalogFoodScreen.viewModel.onCategoryScreen = { [weak self] typeFoodQuery in
            guard let self = self else { return }
            self.showCategoryFoodScreen(typeFoodQuery: typeFoodQuery)
        }
        router.push(catalogFoodScreen)
    }

    private func showCategoryFoodScreen(typeFoodQuery: TypeFoodQuery) {
        let categoryScreen = moduleFactory.makeCategoryFoodModule(typeFoodQuery: typeFoodQuery)
        router.push(categoryScreen)
        categoryScreen.viewModel.isFinishScreen = {
            self.router.pop(animated: true)
        }
    }
}
