//
//  CategoryFoodView.swift
//  Sarawan
//
//  Created by MAC on 04.11.2021.
//

import UIKit
import Combine

protocol CatalogFoodViewProtocol: AnyObject {
    func tableView(didSelect typeFoodQuery: TypeFoodQuery)
    func didTapSearchButtonView()
}

final class CatalogFoodView: UIView {

    // MARK: - Outlets
    lazy var tableView: DynamicHeightTableView = {
        let tableView = DynamicHeightTableView(frame: .zero, style: .grouped)
        tableView.registerClass(CatalodFoodCell.self)
        tableView.registerHeaderFooter(CatalogFoodHeaderSection.self)
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = nil
        return tableView
    }()
    private let logoView = LogoView()
    let searchView = SearchView()
    private let activityIndicator = UIActivityIndicatorView(style: .large)

    // MARK: - Properties
    weak var delegate: CatalogFoodViewProtocol?
    private let viewModel: CatalogFoodViewModel
    private var categories: [MainCategoryModel] = []
    var cancelablle: AnyCancellable?

    // MARK: - Init
    init(viewModel: CatalogFoodViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        addSubviews([logoView, searchView, tableView, activityIndicator])

        cancelablle = viewModel.catalogs.sink(receiveValue: { categories in
            self.categories = categories
            self.tableView.reloadData()
        })

        searchView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Public API
extension CatalogFoodView {

    func removeTextTextField() {
        searchView.removeTextTextField()
    }

    func removeMainFoodTextFieldFirstResponder() {
        searchView.removeTextFieldFirstResponder()
    }

    func startingActivityIndicator() {
        activityIndicator.startAnimating()
    }

    func stopActivityIndicator() {
        activityIndicator.stopAnimating()
    }

    func isHiddenCollectionView(isLoadingIndicator: Bool) {
        if isLoadingIndicator {
            tableView.alpha = 0
        } else {
            tableView.alpha = 1
        }
    }
}

// MARK: - UITableViewDelegate
extension CatalogFoodView: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            tableView.deselectRow(at: indexPath, animated: false)
            let categoryFoodModel = categories[indexPath.row]
            delegate?.tableView(didSelect: .productsPopular(catalogFoodModel: categoryFoodModel))
        }
    }
}

// MARK: - UITableViewDataSource
extension CatalogFoodView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        return categories.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return UITableViewCell()
        }

        let categoryFoodModel = categories[indexPath.row]
        guard let categoryFoodCell: CatalodFoodCell = tableView.cell(forRowAt: indexPath) else { return UITableViewCell() }
        categoryFoodCell.configureCell(categoryFoodModel: .init(nameFood: categoryFoodModel.name))
        return categoryFoodCell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            guard let header: CatalogFoodHeaderSection = tableView.dequeHeaderFooter() else {
                return UITableViewHeaderFooterView()
            }
            return header
        }
        return nil
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return UITableView.automaticDimension
        }
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
}

// MARK: - SearchViewProtocol
extension CatalogFoodView: SearchViewProtocol {
    func didTapSearchButtonView() {
        delegate?.didTapSearchButtonView()
    }
}

// MARK: - ConfigureUI
private extension CatalogFoodView {

    func configureUI() {
        logoView.snp.makeConstraints {
            $0.top.equalTo(safeAreaLayoutGuide).offset(20)
            $0.leading.trailing.equalTo(self)
        }

        searchView.snp.makeConstraints {
            $0.top.equalTo(logoView.snp.bottom).offset(10)
            $0.leading.trailing.equalTo(self)
            $0.height.equalTo(36)
        }

        tableView.snp.makeConstraints {
            $0.top.equalTo(searchView.snp.bottom).offset(10)
            $0.leading.trailing.bottom.equalTo(self).offset(0)
        }

        activityIndicator.snp.makeConstraints {
            $0.centerX.equalTo(self).offset(0)
            $0.centerY.equalTo(self).offset(0)
        }
    }
}
