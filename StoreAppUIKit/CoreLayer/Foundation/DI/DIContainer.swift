//
//  DIContainer.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import SwiftKeychainWrapper

final class DIContainer {

    // MARK: - Public Properties

    let session: URLSession
    let decoder: JSONDecoder

    let keychainWrapper: KeychainWrapper
    let requestBuilder: RequestBuilder
    let apiClient: APIClient
    let tokenRepository: TokenRepository

    lazy var userService = UserService(userAPIClient: apiClient, tokenRepository: tokenRepository)
    lazy var authService = AuthService(authAPIClient: apiClient, tokenRepository: tokenRepository)
    lazy var basketService = BasketService(basketAPIClient: apiClient, orderAPIClient: apiClient, tokenRepository: tokenRepository)
    lazy var productService = ProductService(productAPIClient: apiClient)
    lazy var profileService = ProfileService(userAPIClient: apiClient, tokenRepository: tokenRepository)

    // MARK: - Initialisers

    init() {
        keychainWrapper = KeychainWrapper.standard
        tokenRepository = TokenRepository(keychainWrapper: keychainWrapper)
        requestBuilder = RequestBuilder()
        session = URLSession.shared
        decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        apiClient = APIClient(requestBuilder: requestBuilder, session: session, decoder: decoder)
    }
}
