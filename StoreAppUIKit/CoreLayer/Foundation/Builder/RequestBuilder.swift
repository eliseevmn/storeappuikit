//
//  RequestBuilder.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
// swiftlint:disable identifier_name
enum Endpoint {
    case none
    case sms
    case token
    case basket(id: Int? = nil)
    case basketDelete(id: Int)
    case addresses
    case address(id: Int? = nil)
    case user(id: Int? = nil)
    case product(id: Int? = nil)
    case categories
    case getProducts
    case order(id: Int? = nil)
    case orderStart
    case orderCalculate
    case orderApprove

    var rawValue: String {
        switch self {
        case .none:
            return "/"
        case .sms:
            return "/users/authentication/api/sms-send/"
        case .token:
            return "/users/authentication/api/get-token/"
        case .basket(id: let id):
            guard let id = id else { return "​/api​/basket​/" }
            return "​/api​/basket​/\(id)/"
        case .user(id: let id):
            guard let id = id else { return "/api/user/" }
            return "/api/user/\(id)/"
        case .addresses:
            return "/api/user_address/"
        case .address(id: let id):
            guard let id = id else { return "/api/user_address/" }
            return "/api/user_address/\(id)/"
        case .basketDelete(id: let id):
            return "/api/basket_product/\(id)/"
        case .user(id: let id):
            guard let id = id else { return "/api/user/" }
            return "/api/user/\(id)/"
        case .product(id: let id):
            guard let id = id else { return "/api/products/" }
            return "/api/products/\(id)/"
        case .categories:
            return "/api/categories/"
        case .getProducts:
            return "/api/products/"
        case .order(id: let id):
            guard let id = id else { return "/api/order/" }
            return "/api/order/\(id)/"
        case .orderStart:
            return "/api/order_start/"
        case .orderCalculate:
            return "/api/order_calculate/"
        case .orderApprove:
            return "/api/order_approve/"
        }
    }
}

enum HTTPMethod: String {
    case POST
    case PUT
    case GET
    case DELETE
    case PATCH
}

protocol RequestBuildable {
    var host: String { get }
    var path: Endpoint { get }

    var method: HTTPMethod { get }
    var parameters: [String: Any] { get }
    var headers: [String: String] { get }

    func set(host: String) -> Self
    func set(path: Endpoint) -> Self
    func set(method: HTTPMethod) -> Self
    func set<T: Encodable>(parameters: T) -> Self
    func add(header name: String, value: String) -> Self
    func set(headers: [String: String]) -> Self

    func build() -> URLRequest?
}

class RequestBuilder: RequestBuildable {

    private(set) var host: String = "https://dev.sarawan.ru"
    private(set) var path: Endpoint = .none

    private(set) var method: HTTPMethod = .GET
    private(set) var parameters: [String: Any] = [:]
    private(set) var headers: [String: String] = ["Accept": "application/json",
                                                  "Content-Type": "application/json"]
    @discardableResult
    func set(host: String) -> Self {
        self.host = host
        return self
    }

    @discardableResult
    func set(path: Endpoint) -> Self {
        self.path = path
        return self
    }

    @discardableResult
    func set(method: HTTPMethod) -> Self {
        self.method    = method
        return self
    }

    @discardableResult
    func set<T: Encodable>(parameters: T) -> Self {
        if let dictionary = parameters as? [String: Any] {
            self.parameters = dictionary
        } else {
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = .convertToSnakeCase
            guard let jsonData = try? encoder.encode(parameters) else { return self }
            guard let jsonObject = try? JSONSerialization.jsonObject(with: jsonData) else { return self }
            if let dictionary = jsonObject as? [String: Any] {
                self.parameters = dictionary
            }
        }
        return self
    }

    @discardableResult
    func add(header name: String, value: String) -> Self {
        self.headers[name] = value
        return self
    }

    @discardableResult
    func set(headers: [String: String]) -> Self {
        self.headers.merge(headers) { (_, last) -> String in
            last
        }
        return self
    }

    func build() -> URLRequest? {
        guard var urlComponents = URLComponents(string: host) else { return nil }
        urlComponents.path = path.rawValue

        let isGetMethod = method == .GET
        if  !parameters.isEmpty, isGetMethod {
            urlComponents.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value as? String )}
        }

        guard let url = urlComponents.url else { return nil }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue

        if !parameters.isEmpty, !isGetMethod {
            let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
            urlRequest.httpBody = jsonData
        }

        headers.forEach { key, value in
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }

        parameters = [:]
        headers = ["Accept": "application/json",
                   "Content-Type": "application/json"]

        return urlRequest
    }
}
