//
//  ModuleFactory.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

protocol ModuleFactoryProtocol {

    func makeTabBarModule() -> TabBarController
    func makeBasketModule() -> BasketViewController
    func makeAboutProjectInfoModule() -> AboutProjectInfoController
    func makeHowWeWorkInfoModule() -> HowWeWorkInfoController
    func makeSupportProjectInfoModule() -> SupportProjectInfoController
    func makeCommonInfoModule() -> CommonInfoController
    func makeCatalogFoodModule() -> CatalogFoodController
    func makeCategoryFoodModule(typeFoodQuery: TypeFoodQuery) -> CategoryFoodViewController
    func makeMainFoodModule() -> MainFoodController
    func makeLoginModule() -> LoginViewController
    func makeAuthModule(with phoneNumber: String) -> AuthenticationViewController
    func makeProfileModule() -> ProfileViewController
    func makeNameModule() -> NameViewController
    func makeAddressModule() -> AddressViewController
}

final class ModuleFactory: ModuleFactoryProtocol {

    private lazy var container = DIContainer()

    func makeTabBarModule() -> TabBarController {
        return TabBarController()
    }

    func makeMainFoodModule() -> MainFoodController {
        let viewModel = MainFoodViewModel(productService: container.productService)
        return MainFoodController(viewModel: viewModel)
    }

    func makeBasketModule() -> BasketViewController {
        let viewModel = BasketViewModel(service: container.basketService)
        return BasketViewController(viewModel: viewModel)
    }

    func makeAboutProjectInfoModule() -> AboutProjectInfoController {
        let viewModel = AboutProjectInfoViewModel()
        return AboutProjectInfoController(viewModel: viewModel)
    }

    func makeHowWeWorkInfoModule() -> HowWeWorkInfoController {
        let viewModel = HowWeWorkInfoViewModel()
        return HowWeWorkInfoController(viewModel: viewModel)
    }

    func makeSupportProjectInfoModule() -> SupportProjectInfoController {
        let viewModel = SupportProjectInfoViewModel()
        return SupportProjectInfoController(viewModel: viewModel)
    }

    func makeCommonInfoModule() -> CommonInfoController {
        let viewModel = CommonInfoViewModel()
        let controller = CommonInfoController(viewModel: viewModel)
        return controller
    }

    func makeCatalogFoodModule() -> CatalogFoodController {
        let viewModel = CatalogFoodViewModel(productService: container.productService)
        let controller = CatalogFoodController(viewModel: viewModel)
        return controller
    }

    func makeCategoryFoodModule(typeFoodQuery: TypeFoodQuery) -> CategoryFoodViewController {
        let viewModel = CategoryFoodViewModel(productService: container.productService,
                                              typeFoodQuery: typeFoodQuery)
        let controller = CategoryFoodViewController(viewModel: viewModel)
        return controller
    }

    func makeLoginModule() -> LoginViewController {
        let viewModel = LoginViewModel(service: container.authService)
        return LoginViewController(viewModel: viewModel)
    }

    func makeAuthModule(with phoneNumber: String) -> AuthenticationViewController {
        return AuthenticationViewController(viewModel: AuthenticationViewModel(service: container.authService, phoneNumber: phoneNumber)/*provider: LoginProvider*/)
    }

    func makeProfileModule() -> ProfileViewController {
        return ProfileViewController(viewModel: ProfileViewModel(service: container.profileService)/*provider: ProfileProvider*/)
    }

    func makeNameModule() -> NameViewController {
        return NameViewController(viewModel: NameViewModel(service: container.userService))
    }
    func makeAddressModule() -> AddressViewController {
        return AddressViewController(viewModel: AddressViewModel(service: container.userService))
    }
}
