//
//  CoordinatorFactory.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

protocol CoordinatorFactoryProtocol {

    func makeApplicationCoordinator(with router: Router) -> AppCoordinator
    func makeTabBarCoordinator(with router: Router) -> TabBarCoordinator
    func makeMainFoodCoordinator(with router: Router) -> MainFoodCoordinator
    func makeCatalogFoodCoordinator(with router: Router) -> CatalogFoodCoordinator
    func makeBasketCoordinator(with router: Router) -> BasketCoordinator
    func makeCommonInfoCoordinator(with router: Router) -> CommonInfoCoordinator
    func makeProfileCoordinator(with router: Router) -> ProfileCoordinator
    func makeLoginCoordinator(with router: Router) -> LoginCoordinator
}

final class CoordinatorFactory: CoordinatorFactoryProtocol {

    private let moduleFactory: ModuleFactoryProtocol

    init(moduleFactory: ModuleFactoryProtocol) {
        self.moduleFactory = moduleFactory
    }

    func makeApplicationCoordinator(with router: Router) -> AppCoordinator {
        return AppCoordinator(router: router, coordinatorFactory: self)
    }

    func makeTabBarCoordinator(with router: Router) -> TabBarCoordinator {
        return TabBarCoordinator(router: router, coordinatorFactory: self)
    }

    func makeMainFoodCoordinator(with router: Router) -> MainFoodCoordinator {
        return MainFoodCoordinator(router: router, moduleFactory: moduleFactory)
    }

    func makeCatalogFoodCoordinator(with router: Router) -> CatalogFoodCoordinator {
        return CatalogFoodCoordinator(router: router, moduleFactory: moduleFactory)
    }

    func makeBasketCoordinator(with router: Router) -> BasketCoordinator {
        return BasketCoordinator(router: router, moduleFactory: moduleFactory)
    }

    func makeCommonInfoCoordinator(with router: Router) -> CommonInfoCoordinator {
        return CommonInfoCoordinator(router: router, moduleFactory: moduleFactory)
    }

    func makeProfileCoordinator(with router: Router) -> ProfileCoordinator {
        return ProfileCoordinator(router: router, moduleFactory: moduleFactory)
    }

    func makeLoginCoordinator(with router: Router) -> LoginCoordinator {
        return LoginCoordinator(router: router, moduleFactory: moduleFactory)
    }
}
