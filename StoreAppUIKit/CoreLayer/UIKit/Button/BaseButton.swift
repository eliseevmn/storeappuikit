//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class BaseButton: UIButton {

	override init(frame: CGRect) {
		super.init(frame: frame)

		translatesAutoresizingMaskIntoConstraints = false
		backgroundColor = AppColor.green
		setTitleColor(AppColor.white, for: .normal)
		titleLabel?.font = AppFont.montserratFont(ofSize: 16, weight: .semiBold)
		layer.cornerRadius = 10.0
		layer.masksToBounds = true
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override var isEnabled: Bool {
		willSet {
			backgroundColor = AppColor.green.withAlphaComponent(newValue ? 1 : 0.3)
		}
	}

}
