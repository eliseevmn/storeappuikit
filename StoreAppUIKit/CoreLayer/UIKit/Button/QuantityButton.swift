//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class QuantityButton: UIControl {

	private enum ActionState {
		case initial(Int), tap, increase, decrease
	}

	// MARK: - Private Properties

	private var count: Int = 0

	private lazy var buyButton: UIButton = {
		let button = UIButton()
        button.setTitle("Купить", for: .normal)
		button.backgroundColor = AppColor.green
		button.layer.cornerRadius = cornerRadius
		button.layer.masksToBounds = self.layer.masksToBounds
		button.addTarget(self, action: #selector(buyButtonTapped), for: .touchUpInside)

		return button
	}()

	private lazy var textField: UITextField = {
		let textField = UITextField()

		let plusMinusButtonSize = CGRect(x: 0.0,
										 y: 0.0,
										 width: self.bounds.height * 0.5,
										 height: self.bounds.height * 0.5)

		let minusButton = UIButton(type: .custom)
		minusButton.tintColor = textFieldTextColor
		minusButton.setImage(UIImage(systemName: "minus"), for: .normal)
		minusButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -16)
		minusButton.frame = plusMinusButtonSize
		minusButton.addTarget(self, action: #selector(minusButtonTapped), for: .touchUpInside)

		let plusButton = UIButton(type: .custom)
		plusButton.tintColor = textFieldTextColor
		plusButton.setImage(UIImage(systemName: "plus"), for: .normal)
		plusButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
		plusButton.frame = plusMinusButtonSize
		plusButton.addTarget(self, action: #selector(plusButtonTapped), for: .touchUpInside)

		textField.leftView = minusButton
		textField.leftViewMode = .always
		textField.rightView = plusButton
		textField.rightViewMode = .always

		textField.layer.cornerRadius = cornerRadius
		textField.layer.masksToBounds = maskToBounds
		textField.layer.borderColor = textFieldTextColor.cgColor
		textField.layer.borderWidth = 1.0

		textField.borderStyle = .line
		textField.backgroundColor = textFieldBackgroundColor
		textField.text = quantity
		textField.textColor = textFieldTextColor
		textField.textAlignment = .center

		textField.delegate = self

		// textField.keyboardType = .numberPad

		return textField
	}()

	// MARK: - Public Properties

	let maxCount: Int = 50
	var quantity: String {
		return String(count)
	}
	var onChange: ((String) -> Void)?
	var title: String = "Button" {
		didSet {
			buyButton.setTitle(title, for: .normal)
		}
	}

	var cornerRadius: CGFloat = 10 {
		didSet {
			layer.cornerRadius = cornerRadius
			buyButton.layer.cornerRadius = cornerRadius
			textField.layer.cornerRadius = cornerRadius
		}
	}

	var maskToBounds: Bool = true {
		didSet {
			layer.masksToBounds = maskToBounds
			textField.layer.masksToBounds = maskToBounds
			buyButton.layer.masksToBounds = maskToBounds
		}
	}

	var buttonBackgroundColor: UIColor = AppColor.green {
		didSet {
			buyButton.backgroundColor = buttonBackgroundColor
		}
	}

	var textFieldBackgroundColor: UIColor = AppColor.white {
		didSet {
			textField.backgroundColor = textFieldBackgroundColor
		}
	}

	var textFieldTextColor: UIColor = AppColor.green {
		didSet {
			textField.textColor = textFieldTextColor
		}
	}

	// MARK: - Initialisers

	override init(frame: CGRect) {
		super.init(frame: frame)

		backgroundColor = .white
		layer.cornerRadius = 10
		layer.masksToBounds = maskToBounds

		addSubview(textField)
		addSubview(buyButton)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Lifecycle

	override func layoutSubviews() {
		super.layoutSubviews()

		textField.frame = self.bounds
		buyButton.frame = self.bounds
	}

	// MARK: - Private Methods

	@objc private func minusButtonTapped() {
		updateCount(action: .decrease)
	}

	@objc private func plusButtonTapped() {
		updateCount(action: .increase)
	}

	@objc private func buyButtonTapped() {
		updateCount(action: .tap)
	}
// swiftlint:disable identifier_name
	private func performAnimation(from: UIView, to: UIView) {
		UIView.transition(from: from,
						  to: to,
						  duration: 0.3,
						  options: [.transitionCrossDissolve, .showHideTransitionViews, .curveEaseInOut],
						  completion: nil)
	}
	// swiftlint:enable identifier_name

	private func updateCount(action: ActionState) {
		switch action {
		case .initial(let value):
			if value == 0 {
				count = value
				performAnimation(from: textField, to: buyButton)
			}
		case .tap:
			count += 1
			performAnimation(from: buyButton, to: textField)
		case .increase:
			if count >= maxCount { return }
			count += 1
		case .decrease:
			count -= 1
			if count < 1 {
				performAnimation(from: textField, to: buyButton)
			}
		}
		textField.text = quantity
		onChange?(quantity)
		textField.resignFirstResponder()
	}

}

extension QuantityButton: UITextFieldDelegate {

	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		// let text = "\(textField.text ?? "0")\(string)"
		// guard let count = Int(text) else { return false }

		return string.isEmpty || Int(string) != nil && textField.text?.count ?? 0 < 3

		/*
		let allowedCharacters = "1234567890"
		let allowedCharcterSet = CharacterSet(charactersIn: allowedCharacters)
		let typedCharcterSet = CharacterSet(charactersIn: string)
		return allowedCharcterSet.isSuperset(of: typedCharcterSet)
		*/
	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		return textField.resignFirstResponder()
	}

	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField.text == "" || textField.text == "0" {
			updateCount(action: .initial(0))
		} else {
			let text = Int(textField.text ?? "0") ?? 0
			count = text > maxCount ? maxCount : text
			updateCount(action: .initial(count))
		}
	}

}
