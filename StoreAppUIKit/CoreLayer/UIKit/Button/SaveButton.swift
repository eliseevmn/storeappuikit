//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class SaveButton: UIButton {

	// MARK: - Initializers

	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private Methods

	private func setup() {
		layer.cornerRadius = 10
		layer.borderColor = AppColor.green.cgColor
		layer.borderWidth = 1.0
		backgroundColor = AppColor.white

		setTitle("Сохранить", for: .normal)
		setTitleColor(AppColor.green, for: .normal)

		translatesAutoresizingMaskIntoConstraints = false
	}

}
