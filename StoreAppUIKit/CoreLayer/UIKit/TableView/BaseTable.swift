//
//  BaseTable.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import SnapKit

extension BaseTable {

    struct Section {
        var name: String = ""
        var cells: [Cell] = []
    }

    struct Cell {
        static let reuseID = "BaseTableCell"
        var data: Any?
    }
}

class BaseTable: UITableView, UITableViewDelegate, UITableViewDataSource {

    var data: [Section] = []

    var cellDidTap: ((Int) -> Void)?

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        delegate = self
        dataSource = self
        separatorStyle = .none
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        data.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = data[indexPath.section].cells[indexPath.row]
        let cell = dequeueReusableCell(withIdentifier: Cell.reuseID) as? BaseTableCell
        cell?.setup(with: item.data)

        return cell ?? UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectRow(at: indexPath, animated: true)
        cellDidTap?(indexPath.row)
    }
}
