//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class MainNavigationController: UINavigationController {

	init() {
		super.init(nibName: nil, bundle: nil)
		self.setStatusBar()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

// MARK: - Public API
extension MainNavigationController {

}

// MARK: - Private API
private extension MainNavigationController {

	func setStatusBar() {
		let arrowImage = UIImage(systemName: "arrow.left")
		let transImage = UIImage(systemName: "arrow.left")
		let navBarAppearance = UINavigationBarAppearance()

		navBarAppearance.configureWithOpaqueBackground()
		navBarAppearance.titleTextAttributes = [.foregroundColor: AppColor.black,
												.font: AppFont.montserratFont(ofSize: 18, weight: .medium)]
		navBarAppearance.largeTitleTextAttributes = [.foregroundColor: AppColor.black,
												.font: AppFont.montserratFont(ofSize: 28, weight: .medium)]
		navBarAppearance.backgroundColor = AppColor.white
		navBarAppearance.setBackIndicatorImage(arrowImage, transitionMaskImage: transImage)
		navBarAppearance.shadowColor = AppColor.white

		navigationBar.tintColor = UIColor.black
		navigationBar.standardAppearance = navBarAppearance
		navigationBar.scrollEdgeAppearance = navBarAppearance
		navigationBar.isTranslucent = true
	}
}
