//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

protocol DropDownProtocol: AnyObject {
    func dropDownPressed(filterType: FilterType)
}

final class DropDownView: UIView {

    // MARK: - Outlets
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(DropDownMenuCell.self)
        tableView.isScrollEnabled = false
        return tableView
    }()

    // MARK: - Properties
    weak var delegate: DropDownProtocol?
    var dropDownOptions = FilterType.allCases

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .init(x: 0, y: 0, width: 0, height: 0))
        tableView.selectRow(at: IndexPath(row: 5, section: 0), animated: false, scrollPosition: .none)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension DropDownView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownOptions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: DropDownMenuCell = tableView.cell(forRowAt: indexPath) else {
            return UITableViewCell()
        }
        cell.configureCell(filterType: dropDownOptions[indexPath.row])
        changeSelectedBackgroundColor(cell: cell)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.dropDownPressed(filterType: dropDownOptions[indexPath.row])
    }
}

// MARK: - ConfigureUI
private extension DropDownView {
    func configureUI() {
        self.layer.cornerRadius = 5
        self.translatesAutoresizingMaskIntoConstraints = false

        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.top.equalTo(self).offset(1)
            $0.trailing.leading.equalTo(self)
            $0.bottom.equalTo(self)
        }
    }

    func changeSelectedBackgroundColor(cell: DropDownMenuCell) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = AppColor.greenBackground
        cell.selectedBackgroundView = backgroundView
    }
}
