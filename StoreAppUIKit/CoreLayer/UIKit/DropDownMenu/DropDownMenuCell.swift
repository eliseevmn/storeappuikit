//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class DropDownMenuCell: UITableViewCell {

    // MARK: - Properties
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = AppFont.montserratFont(ofSize: 12, weight: .regular)
        label.textColor = AppColor.black
        label.textAlignment = .left
        return label
    }()

    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = AppColor.black
        return imageView
    }()

    // MARK: - Outlets

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Configure Cell
extension DropDownMenuCell {

    func configureCell(filterType: FilterType) {
        titleLabel.text = filterType.description
        iconImageView.image = filterType.image
    }
}

// MARK: - Configure Cell
extension DropDownMenuCell {

    func configureUI() {
        backgroundColor = AppColor.white

        let stackView = UIStackView()
        stackView.spacing = 10
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(UIView())
        contentView.addSubviews(stackView)

        stackView.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(10)
            $0.bottom.equalTo(contentView).offset(-10)
            $0.leading.equalTo(contentView).offset(15)
            $0.trailing.equalTo(contentView).offset(-15)
        }
    }
}
