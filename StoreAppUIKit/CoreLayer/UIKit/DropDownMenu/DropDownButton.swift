//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class DropDownButton: UIButton {

    // MARK: - Properties
    var dropView = DropDownView()
    private var height = NSLayoutConstraint()
    private var isOpenMenu: Bool = false

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: -
    override func didMoveToSuperview() {
        self.superview?.addSubview(dropView)
        self.superview?.bringSubviewToFront(dropView)

        dropView.topAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        dropView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        dropView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        height = dropView.heightAnchor.constraint(equalToConstant: 0)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isOpenMenu == false {
            openDropDown()
        } else {
            dismissDropDown()
        }
    }

    // MARK: - Helpers functions
    func dismissDropDown() {
        isOpenMenu = false
        NSLayoutConstraint.deactivate([self.height])
        self.height.constant = 0
        NSLayoutConstraint.activate([self.height])
        UIView.animate(withDuration: 0.2) {
            self.dropView.center.y -= self.dropView.frame.height / 2
            self.dropView.layoutIfNeeded()
        }
    }

    func openDropDown() {
        isOpenMenu = true
        NSLayoutConstraint.deactivate([self.height])
//        self.height.constant = self.dropView.tableView.contentSize.height
        self.height.constant = 210
        print("TODO: ", height.constant)
        NSLayoutConstraint.activate([self.height])
        UIView.animate(withDuration: 0.2) {
            self.dropView.layoutIfNeeded()
            self.dropView.center.y += self.dropView.frame.height / 2
        }
    }
}

// MARK: - Configure UI Button
private extension DropDownButton {
    func configureUI() {
        setTitleColor(AppColor.black, for: .normal)
        backgroundColor = AppColor.greenBackground
        layer.cornerRadius = 5
        setTitle(FilterType.popularFilter.description, for: .normal)
        setImage(UIImage(named: "icArrowDownFilter"), for: .normal)
        imageEdgeInsets = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
        imageEdgeInsets.left = 6
        contentMode = .scaleAspectFit
        tintColor = AppColor.black
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        semanticContentAttribute = .forceRightToLeft
        titleLabel?.font = AppFont.montserratFont(ofSize: 14, weight: .regular)
    }
}
