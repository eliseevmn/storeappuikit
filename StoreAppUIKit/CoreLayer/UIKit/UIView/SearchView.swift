//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

protocol SearchViewProtocol: AnyObject {
    func didTapSearchButtonView()
}

final class SearchView: UIView {

    // MARK: - Outlets
    lazy var textField: UITextField = SearchSarawanTextField()
    lazy var searchButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = AppColor.green
        button.layer.cornerRadius = 10
        button.setTitle("Найти", for: .normal)
        button.setTitleColor(AppColor.white, for: .normal)
        button.addTarget(self, action: #selector(handleTappedSearchButton), for: .touchUpInside)
        return button
    }()

    // MARK: - Properties
    weak var delegate: SearchViewProtocol?

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        addSubviews([textField, searchButton])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Private API
extension SearchView {
    @objc private func handleTappedSearchButton() {
        delegate?.didTapSearchButtonView()
    }
}

// MARK: - Public API
extension SearchView {

    func removeTextFieldFirstResponder() {
        textField.resignFirstResponder()
    }

    func makeTextFieldFirstResponder() {
        textField.becomeFirstResponder()
    }

    func removeTextTextField() {
        textField.text = ""
    }
}

// MARK: - Configure UI
private extension SearchView {

    func configureUI() {
        searchButton.snp.makeConstraints {
            $0.top.equalTo(self).offset(0)
            $0.bottom.equalTo(self).offset(0)
            $0.trailing.equalTo(self).offset(-20)
            $0.width.equalTo(110)
        }

        textField.snp.makeConstraints {
            $0.top.equalTo(self).offset(0)
            $0.bottom.equalTo(self).offset(0)
            $0.leading.equalTo(self).offset(20)
            $0.trailing.equalTo(searchButton.snp.leading).offset(20)
        }
    }
}
