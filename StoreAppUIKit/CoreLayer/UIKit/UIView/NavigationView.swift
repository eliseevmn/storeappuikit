//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

protocol NavigationViewProtocol: AnyObject {
    func didTapBackButton()
}

final class NavigationView: UIView {

    // MARK: - Properties
    weak var delegate: NavigationViewProtocol?
    private let typeFoodQyery: TypeFoodQuery?
    private let titleName: String?

    // MARK: - Outlets
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "icArrowBack"), for: .normal)
        button.tintColor = AppColor.black
        button.addTarget(self, action: #selector(handleBackButtonTapped), for: .touchUpInside)
        button.imageView?.contentMode = .scaleToFill
        return button
    }()

    private lazy var titleNameLabel: UILabel = {
        let label = SarawanLabel(title: Texts.CommonInfo.info,
                                 alignment: .left,
                                 fontSize: AppFont.montserratFont(ofSize: 20, weight: .semiBold),
                                numberLines: 2)
        return label
    }()

    // MARK: - Init
    init(typeFoodQyery: TypeFoodQuery?,
         titleName: String? = nil) {
        self.typeFoodQyery = typeFoodQyery
        self.titleName = titleName
        super.init(frame: .zero)
        backgroundColor = AppColor.white
        addSubviews([backButton, titleNameLabel])

        switch typeFoodQyery {
        case .productsPopular(catalogFoodModel: let catalogFoodModel):
            titleNameLabel.text = catalogFoodModel.name ?? "Назад"
        case .productsBySearchName(foodName: let foodName):
            titleNameLabel.text = foodName
        case .none:
            titleNameLabel.text = titleName
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }

    @objc private func handleBackButtonTapped() {
        delegate?.didTapBackButton()
    }
}

// MARK: - Configure UI
private extension NavigationView {

    func configureUI() {
        backButton.snp.makeConstraints {
            $0.leading.equalTo(self).offset(-5)
            $0.centerY.equalTo(self).offset(0)
            $0.width.equalTo(30)
            $0.height.equalTo(25)
        }
        titleNameLabel.snp.makeConstraints {
            $0.leading.equalTo(backButton.snp.trailing).offset(0)
            $0.trailing.equalTo(self).offset(0)
            $0.centerY.equalTo(self).offset(0)
        }
    }
}
