//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class LogoView: UIView {

    // MARK: - Outlets
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icLogo")
        imageView.contentMode = .scaleAspectFit
//        imageView.tintColor = AppColor.green
        return imageView
    }()

    private lazy var logoTitleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icLogoTitle")
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = AppColor.greenMain
        return imageView
    }()

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        addSubviews([logoImageView, logoTitleImageView])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
}

// MARK: - Configure UI
private extension LogoView {

    func configureUI() {
        self.snp.makeConstraints {
            $0.height.equalTo(40)
        }

        logoImageView.snp.makeConstraints {
            $0.leading.equalTo(self).offset(20)
            $0.top.bottom.equalTo(self).offset(0)
            $0.width.equalTo(37)
        }
        logoTitleImageView.snp.makeConstraints {
            $0.leading.equalTo(logoImageView.snp.trailing).offset(10)
            $0.centerY.equalTo(logoImageView.snp.centerY).offset(0)
        }
    }
}
