//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class VerificationCodeTextField: UITextField {

	// MARK: - Public Properties

	var didEnterLastDigit: ((String) -> Void)?
	var isCorrect: Bool = false {
		willSet {
			digitLabels.forEach { $0.layer.borderColor = newValue ? UIColor.systemGray.cgColor : UIColor.systemRed.cgColor }
		}
	}

	// MARK: - Private Properties

	private var isConfigure = false
	private var digitLabels = [UILabel]()

	private lazy var tapRecognizer: UITapGestureRecognizer = {
		let recognizer = UITapGestureRecognizer()
		recognizer.addTarget(self, action: #selector(becomeFirstResponder))
		return recognizer
	}()

	override init(frame: CGRect) {
		super.init(frame: frame)
		configure()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	convenience init(with slotCount: Int) {
		self.init(frame: .zero)
		configure(with: slotCount)
	}

	// MARK: - Public Methods

	func changeBorderColor() {
		digitLabels.forEach { $0.layer.borderColor = UIColor.systemPink.cgColor }
	}

	// MARK: - Private Methods

	private func configure(with slotCount: Int = 6) {
		guard !isConfigure else { return }
		isConfigure.toggle()
		setupTextField()

		let labelsStackView = createLabelsStackView(with: slotCount)
		addSubview(labelsStackView)

		addGestureRecognizer(tapRecognizer)

		labelsStackView.snp.makeConstraints {
			$0.top.equalTo(self.snp.top)
			$0.leading.equalTo(self.snp.leading)
			$0.trailing.equalTo(self.snp.trailing)
			$0.bottom.equalTo(self.snp.bottom)
		}
	}

	private func setupTextField() {
		delegate = self
		tintColor = .clear
		textColor = .clear
		keyboardType = .numberPad
		textContentType = .oneTimeCode

		addTarget(self, action: #selector(textDidChange), for: .editingChanged)
	}

	private func createLabelsStackView(with count: Int) -> UIStackView {
		let stackView = UIStackView()
		stackView.translatesAutoresizingMaskIntoConstraints = false
		stackView.axis = .horizontal
		stackView.alignment = .fill
		stackView.distribution = .fillEqually
		stackView.spacing = 8

		for _ in 1 ... count {
			let label = UILabel()
			label.translatesAutoresizingMaskIntoConstraints = false
			label.textAlignment = .center
			label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
			label.backgroundColor = .systemGray
			label.layer.borderWidth = 1.0
			label.layer.borderColor = UIColor.systemGray.cgColor

			label.isUserInteractionEnabled = true

			stackView.addArrangedSubview(label)

			digitLabels.append(label)
		}

		return stackView
	}

	@objc private func textDidChange() {
		guard let text = self.text, text.count <= digitLabels.count else { return }

		for ind in 0 ..< digitLabels.count {
			let currentLabel = digitLabels[ind]
			if ind < text.count {
				let index = text.index(text.startIndex, offsetBy: ind)
				currentLabel.text = String(text[index])
			} else {
				currentLabel.text?.removeAll()
			}
		}

		if text.count == digitLabels.count {
			didEnterLastDigit?(text)
		}
	}

}

extension VerificationCodeTextField: UITextFieldDelegate {

	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let charCount = textField.text?.count else { return false }
		return charCount < digitLabels.count || string.isEmpty
	}

}
