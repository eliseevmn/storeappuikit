//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class SearchSarawanTextField: UITextField {

    // MARK: - Properties
    private let padding = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 47)

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: .zero)

        setupLeftImage(imageName: "icSearch")
        setupRightImage(imageName: "icDictation")
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: -
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

// MARK: - Private Api
private extension SearchSarawanTextField {

    func configureUI() {
        backgroundColor = AppColor.white
        layer.cornerRadius = 10
        layer.borderColor = AppColor.green.cgColor
        layer.borderWidth = 1

        let placeHolderAttribited = NSAttributedString(string: "Искать в Сарафане",
                                                       attributes: [.foregroundColor: AppColor.grey])
        attributedPlaceholder = placeHolderAttribited
    }

    func setupRightImage(imageName: String) {
        let imageView = UIImageView(frame: CGRect(x: 5, y: 12, width: 10, height: 16))
        imageView.image = UIImage(named: imageName)
        let imageContainerView: UIView = UIView(frame: CGRect(x: 20, y: 0, width: 45, height: 40))
        imageContainerView.addSubview(imageView)
        rightView = imageContainerView
        rightViewMode = .always
        tintColor = AppColor.grey
    }

    func setupLeftImage(imageName: String) {
        let imageView = UIImageView(frame: CGRect(x: 5, y: 12, width: 16, height: 16))
        imageView.image = UIImage(named: imageName)
        let imageContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 40))
        imageContainerView.addSubview(imageView)
        leftView = imageContainerView
        leftViewMode = .always
        tintColor = AppColor.grey
    }
}
