//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class STextField: UITextField {

    // MARK: - Private Properties

    private let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setup() {
        layer.cornerRadius = 10

        textColor = UIColor.black
        textAlignment = .left
		font = AppFont.montserratFont(ofSize: 14, weight: .regular)
        adjustsFontSizeToFitWidth = true
        minimumFontSize = 12
        backgroundColor = AppColor.greenBackground

        autocorrectionType = .no
        returnKeyType = .go

        translatesAutoresizingMaskIntoConstraints = false
    }

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

}
