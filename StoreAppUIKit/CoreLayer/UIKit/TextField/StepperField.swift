//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class StepperField: UIStackView {

	// MARK: - Private Properties

	private lazy var minusButton: UIButton = {
		let button = UIButton(type: .custom)
		button.tintColor = borderColor
		button.setImage(UIImage(systemName: "minus"), for: .normal)
		button.addTarget(self, action: #selector(minusButtonTapped), for: .touchUpInside)

		return button
	}()

	private lazy var plusButton: UIButton = {
		let button = UIButton(type: .custom)
		button.tintColor = borderColor
		button.setImage(UIImage(systemName: "plus"), for: .normal)
		button.addTarget(self, action: #selector(plusButtonTapped), for: .touchUpInside)

		return button
	}()

	private lazy var textField: UITextField = {
		let field = UITextField()
		field.layer.borderColor = borderColor.cgColor
		field.layer.borderWidth = 1.0
		field.textColor = borderColor
		field.text = String(quantity)
		field.textAlignment = .center

		field.delegate = self

		return field
	}()

	// MARK: - Public Properties

	var maxCount: Int = 50
	var quantity: Int = 0 {
		didSet {
			updateCount(value: quantity)
		}
	}
	var onChange: ((Int) -> Void)?

	var borderColor: UIColor = AppColor.black {
		didSet {
			textField.layer.borderColor = borderColor.cgColor
			textField.textColor = borderColor
			minusButton.tintColor = borderColor
			plusButton.tintColor = borderColor
		}
	}

	// MARK: - Initialisers

	override init(frame: CGRect) {
		super.init(frame: frame)

		axis = .horizontal
		alignment = .fill
		distribution = .equalSpacing
		spacing = 1

		addArrangedSubview(minusButton)
		addArrangedSubview(textField)
		addArrangedSubview(plusButton)
	}

	required init(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Lifecycle

	override func layoutSubviews() {
		super.layoutSubviews()

		textField.bounds.size = .init(width: self.bounds.size.height, height: self.bounds.size.height)
	}

	// MARK: - Private Methods

	private func updateCount(value: Int) {
		textField.text = String(value)
		// onChange?(value)
		// textField.resignFirstResponder()
	}

	@objc private func minusButtonTapped() {
		if quantity == 0 { return }
		quantity -= 1
		onChange?(quantity)
	}

	@objc private func plusButtonTapped() {
		if quantity >= maxCount { return }
		quantity += 1
		onChange?(quantity)
	}

}

extension StepperField: UITextFieldDelegate {

	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		// запрет вводить символы кроме цифр и не более 3х
		return string.isEmpty || Int(string) != nil && textField.text?.count ?? 0 < 3
	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		return textField.resignFirstResponder()
	}

	func textFieldDidEndEditing(_ textField: UITextField) {
			let text = Int(textField.text ?? "0") ?? 0
		quantity = text > maxCount ? maxCount : text
		textField.resignFirstResponder()
	}

}
