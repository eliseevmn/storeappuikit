//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import SnapKit

class BaseTableCell: UITableViewCell {

	// MARK: - Initializers

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setup()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func setup() {}

	func setup(with data: Any?) {}

}
