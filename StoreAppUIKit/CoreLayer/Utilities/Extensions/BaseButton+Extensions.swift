//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

extension BaseButton {

	var isValid: Bool {
		get { isEnabled && backgroundColor == AppColor.green }
		set {
			backgroundColor = newValue ? AppColor.green.withAlphaComponent(0.3) : AppColor.green
			isEnabled = newValue
		}
	}
}
