//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

extension UIButton {

	var tapPublisher: EventPublisher {
		publisher(for: .touchUpInside)
	}

}
