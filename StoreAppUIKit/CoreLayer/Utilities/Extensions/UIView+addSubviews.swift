//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

extension UIView {

	func addSubviews(_ subviews: [UIView]) {
		subviews.forEach(self.addSubview)
	}

	func addSubviews(_ subviews: UIView...) {
		subviews.forEach(self.addSubview)
	}
}
