//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit
import Combine

extension UITextField {

	var textPublisher: AnyPublisher<String, Never> {
		publisher(for: .editingChanged)
			.map { self.text ?? "" }
			.eraseToAnyPublisher()
	}

//	var textPublisher: AnyPublisher<String, Never> {
//		NotificationCenter.default
//			.publisher(for: UITextField.textDidEndEditingNotification, object: self)
//			.compactMap { $0.object as? UITextField } // receiving notifications with objects which are instances of UITextFields
//			.compactMap(\.text) // extracting text and removing optional values (even though the text cannot be nil)
//			.eraseToAnyPublisher()
//	}
}
