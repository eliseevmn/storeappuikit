//
//  AppFont.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

struct AppFont {

    enum MontserratWeight: String {
        case regular = "-Regular"
        case medium = "-Medium"
        case semiBold = "-SemiBold"
    }

    static func montserratFont(ofSize size: CGFloat = UIFont.systemFontSize, weight: MontserratWeight = .regular) -> UIFont {
        return UIFont(name: "Montserrat\(weight.rawValue)", size: size) ?? .systemFont(ofSize: size)
    }
}
