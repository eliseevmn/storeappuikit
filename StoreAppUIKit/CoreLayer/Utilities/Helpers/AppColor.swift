//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

enum AppColor {

    private static let missingColor: UIColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)

    static let green = UIColor(named: "primary") ?? missingColor
    static let orange = UIColor(named: "secondary") ?? missingColor
    static let black = UIColor(named: "black") ?? missingColor
    static let grey = UIColor(named: "grey") ?? missingColor
    static let white = UIColor(named: "white") ?? missingColor
    static let lightGray = UIColor(named: "lightGray") ?? missingColor
    static let greenMain = UIColor(named: "greenMain") ?? missingColor

    static let greenBackground = UIColor(named: "greenBackground") ?? missingColor
    static let orangeBackground = UIColor(named: "orangeBackground") ?? missingColor
    static let favorableBackground = UIColor(named: "favorableBackground") ?? missingColor
}
