//
//  AppCoordinator.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

final class AppCoordinator: BaseCoordinator {

    // MARK: - Private Properties

    private let coordinatorFactory: CoordinatorFactory
    private let router: Router

    // MARK: - Initialisers

    init(router: Router, coordinatorFactory: CoordinatorFactory) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory
    }

    // MARK: - Public Methods

    override func start() {
        runTabBar()
    }

    // MARK: - Private Methods

    private func runTabBar() {
        let tabBarCoordinator = coordinatorFactory.makeTabBarCoordinator(with: router)
        retain(tabBarCoordinator)
        tabBarCoordinator.start()
    }
}

#warning("#1 расскомментировать для авторизации поверх приложения")
/* Для логина на все приложение удалить ВЕСЬ класс сверху, и раскомментировать ниже

final class AppCoordinator: BaseCoordinator {
    // MARK: - Private Properties

    private let coordinatorFactory: CoordinatorFactory
    private let router: Router

    private var isLogin = false

    // MARK: - Initialisers

    init(router: Router, coordinatorFactory: CoordinatorFactory) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory
    }

    // MARK: - Public Methods

    override func start() {
        if isLogin {
            runTabBar()
        } else {
            runLoginFlow()
        }
    }

    // MARK: - Private Methods

    private func runTabBar() {
        let tabBarCoordinator = coordinatorFactory.makeTabBarCoordinator(with: router)
        tabBarCoordinator.onLogout = { [weak self] isLogin in
            self?.isLogin = isLogin
            self?.start()
        }
        retain(tabBarCoordinator)
        tabBarCoordinator.start()
    }

    private func runLoginFlow() {
        let loginCoordinator = coordinatorFactory.makeLoginCoordinator(with: router)

        retain(loginCoordinator)
        loginCoordinator.start()

        isLogin = loginCoordinator.isLogin ?? false

        loginCoordinator.finishFlow = { [weak self, weak loginCoordinator] isLogin in
            self?.isLogin = isLogin
            self?.release(loginCoordinator)
            self?.start()
        }

        if isLogin {
            release(loginCoordinator)
            start()
        }
    }

}
*/
