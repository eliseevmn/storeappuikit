//
//  SceneDelegate.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    fileprivate var appCoordinator: Coordinatable?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        runUI()
    }
}

extension SceneDelegate {

    fileprivate func runUI() {

        let navigationController = UINavigationController()
        let router = Router(rootController: navigationController)
        let moduleFactory = ModuleFactory()
        let coordinatorFactory = CoordinatorFactory(moduleFactory: moduleFactory)
        self.appCoordinator = coordinatorFactory.makeTabBarCoordinator(with: router)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        self.appCoordinator?.start()
    }

    #warning("#2 расскомментировать для авторизации поверх приложения")
    /* Для логина на все приложение удалить runUI() сверху, и раскомментировать ниже
    fileprivate func runUI() {

        let navigationController = MainNavigationController()
        let router = Router(rootController: navigationController)
        let moduleFactory = ModuleFactory()
        let coordinatorFactory = CoordinatorFactory(moduleFactory: moduleFactory)
        self.appCoordinator = coordinatorFactory.makeApplicationCoordinator(with: router)

        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        self.appCoordinator?.start()
    }
    */
}
