//
//  ProductModel.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct ProductModel: Decodable {
    let idProduct: Int
    let name: String
    let image: String
    let storeName: String
    let priceFood: String
    let priceType: StringLiteralType
    let discountFood: String

    init(productResponse: ProductResponse) {
        self.idProduct = productResponse.idProduct
        self.name = productResponse.name
        self.image = productResponse.images.first?.imageUrl ?? "icDefaultFood"
        self.storeName = productResponse.storePrices.first?.storeName ?? "-"
        self.priceFood = productResponse.storePrices.first?.priceFood ?? "-"
        self.priceType = productResponse.priceType
        self.discountFood = "\(productResponse.storePrices.first?.discount ?? 0) %"
    }
}
