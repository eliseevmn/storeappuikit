//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct MainCategoryModel {

    let idMainCategory: Int?
    let name: String?
    let image: String?

    init(mainCategoryResponse: MainCategoryResponse) {
        self.idMainCategory = mainCategoryResponse.idMainCategory
        self.name = mainCategoryResponse.name
        self.image = mainCategoryResponse.image
    }
}
