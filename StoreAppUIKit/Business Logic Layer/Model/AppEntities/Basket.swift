//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct Basket {
	let totalQuantity: Int
	let totalCost: Double
	let totalWeight: Double
	var products: [Product]

	init() {
		totalQuantity = 0
		totalCost = 0.0
		totalWeight = 0.0
		products = []
	}

	init(response: BasketResponse) {
		totalQuantity = response.totalQuantity
		totalCost = response.totalCost
		totalWeight = response.totalWeight
		products = response.products.map { (product) -> Product in
			Product(product: product)
		}
	}

}

extension Basket {
	struct Product {
		struct ProductInformation {
			let brand: String
			let manufacturingCountry: String

			init(string: String) {
				let dictionary = ProductInformation.convertToDictionary(from: string)
				brand = dictionary["brand"] ?? ""
				manufacturingCountry = dictionary["manufacturing_country"] ?? ""
			}

			private static func convertToDictionary(from text: String) -> [String: String] {
				let replacedString = text.replacingOccurrences(of: "'", with: "\"")
				guard let data = replacedString.data(using: .utf8) else { return [:] }
				let jsonObject: Any? = try? JSONSerialization.jsonObject(with: data, options: [])
				return jsonObject as? [String: String] ?? [:]
			}
		}

		let basketProductId: Int // по нему удалять
		let productId: Int // по нему запрашивать к api product
		let productStoreId: Int // по нему обновлять количество
		let store: String // магазин
		let name: String //  название продукта
		let priceType: String // цена за единиицу( 100р за штуку, коробку, за 100г...)
		let productInformation: ProductInformation // содержит брэнд, производитель и пр.
		let images: [String]
		let price: String
		var quantity: Int

		init(product: BasketResponse.BasketProduct) {
			basketProductId = product.basketProductId
			productId = product.storeProduct.productItSelf.productId
			productStoreId = product.storeProduct.productStoreId
			store = product.storeProduct.store
			name = product.storeProduct.productItSelf.name
			priceType = product.storeProduct.productItSelf.priceType
			productInformation = ProductInformation(string: product.storeProduct.productItSelf.information.productInformation)
			images = product.storeProduct.productItSelf.images.map { $0.image }
			price = product.storeProduct.price
			quantity = product.quantity
		}
	}

}
