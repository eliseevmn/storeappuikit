//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct Delivery {
	enum PaymentOption: String {
		case card = "Картой курьеру"
		case cash = "наличные курьеру"
	}
	var address: AAddress = AAddress()
	// let address: String = "Москва, ул. Несуществуюящая, д.15/2, кв. 12"
	let deliveryTime: String = ""
	let paymentOption: PaymentOption = .cash
}
