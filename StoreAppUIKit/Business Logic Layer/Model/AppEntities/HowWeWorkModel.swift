//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

class HowWeWorkModel {

    let idNumber: Int
    let icon: String
    let description: String

    init(idNumber: Int, icon: String, description: String) {
        self.idNumber = idNumber
        self.icon = icon
        self.description = description
    }
}
