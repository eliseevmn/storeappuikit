//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import UIKit

enum FilterType: CaseIterable {
    case upPriceFilter
    case downPriceFilter
    case alphabetFilter
    case upDiscountFilter
    case downDiscountFilter
    case popularFilter

    var description: String {
        switch self {
        case .upPriceFilter:
            return "Цене"
        case .downPriceFilter:
            return "Цене"
        case .alphabetFilter:
            return "Алфавиту"
        case .upDiscountFilter:
            return "Скидке"
        case .downDiscountFilter:
            return "Скидке"
        case .popularFilter:
            return "Популярные"
        }
    }

    var image: UIImage {
        switch self {
        case .upPriceFilter:
            return UIImage(named: "icArrowUp") ?? UIImage()
        case .downPriceFilter:
            return UIImage(named: "icArrowDown") ?? UIImage()
        case .alphabetFilter:
            return UIImage(named: "") ?? UIImage()
        case .upDiscountFilter:
            return UIImage(named: "icArrowUp") ?? UIImage()
        case .downDiscountFilter:
            return UIImage(named: "icArrowDown") ?? UIImage()
        case .popularFilter:
            return UIImage(named: "") ?? UIImage()
        }
    }
}
