//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

// Класс, который делает промежуточные расчеты, последовательные запросы нескольких API методов.
// В этом классе формируются готовые методы которые будут вызваны из ViewModel
// Думаю здесь делать всю необходимую фильтрацию, валидацию, map'ы чтобы VM получала уже чистые данные для отображения.

// swiftlint:disable identifier_name
protocol ProductServiceProtocol {
	func getProduct(by id: Int) -> AnyPublisher<ProductModel, Error>
    func getCategories() -> AnyPublisher<[MainCategoryResponse], Error>
    func getProductsByTypeFilters(isPopularProducts: Bool?,
                                  nameFood: String?,
                                  categoryId: Int?,
                                  filters: FiltersParameters?,
                                  isDiscountProducts: Bool?) -> AnyPublisher<AllProductsResponse, Error>
}

final class ProductService: ProductServiceProtocol {

	// MARK: - Private Properties

	private let productAPIClient: ProductAPIClient

	// MARK: - Initialisers

	init(productAPIClient: ProductAPIClient) {
		self.productAPIClient = productAPIClient
	}

	// MARK: - Public Methods

	func getProduct(by id: Int) -> AnyPublisher<ProductModel, Error> {
		return productAPIClient.product(by: id)
			.eraseToAnyPublisher()
	}

    func getCategories() -> AnyPublisher<[MainCategoryResponse], Error> {
        return productAPIClient.getAllCategories()
            .eraseToAnyPublisher()
    }

    func getProductsByTypeFilters(isPopularProducts: Bool?,
                                  nameFood: String?,
                                  categoryId: Int?,
                                  filters: FiltersParameters?,
                                  isDiscountProducts: Bool?) -> AnyPublisher<AllProductsResponse, Error> {
        return productAPIClient.getProductsByTypeFilters(isPopularProducts: isPopularProducts,
                                                         nameFood: nameFood,
                                                         categoryId: categoryId,
                                                         filters: filters,
                                                         isDiscountProducts: isDiscountProducts)
    }
}
