//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

protocol UserProfileServiceProtocol {
	var userId: Int { get }
	func getUser(by id: Int) -> AnyPublisher<User, Error>
}

protocol AddressProfileServiceProtocol {
	func getAddresses() -> AnyPublisher<[Address], Error>
}

protocol ProfileServiceProtocol: UserProfileServiceProtocol, AddressProfileServiceProtocol {
	func removeToken()
}

final class ProfileService: ProfileServiceProtocol {

	// MARK: - Public Properties

	var userId: Int {
		guard let userId = tokenRepository.userId else { return 0 }
		return userId
	}

	// MARK: - Private Properties

	private let userAPIClient: UserAPIClient
	private let tokenRepository: TokenStorable

	// MARK: - Initializers

	init(userAPIClient: UserAPIClient, tokenRepository: TokenStorable) {
		self.userAPIClient = userAPIClient
		self.tokenRepository = tokenRepository
	}

	// MARK: - Public Methods

	func getUser(by id: Int) -> AnyPublisher<User, Error> {
		return userAPIClient.user(by: id, token: tokenRepository.token ?? "")
			.eraseToAnyPublisher()
	}

	func getAddresses() -> AnyPublisher<[Address], Error> {
		return userAPIClient.addresses(token: tokenRepository.token ?? "")
			.eraseToAnyPublisher()
	}
	func removeToken() {
		tokenRepository.token = nil
	}

}
