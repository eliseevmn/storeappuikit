//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

protocol BasketServiceProtocol {
	func getBasket() -> AnyPublisher<Basket, Error>
	func add(product: (productId: Int, quantity: Int)) -> AnyPublisher<BasketUpdate, Error>
	func update(basketWith product: (productId: Int, quantity: Int)) -> AnyPublisher<BasketUpdate, Error>
	func remove(productBy basketProductId: Int) -> AnyPublisher<EmptyResponse, Error>
	func clear() -> AnyPublisher<EmptyResponse, Error>

	func getUserData() -> AnyPublisher<[OrderStartResponse], Error>
	func getOrderDeliveryCost(forAddress addressId: Int) -> AnyPublisher<OrderCalculateResponse, Error>
	func makeOrder(forAddress addressId: Int) -> AnyPublisher<OrderApproveResponse, Error>
}

final class BasketService: BasketServiceProtocol {

	// MARK: - Private Properties

	private let basketAPIClient: BasketAPIClient
	private let orderAPIClient: OrderAPIClient
	private let tokenRepository: TokenStorable
	private var basketId: Int = -1
	private var token: String {
		"ccff232e5712d705e971142b6b8fe207fc972676"// tokenRepository.token ?? ""
	}

	// MARK: - Initialisers

	init(basketAPIClient: BasketAPIClient, orderAPIClient: OrderAPIClient, tokenRepository: TokenStorable) {
		self.basketAPIClient = basketAPIClient
		self.orderAPIClient = orderAPIClient
		self.tokenRepository = tokenRepository
	}

	// MARK: - Public Methods

	func getBasket() -> AnyPublisher<Basket, Error> {
		return basketAPIClient.get(basketWithToken: token)
			.map { [unowned self] (basketResponse) -> Basket in
				self.basketId = basketResponse.basketId
				return Basket(response: basketResponse)
			}
			.eraseToAnyPublisher()
	}

	func add(product: (productId: Int, quantity: Int)) -> AnyPublisher<BasketUpdate, Error> {
		return basketAPIClient.add(product: product, token: token)
			// map here if needed
			.eraseToAnyPublisher()
	}

	func update(basketWith product: (productId: Int, quantity: Int)) -> AnyPublisher<BasketUpdate, Error> {
		return basketAPIClient.update(forBasketId: basketId, with: product, token: token)
			// map here if needed
			.eraseToAnyPublisher()
	}

	func remove(productBy basketProductId: Int) -> AnyPublisher<EmptyResponse, Error> {
		return basketAPIClient.delete(productBy: basketProductId, token: token)
			.eraseToAnyPublisher()
	}

	func clear() -> AnyPublisher<EmptyResponse, Error> {
		return basketAPIClient.clear(basketWithToken: token)
			.eraseToAnyPublisher()
	}

	func getUserData() -> AnyPublisher<[OrderStartResponse], Error> {
		return orderAPIClient.orderStart(token: token)
			.eraseToAnyPublisher()
	}

	func getOrderDeliveryCost(forAddress addressId: Int) -> AnyPublisher<OrderCalculateResponse, Error> {
		return orderAPIClient.createOrderCalculation(forAddress: addressId, token: token)
			.eraseToAnyPublisher()
	}

	func makeOrder(forAddress addressId: Int) -> AnyPublisher<OrderApproveResponse, Error> {
		return orderAPIClient.createOrderApproval(forAddress: addressId, token: token)
			.eraseToAnyPublisher()
	}
}
