//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

enum FiltersParameters {
    case upPrice
    case downPrice
    case upDiscount
    case downDiscount
    case name

    var description: String {
        switch self {
        case .upPrice:
            return "price"
        case .downPrice:
            return "-price"
        case .upDiscount:
            return "discount"
        case .downDiscount:
            return "-discount"
        case .name:
            return "name"
        }
    }
}
