//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct OrderCalculateResponse: Decodable {
	let basketCount: Int
	let basketSumm: Double
	let paymentAmount: Double
	let deliveryAmount: Double

	init() {
		basketCount = 0
		basketSumm = 0.0
		paymentAmount = 0.0
		deliveryAmount = 0.0
	}
}

struct OrderApproveResponse: Decodable {
	let orderId: Int
	let orderName: String
	let createdDatetime: String
	let vehicleTypeId: Int
	let paymentAmount: String
	let deliveryFeeAmount: String
	let deliveryAmount: String
	let trackingUrl: String
	let totalWeightKg: Int
	let status: String
	let error: Bool
}

struct OrderStartResponse: Decodable {
	let firstName: String
	let lastName: String
	let phone: String
	let email: String
	let address: [AAddress]
}
#warning("Изменить имя или взять модель из Profile. Если подходит, то написать инит через OrderResponse")
struct AAddress: Codable {
	var addressId: Int = 0
	var city: String = ""
	var street: String = ""
	var house: String = ""
	var building: String = ""
	var housing: String = ""
	var roomNumber: String = ""
	var primary: Bool = true

	enum CodingKeys: String, CodingKey {
		case addressId = "id"
		case city
		case street
		case house
		case building
		case housing
		case roomNumber
		case primary
	}

	var fullAddress: String {
		return "\(self)"
	}

	var isEmptyAddress: Bool {
		city.isEmpty || street.isEmpty || house.isEmpty
	}
}

extension String.StringInterpolation {
	mutating func appendInterpolation(_ address: AAddress) {
		if address.isEmptyAddress { appendLiteral("Адрес"); return }
		appendLiteral("\(address.street), д. \(address.house)")
		if !address.building.isEmpty { appendLiteral(", корп. \(address.building)") }
		if !address.housing.isEmpty { appendLiteral(", стр. \(address.housing)") }
		if !address.roomNumber.isEmpty { appendLiteral(", кв. \(address.roomNumber)") }
	}
}
