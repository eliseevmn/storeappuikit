//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct User: Codable {
	var firstName: String
	var lastName: String
	var email: String
	var image: String?
	var birthday: String?
	var phone: String
	var status: String

	var fullName: String {
		if isEmptyName { return "Добавить имя"}
		return "\(firstName) \(lastName)"
	}

	var isEmptyName: Bool {
		firstName.isEmpty || lastName.isEmpty
	}
}
