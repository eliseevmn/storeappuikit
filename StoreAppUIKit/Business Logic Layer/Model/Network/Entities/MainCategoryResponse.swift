//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct MainCategoryResponse: Decodable {
    let idMainCategory: Int?
    let name: String?
    let image: String?
    let categories: [CategoriesResponse]
}
