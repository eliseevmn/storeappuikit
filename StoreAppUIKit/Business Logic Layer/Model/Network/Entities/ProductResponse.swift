//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct ProductResponse: Decodable {
    let idProduct: Int
    let name: String
    let images: [ImageResponse]
    let storePrices: [StorePrice]
    let priceType: String

    enum CodingKeys: String, CodingKey {
        case idProduct = "id"
        case name
        case images
        case storePrices
        case priceType
    }
}

struct ImageResponse: Decodable {
    let idImage: Int?
    let imageUrl: String?

    enum CodingKeys: String, CodingKey {
        case idImage = "id"
        case imageUrl = "image"
    }
}

struct StorePrice: Decodable {
    let idStore: Int?
    let storeName: String?
    let priceFood: String?
    let discount: Int?

    enum CodingKeys: String, CodingKey {
        case idStore = "id"
        case storeName = "store"
        case priceFood = "price"
        case discount
    }
}
