//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct BasketResponse: Decodable {
	let basketId: Int
	let products: [BasketProduct]
	let totalQuantity: Int
	let totalCost: Double
	let totalWeight: Double

	struct BasketProduct: Decodable {
		let basketProductId: Int
		let storeProduct: BasketProductStorePrices
		let quantity: Int

		enum CodingKeys: String, CodingKey {
			case basketProductId
			case storeProduct = "product"
			case quantity
		}
	}

	struct BasketProductStorePrices: Decodable {
		let productStoreId: Int
		let store: String
		let productItSelf: ProductShort
		let price: String
		let discount: Int?

		enum CodingKeys: String, CodingKey {
			case productStoreId
			case store
			case productItSelf = "product"
			case price
			case discount
		}
	}

	struct ProductShort: Decodable {
		let productId: Int
		let name: String
		let information: Information
		let priceType: String
		let unitQuantity: String
		let images: [ProductImage]
		let isFavorite: Bool?
		let storePrices: [BasketStorePrice]

		enum CodingKeys: String, CodingKey {
			case productId = "id"
			case name
			case information
			case priceType
			case unitQuantity
			case images
			case isFavorite
			case storePrices
		}
	}
	struct Information: Decodable {
		let productInformation: String
	}

	struct ProductImage: Decodable {
		let imageId: Int
		let image: String

		enum CodingKeys: String, CodingKey {
			case imageId = "id"
			case image
		}
	}

	struct BasketStorePrice: Decodable {
		let storeName: String
		let storePrice: Double
	}
}

struct BasketUpdate: Decodable {
	let basketId: Int?
	let products: [BasketProductUpdate]

	enum CodingKeys: String, CodingKey {
		case basketId = "id"
		case products
	}
}

struct BasketProductUpdate: Decodable {
	let productId: Int
	let quantity: Int

	enum CodingKeys: String, CodingKey {
		case productId = "product"
		case quantity
	}
}
