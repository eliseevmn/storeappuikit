//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct CategoriesResponse: Decodable {
    let idCategory: Int?
    let name: String?
}
