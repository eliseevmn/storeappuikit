//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation

struct AllProductsResponse: Decodable {

    let count: Int?
    let next: String?
    let previous: String?
    let results: [ProductResponse]
}
