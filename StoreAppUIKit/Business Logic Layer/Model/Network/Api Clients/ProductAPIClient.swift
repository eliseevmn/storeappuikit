//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

// Класс, который содержит все API методы для юзера. Здесь идет настройка запроса через RequestBuilder.
// Возвращает Паблишер, который содержит в себе запрашиваемые данные. Где-то это готовые entity, где-то response как в products.
// Фактического запроса не происходит. Он будет только когда появится подписчик. Все подписки предлагаю делать во VM.

// swiftlint:disable identifier_name
protocol ProductAPIClient {
	func product(by id: Int) -> AnyPublisher<ProductModel, Error>
    func getAllCategories() -> AnyPublisher<[MainCategoryResponse], Error>
    func getProductsByTypeFilters(isPopularProducts: Bool?,
                                  nameFood: String?,
                                  categoryId: Int?,
                                  filters: FiltersParameters?,
                                  isDiscountProducts: Bool?) -> AnyPublisher<AllProductsResponse, Error>
}

extension APIClient: ProductAPIClient {

	func product(by id: Int) -> AnyPublisher<ProductModel, Error> {
		let request = requestBuilder
			.set(path: .product(id: id))
			.set(method: .GET)
			.build()

		return performRequest(request)
	}

    func getAllCategories() -> AnyPublisher<[MainCategoryResponse], Error> {
        let request = requestBuilder
            .set(path: .categories)
            .set(method: .GET)
            .build()
        return performRequest(request)
    }

    func getProductsByTypeFilters(isPopularProducts: Bool?,
                                  nameFood: String?,
                                  categoryId: Int?,
                                  filters: FiltersParameters?,
                                  isDiscountProducts: Bool?) -> AnyPublisher<AllProductsResponse, Error> {
        let request = requestBuilder
            .set(path: .getProducts)
            .set(method: .GET)
            .set(parameters: ["categoryId": "\(categoryId ?? 0)",
                              "popular_products": String(isPopularProducts ?? false),
                              "product_name": nameFood ?? "",
                              "order": "\(filters?.description ?? "price")",
                              "discount_products": String(isDiscountProducts ?? false)])
            .build()
        return performRequest(request)
    }
}
