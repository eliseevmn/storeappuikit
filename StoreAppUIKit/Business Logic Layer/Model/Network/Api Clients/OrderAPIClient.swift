//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

protocol OrderAPIClient {
	#warning("Посмотреть может можно взять модель user c экрана Profile вместо OrderStartResponse ")
	func orderStart(token: String) -> AnyPublisher<[OrderStartResponse], Error>
	func createOrderCalculation(forAddress addressId: Int, token: String) -> AnyPublisher<OrderCalculateResponse, Error>
	func createOrderApproval(forAddress addressId: Int, token: String) -> AnyPublisher<OrderApproveResponse, Error>
}

extension APIClient: OrderAPIClient {
	func orderStart(token: String) -> AnyPublisher<[OrderStartResponse], Error> {
		let request = requestBuilder
			.set(path: .orderStart)
			.set(method: .GET)
			.add(header: "Authorization", value: "Token \(token)")
			.build()

		return performRequest(request)
	}

	func createOrderCalculation(forAddress addressId: Int, token: String) -> AnyPublisher<OrderCalculateResponse, Error> {
		let request = requestBuilder
			.set(path: .orderCalculate)
			.set(method: .POST)
			.set(parameters: ["adress_id": "\(addressId)"])
			.add(header: "Authorization", value: "Token \(token)")
			.build()

		return performRequest(request)
	}

	func createOrderApproval(forAddress addressId: Int, token: String) -> AnyPublisher<OrderApproveResponse, Error> {
		let request = requestBuilder
			.set(path: .orderApprove)
			.set(method: .POST)
			.set(parameters: ["adress_id": "\(addressId)"])
			.add(header: "Authorization", value: "Token \(token)")
			.build()

		return performRequest(request)
	}
}
