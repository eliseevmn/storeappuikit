//
//  AppColor.swift
//  StoreAppUIKit
//
//  Created by MAC on 15.02.2022.
//

import Foundation
import Combine

protocol BasketAPIClient {
	func get(basketWithToken token: String) -> AnyPublisher<BasketResponse, Error>
	func add(product: (productId: Int, quantity: Int), token: String) -> AnyPublisher<BasketUpdate, Error>
	func update(forBasketId basketId: Int, with product: (productId: Int, quantity: Int), token: String) -> AnyPublisher<BasketUpdate, Error>
	func delete(productBy basketProductId: Int, token: String) -> AnyPublisher<EmptyResponse, Error>
	func clear(basketWithToken token: String) -> AnyPublisher<EmptyResponse, Error>
}

extension APIClient: BasketAPIClient {
	func get(basketWithToken token: String) -> AnyPublisher<BasketResponse, Error> {
		let request = requestBuilder
			.set(path: .basket())
			.set(method: .GET)
			.add(header: "Authorization", value: "Token \(token)")
			.build()

		return performRequest(request)
	}

	func add(product: (productId: Int, quantity: Int), token: String) -> AnyPublisher<BasketUpdate, Error> {
		let request = requestBuilder
			.set(path: .basket())
			.set(method: .POST)
			.add(header: "Authorization", value: "Token \(token)")
			.set(parameters: ["products": [["product": product.productId,
										   "quantity": product.quantity]]])
			.build()

		return performRequest(request)
	}

	func update(forBasketId basketId: Int, with product: (productId: Int, quantity: Int), token: String) -> AnyPublisher<BasketUpdate, Error> {
		let request = requestBuilder
			.set(path: .basket(id: basketId))
			.set(method: .PATCH)
			.add(header: "Authorization", value: "Token \(token)")
			.set(parameters: ["products": [["product": product.productId,
										   "quantity": product.quantity]]])
			.build()

		return performRequest(request)
	}

	func delete(productBy basketProductId: Int, token: String) -> AnyPublisher<EmptyResponse, Error> {
		let request = requestBuilder
			.set(path: .basketDelete(id: basketProductId))
			.set(method: .DELETE)
			.add(header: "Authorization", value: "Token \(token)")
			.build()

		return performRequest(request)
	}

	func clear(basketWithToken token: String) -> AnyPublisher<EmptyResponse, Error> {
		let request = requestBuilder
			.set(path: .basket())
			.set(method: .DELETE)
			.add(header: "Authorization", value: "Token \(token)")
			.build()

		return performRequest(request)
	}
}

struct EmptyResponse: Decodable {}
